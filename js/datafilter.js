$('#selectcategory').on('change', function(){
                var id = this.value;
              if(id === 'all'){
                    $(".thumbnails").children().show(); 
                }else {
                    $(".thumbnails").children().hide(); 
                    $(".category"+id).show(); 
                }
                    
            });
            
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                      });
            var frm3 = $('#loginForm');
    frm3.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: frm3.attr('method'),
            url: frm3.attr('action'),
            data: frm3.serialize(),
            dataType:'json',
            success: function (response) {
                console.log(response);
                if(response.count < 1 )
                $('.emailorpassword').show(0).delay(5000).hide(0);
           else 
                window.location.href = "dashboard";
                
            },
            error: function (response) {
               $('.emailorpassword').show(0).delay(5000).hide(0);
            },
        });
    });