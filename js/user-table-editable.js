
var TableEditable = function () {

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            /* Search Noftify */

            var searchRes1 = aData[3].search("On");
            if (searchRes1 != "-1")
            {


                var on_flag = "selected";
                var off_flag = "";
            }
            else
            {


                var on_flag = "";
                var off_flag = "selected";

            }
            var searchRes1 = aData[4].search("Activate");
            if (searchRes1 != "-1")
            {


                var a_on_flag = "selected";
                var a_off_flag = "";
            }
            else
            {


                var a_on_flag = "";
                var a_off_flag = "selected";

            }


            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
            jqTds[2].innerHTML = '<input type="email" class="form-control input-small" value="' + aData[2] + '">';
            jqTds[3].innerHTML = '<select name="notify" class = "form-control"><option value="1"' + on_flag + '> On </option><option value="0" ' + off_flag + '> Off </option> </select>';
            jqTds[4].innerHTML = '<select name="active" class = "form-control"><option value="1"' + a_on_flag + '> Activate </option><option value="0" ' + a_off_flag + '> Deactivate </option> </select>';

            jqTds[5].innerHTML = '<a class="edit" href="">Save</a> <a class="cancel" href="">Cancel</a>';
        }

        function saveRow(oTable, nRow, id, table) {
            var jqInputs = $('input', nRow);
            var jqInputs2 = $('select', nRow);

            if (jqInputs2[0].value == '1')
            {

                var notify = '<span class="label label-success">On </span>';

            }
            else
            {

                var notify = '<span class="label label-default"> Off </span>';


            }
            if (jqInputs2[1].value == '1')
            {

                var active = '<span class="label label-success">Activate </span>';

            }
            else
            {

                var active = '<span class="label label-default"> Deactivate </span>';


            }

            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var valid = true;
            if (!re.test(jqInputs[2].value))
            {
                alert("Enter Valid Email...");
                valid = false;
            }


            if (valid == true)
            {

                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(notify, nRow, 3, false);
                oTable.fnUpdate(active, nRow, 4, false);
                oTable.fnDraw();


                // ajax
                var AssocArray = {};  // <- initialize an object, not an array

                AssocArray["username"] = jqInputs[0].value;
                AssocArray["name"] = jqInputs[1].value;
                AssocArray["email"] = jqInputs[2].value;
                AssocArray["notify"] = jqInputs2[0].value;
                AssocArray["active"] = jqInputs2[1].value;
                JSON.stringify(AssocArray);
                $.post("ajax_op/userQuickUpdate.php", {UpdatedArray: AssocArray, id: id, table: table}, function (data) {
                    //alert(data);
                });
            }






        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
            oTable.fnDraw();
        }

        var table = $('#sample_editable_1');
        var oTable = table.dataTable({
            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{// set default column settings
                    'orderable': true,
                    'targets': [0]
                }, {
                    "searchable": true,
                    "targets": [0]
                }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });
        var tableWrapper = $("#sample_editable_1_wrapper");
        tableWrapper.find(".dataTables_length select").select({
            showSearchInput: false //hide search box with special css class
        }); // initialize select2 dropdown

        var nEditing = null;
        var nNew = false;
        $('#sample_editable_1_new').click(function (e) {
            e.preventDefault();
            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {


                    saveRow(oTable, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;
                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '', '', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });
        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this object ?") == false) {
                return;
            }




            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);

            /* DELETE AJAX */
            var id = $(this).parents('tr').attr('id');
            var table = $(this).parents('tr').attr('table');
            $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
            $.post(base_url+"/delete", {id: id, table: table}, function (response) {
                console.log(response);
            });
            /* DELETE AJAX */

        });
        table.on('click', '.edit', function (e) {



            e.preventDefault();
            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];
            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                var id = $(this).parents('tr').attr('id');
                var table = $(this).parents('tr').attr('table');
                saveRow(oTable, nEditing, id, table);
                nEditing = null;

            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    }

    return {
//main function to initiate the module
        init: function () {
            handleTable();
        }

    };
}();