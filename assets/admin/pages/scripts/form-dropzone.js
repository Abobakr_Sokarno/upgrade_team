var FormDropzone = function () {


    return {
        //main function to initiate the module
        init: function () {

            Dropzone.options.myDropzone = {
                init: function () {
                    this.on("addedfile", function (file) {

                        // Create the remove button
                        var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block'>Remove file</button>");

                        // Capture the Dropzone instance as closure.
                        var _this = this;

                        // Listen to the click event
                        removeButton.addEventListener("click", function (e) {
                            // Make sure the button click doesn't submit the form:
                            e.preventDefault();
                            e.stopPropagation();

                            // Remove the file preview.
                            _this.removeFile(file);
                            // If you want to the delete the file on the server as well,
                            // you can do the AJAX request here.
                        });

                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    });
                },
                success: function (file, response) {

                    if (file.type == "") {
                        done("Error! Files of this type are not accepted");

                    } else {
                        // AJAX
                        var t_id = $("#t_id").val();
                        var t_table = $("#t_table").val();
                        var file_name = file.name;
                        var image_name = response;
                        image_name = image_name.replace(/\s+/g, '');


                      
                        $.ajax({
                            url: "ajax_op/insertRelatedFiles.php",
                            type: "post",
                            data: "id=" + t_id + "&table=" + t_table + "&file_name=" + file_name + "&image_name=" + image_name,
                            success: function (data, textStatus, jqXHR) {
                              
                                var ext = file_name.split('.').pop().toLowerCase();
                                if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                                    var element = '<a href="../images/' + image_name + '" title="' + image_name + '" alt="" class="img-responsive" target="_blank"><img src="images/-text.png" title="../images/' + image_name + '" style="width:292px;height: 160px;" alt="" class="img-responsive"/></a>';
                                } else
                                {
                                    var element = '<a href="../images/' + image_name + '" title="' + image_name + '" alt="" class="img-responsive" target="_blank"><img src="../images/' + image_name + '" title="../images/' + image_name + '" style="width:292px;height: 160px;" alt="" class="img-responsive"/></a>';
                                }
                                var newColThree = '<div class="col-md-3" table="images_to_object" id="menu_' + data + '" ids="' + data + '" ><div class="meet-our-team" style="margin-bottom: 10px;">' +
                                        element +
                                        '<div class="team-info">&nbsp;<a class="delete" title="Delete" href="javascript:;"><i class="glyphicon glyphicon-trash btn-default font-blue"></i></a> &nbsp;' + file_name.substring(0, 25);
                                +'</div></div></div>';
                                $("#sortme").append(newColThree);
                            },
                            error: function () {

                                alert("theres an error with AJAX");
                            }
                        });
                        // APPEND Object




                    }

                }
            }

        }
    };
}();