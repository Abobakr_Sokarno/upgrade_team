var UINestable = function () {

    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
                output = list.data('output');

        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
            var serial = JSON.stringify(list.nestable('serialize'));
           
            
            //alert(window.JSON.stringify(list.nestable('serialize'))); // Data OUTPUT HERE HOMOS
            $.ajax({
                url: "ajax_op/updateNestedCategories-team.php",
                type: "post",
                data: "menu=" + serial ,
                error: function () {

                    //alert("theres an error with AJAX");
                }

            }).done(function (data) {
               console.log(data)
            });


        } else {
            output.val('JSON browser support required for this demo.');
        }
    };


    return {
        //main function to initiate the module
        init: function () {

            // activate Nestable for list 1
            $('#nestable_list_1').nestable({
                group: 1
            })
                    .on('change', updateOutput);

            // activate Nestable for list 2
            /* $('#nestable_list_2').nestable({
             group: 1
             })
             .on('change', updateOutput);*/

            // output initial serialised data

            updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));

            // updateOutput($('#nestable_list_2').data('output', $('#nestable_list_2_output')));

            $('#nestable_list_menu').on('click', function (e) {
                var target = $(e.target),
                        action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });

            // $('#nestable_list_3').nestable();

        }

    };

}();