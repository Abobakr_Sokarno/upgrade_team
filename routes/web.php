<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//------------------------front_end-----------------//
Route::get('/', function () {return view('index');})->name('home');
Route::get('forgetpasswordform', function () {return view('backend.forgetpassword');});
Route::get('changepassword', function () {return view('backend.changepassword');});
Route::post('forgetpasswordaction','Auth\userController@forgetpassword');
Route::post('changepasswordaction','Auth\userController@changepassword');
Route::get('deleteall','commonController@delete_all_messages');
Route::post('search', 'commonController@search');
Route::get('solution', function () {return view('solution');});
Route::get('main_services', function () {return view('main_services');});
Route::get('main_solutions', function () {return view('main_solutions');});
Route::get('service', function () {return view('Services');});
Route::get('gallary', function () {return view('gallary');});
Route::get('main_softwarehouse', function () {return view('main_softwarehouse');});
Route::get('software_house', function () {return view('software_house');});
Route::get('about_us', function () {return view('about_us');});
Route::get('contact', function () {return view('contact');});
Route::get('news_item', function () {return view('news');});
Route::post('contactaction', 'commonController@send');
//------------------------front_end-----------------//



Route::get('admin', function () {return view('backend.login');});
Route::post('delete','commonController@delete');
Route::post('sortme','commonController@sort');
Route::post('featured','commonController@featured');
Route::post('loginValidate','Auth\userController@check');
Route::get('dashboard',function () {return view('backend.dashboard');});



// -----------messages_route------------//
Route::get('contactmessages',function () {return view('backend.contact_messages.contact_messages');});
Route::get('show_contact/',function () {return view('backend.contact_messages.show_contact');});
// -----------messages_route------------//



// -----------users_route------------//
Route::get('users','Auth\userController@index');
Route::get('addusers',function () {return view('backend.users.adduser');})->name('addusers');
Route::post('adduseraction','Auth\userController@create');
Route::get('edituser/{id}','Auth\userController@show')->name('edituser');
Route::get('edituseraction/{id}','Auth\userController@update');
// -----------users_route------------//


// -----------services_route------------//
Route::get('services','servicesController@index');
Route::get('addservice',function () {return view('backend.services.add_service');})->name('addservice');
Route::post('addserviceaction','servicesController@create');
Route::get('editservice/{id}','servicesController@show');
Route::post('editserviceaction/{id}','servicesController@update');
// -----------services_route------------//


// -----------service_category_route------------//
Route::get('servicecategory','servicecategoryController@index');
Route::get('addservicecategory',function () {return view('backend.service_category.add_service_category');})->name('addservicecategory');
Route::post('addservicecategoryaction','servicecategoryController@create');
Route::get('editservicecategory/{id}','servicecategoryController@show');
Route::post('editservicecategoryaction/{id}','servicecategoryController@update');
// -----------service_category_route------------//


// -----------solutions_route------------//
Route::get('solutions','solutionsController@index');
Route::get('addsolution',function () {return view('backend.solutions.add_solution');})->name('addsolution');
Route::post('addsolutionaction','solutionsController@create');
Route::get('editsolution/{id}','solutionsController@show');
Route::post('editsolutionaction/{id}','solutionsController@update');
// -----------solutions_route------------//



// -----------solution_category_route------------//
Route::get('solutioncategory','solutioncategoryController@index');
Route::get('addsolutioncategory',function () {return view('backend.solution_category.add_solution_category');})->name('addsolutioncategory');
Route::post('addsolutioncategoryaction','solutioncategoryController@create');
Route::get('editsolutioncategory/{id}','solutioncategoryController@show');
Route::post('editsolutioncategoryaction/{id}','solutioncategoryController@update');
// -----------solution_category_route------------//



// -----------gallery_route------------//
Route::get('gallery','galleryController@index');
Route::get('gallerys','galleryController@index');
Route::get('addgallery',function () {return view('backend.gallery.add_gallery');})->name('addgallery');
Route::post('addgalleryaction','galleryController@create');
Route::get('editgallery/{id}','galleryController@show');
Route::post('editgalleryaction/{id}','galleryController@update');
// -----------gallery_route------------//



// -----------gallery_category_route------------//
Route::get('gallerycategory','gallerycategoryController@index');
Route::get('addgallerycategory',function () {return view('backend.gallery_category.add_gallery_category');})->name('addgallerycategory');
Route::post('addgallerycategoryaction','gallerycategoryController@create');
Route::get('editgallerycategory/{id}','gallerycategoryController@show');
Route::post('editgallerycategoryaction/{id}','gallerycategoryController@update');
// -----------gallery_category_route------------//



// -----------team__route------------//
Route::get('team','teamController@index');
Route::get('addteam',function () {return view('backend.team.add_team');})->name('addteam');
Route::post('addteamaction','teamController@create');
Route::get('editteam/{id}','teamController@show');
Route::post('editteamaction/{id}','teamController@update');
// -----------team__route------------//



// -----------clients__route------------//
Route::get('clients','clientsController@index');
Route::get('addclients',function () {return view('backend.clients.add_clients');})->name('addclients');
Route::post('addclientsaction','clientsController@create');
Route::get('editclients/{id}','clientsController@show');
Route::post('editclientsaction/{id}','clientsController@update');
// -----------clients__route------------//



// -----------softwarehouse__route------------//
Route::get('softwarehouse','softwarehouseController@index');
Route::get('addsoftwarehouse',function () {return view('backend.softwarehouse.add_softwarehouse');})->name('addsoftwarehouse');
Route::post('addsoftwarehouseaction','softwarehouseController@create');
Route::get('editsoftwarehouse/{id}','softwarehouseController@show');
Route::post('editsoftwarehouseaction/{id}','softwarehouseController@update');
// -----------softwarehouse__route------------//




// -----------slider__route------------//
Route::get('slider','sliderController@index');
Route::get('addslider',function () {return view('backend.slider.add_slider');})->name('addslider');
Route::post('addslideraction','sliderController@create');
Route::get('editslider/{id}','sliderController@show');
Route::post('editslideraction/{id}','sliderController@update');
// -----------slider__route------------//



// -----------feature__route------------//
Route::get('feature','featureController@index');
Route::get('addfeature',function () {return view('backend.feature.add_feature');})->name('addfeature');
Route::post('addfeatureaction','featureController@create');
Route::get('editfeature/{id}','featureController@show');
Route::post('editfeatureaction/{id}','featureController@update');
// -----------feature__route------------//




// -----------testmonial__route------------//
Route::get('testmonial','testmonialController@index');
Route::get('addtestmonial',function () {return view('backend.testmonial.add_testmonial');})->name('addtestmonial');
Route::post('addtestmonialaction','testmonialController@create');
Route::get('edittestmonial/{id}','testmonialController@show');
Route::post('edittestmonialaction/{id}','testmonialController@update');
// -----------testmonial__route------------//





// -----------homepagesliderblocks__route------------//
Route::get('homepageslidrblocks','homepagesliderblocksController@index');
Route::get('homepagesliderblocks','homepagesliderblocksController@index');
Route::get('addhomepageslidrblocks',function () {return view('backend.homepagesliderblocks.add_homepagesliderblocks');})->name('addhomepagesliderblocks');
Route::post('addhomepagesliderblocksaction','homepagesliderblocksController@create');
Route::get('edithomepageslidrblocks/{id}','homepagesliderblocksController@show');
Route::post('edithomepagesliderblocksaction/{id}','homepagesliderblocksController@update');
// -----------homepagesliderblocks__route------------//




// -----------ourskills__route------------//
Route::get('ourskills','ourskillsController@index');
Route::get('addourskills',function () {return view('backend.ourskills.add_ourskills');})->name('addourskills');
Route::post('addourskillsaction','ourskillsController@create');
Route::get('editourskills/{id}','ourskillsController@show');
Route::post('editourskillsaction/{id}','ourskillsController@update');
// -----------ourskills__route------------//



// -----------headertext__route------------//
Route::get('headertext','headertextController@index');
Route::get('addheadertext',function () {return view('backend.headertext.add_headertext');})->name('addheadertext');
Route::post('addheadertextaction','headertextController@create');
Route::get('editheadertext/{id}','headertextController@show');
Route::post('editheadertextaction/{id}','headertextController@update');
// -----------headertext__route------------//


// -----------footertext__route------------//
Route::get('footertext','footertextController@index');
Route::get('addfootertext',function () {return view('backend.footertext.add_footertext');})->name('addfootertext');
Route::post('addfootertextaction','footertextController@create');
Route::get('editfootertext/{id}','footertextController@show');
Route::post('editfootertextaction/{id}','footertextController@update');
// -----------footertext__route------------//



// -----------othertextsettings__route------------//
Route::get('othertextsettings','othertextsettingsController@index');
Route::get('addothertextsettings',function () {return view('backend.othertextsettings.add_othertextsettings');})->name('addothertextsettings');
Route::post('addothertextsettingsaction','othertextsettingsController@create');
Route::get('editothertextsettings/{id}','othertextsettingsController@show');
Route::post('editothertextsettingsaction/{id}','othertextsettingsController@update');
// -----------othertextsettings__route------------//


// -----------new__route------------//
Route::get('news','newsController@index');
Route::get('addnews',function () {return view('backend.news.add_news');})->name('addnews');
Route::post('addnewsaction','newsController@create');
Route::get('editnews/{id}','newsController@show');
Route::post('editnewsaction/{id}','newsController@update');
// -----------new__route------------//


// -----------branches__route------------//
Route::get('branches','branchesController@index');
Route::get('addbranches',function () {return view('backend.branches.add_branches');})->name('addbranches');
Route::post('addbranchesaction','branchesController@create');
Route::get('editbranches/{id}','branchesController@show');
Route::post('editbranchesaction/{id}','branchesController@update');
// -----------branches__route------------//


// -----------whatwedo__route------------//
Route::get('whatwedo','whatwedoController@index');
Route::get('addwhatwedo',function () {return view('backend.whatwedo.add_whatwedo');})->name('addwhatwedo');
Route::post('addwhatwedoaction','whatwedoController@create');
Route::get('editwhatwedo/{id}','whatwedoController@show');
Route::post('editwhatwedoaction/{id}','whatwedoController@update');
// -----------whatwedo__route------------//


// -----------aboutus__route------------//
Route::get('aboutus','aboutusController@index');
Route::get('addaboutus',function () {return view('backend.aboutus.add_aboutus');})->name('addaboutus');
Route::post('addaboutusaction','aboutusController@create');
Route::get('editaboutus/{id}','aboutusController@show');
Route::post('editaboutusaction/{id}','aboutusController@update');
// -----------aboutus__route------------//




// -----------contactus__route------------//
Route::get('contactus','contactusController@index');
Route::get('addcontactus',function () {return view('backend.contactus.add_contactus');})->name('addcontactus');
Route::post('addcontactusaction','contactusController@create');
Route::get('editcontactus/{id}','contactusController@show');
Route::post('editcontactusaction/{id}','contactusController@update');
// -----------contactus__route------------//


Route::get('logout','Auth\userController@logout');