
<?php 


// -----------contactus__route------------//
Route::get('contactus','contactusController@index');
Route::get('addcontactus',function () {return view('backend\contactus\add_contactus');})->name('addcontactus');
Route::post('addcontactusaction','contactusController@create');
Route::get('editcontactus/{id}','contactusController@show');
Route::post('editcontactusaction/{id}','contactusController@update');
// -----------contactus__route------------//
