<?php 
use App\Http\Controllers\commonController;
//echo session()->get('code');
$header_texts = commonController::get_content('header_text');
$footer_texts = commonController::get_content('footer_text');
$whatwedo = commonController::get_content('what_we_do');
$solution_category = commonController::get_content_sortted('solution_category');
$service_category = commonController::get_content_sortted('service_category');
$header_info = array();
$footer_info = array(); $i=1;
foreach ($header_texts as $text){$header_info[$i++]=$text->text_value;}$i=1;
foreach ($footer_texts as $text){$footer_info[$i++]=$text->text_value;}

$curr_tab = str_replace('/upgrade_team/',"",$_SERVER['REQUEST_URI']);

$activetab = array();
$activetab['home']='none';
$activetab['solutions']='none';
$activetab['services']='none';
$activetab['softwarehouse']='none';
$activetab['gallary']='none';
$activetab['about_us']='none';
$activetab['contact']='none';
if(preg_match("/solution/i", $curr_tab)   ){
    $activetab['solutions'] = 'active'; 
}else if(preg_match("/service/i", $curr_tab) ){
    $activetab['services'] = 'active';
}else if($curr_tab == "contact" ){
    $activetab['contact'] = 'active';
}else if($curr_tab == "about_us" ){
    $activetab['about_us'] = 'active';
}else if(preg_match("/software/i", $curr_tab) ){
    $activetab['softwarehouse'] = 'active';
}else if(preg_match("/gallary/i", $curr_tab) ){
    $activetab['gallary'] = 'active';
}else {
    $activetab['home'] = 'active';
}
?>
<!-- 
* by Ahmed Aboelwafa
* Email    : aboelwafa0111@gmail.com
* LinkedIn : https://www.linkedin.com/in/ahmed-aboelwafa/
* Phone    : 002 01154560600
* website  : http:aboelwafa.bitballoon.com 
-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>@yield('pageTitle')</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="keywords" content="upgrade-team">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="images/icon.png"/>
   

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{url('css/style.css')}}">
    <link rel="stylesheet" href="{{url('css/responsive.css')}}">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Start: Preloader
    ============================= -->

    <div class="preloader">
        <div class="loader"></div>
    </div>

    <!-- End: Preloader
    ============================= -->

    <!-- Start: Header Top
    ============================= -->
    <section id="header-top">
        <div class="container">
            
            <div class="row">
                <div class="col-lg-6 col-md-6 text-md-left text-center mb-lg-0 mb-1">
                    <ul class="header-social d-inline-block">
                        <li><a href="<?php echo $header_info[1];?>"><i class="icofont icofont-social-facebook"></i></a></li>
                        <li><a href="<?php echo $header_info[2];?>"><i class="icofont icofont-social-twitter"></i></a></li>
                        <li><a href="<?php echo $header_info[3];?>"><i class="icofont icofont-social-linkedin"></i></a></li>
                        <li><a href="<?php echo $header_info[4];?>"><i class="icofont icofont-social-tumblr"></i></a></li>
                        <li><a href="<?php echo $header_info[4];?>"><i class="icofont icofont-social-dribbble"></i></a></li>
                    </ul>
                    <div class="address d-inline-block"><i class="icofont icofont-social-google-map mr-2"></i><?php echo $header_info[5];?></div>
                </div>
                <div class="col-lg-6 col-md-6 text-center text-md-right">
                    <div class="email d-inline-block">
                        <a href="#"><i class="fa fa-envelope-o mr-2"></i><?php echo $header_info[6];?></a>
                    </div>
                    <div class="phone d-inline-block">
                        <i class="fa fa-phone mr-2"></i><?php echo $header_info[7];?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End: Header Top
    ============================= -->

    <!-- Start: Search
    ============================= -->

    <div id="search">
        <a href="{{url('/')}}" id="close-btn">
        <i class="fa fa-times"></i>
        </a>
        <div>
            <form action="{{url('search')}}" method="post" id="searhForm">
                {!!csrf_field()!!}
            <input placeholder="type here" name="phrase" id="searchbox" type="text" />
            <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
    </div>

    <!-- End: Search
    ============================= -->



    <!-- Start: Header
    ============================= -->
    <header id="header">

        <!-- Navigation Starts -->
        <div class="navbar-area sticky-nav">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <div class="logo main">
                            <a href="{{url('/')}}"><img class="responsive" src="<?php echo url($header_info[8]);?>" alt="upgrade-team"></a>
                        </div>
                    </div>

                    <!-- Nav -->
                    <div class="col-lg-6 d-none d-lg-block">
                        <nav class="text-right main-menu">
                            <ul>
                                <li class="<?php echo $activetab['home']?>">
                                    <?php $nav_bar = explode('[sep]', $header_info[12]);?>
                                    <a href="{{url('/')}}"><?php echo $nav_bar[0];?></a>
                                </li>
                                <li class="<?php echo $activetab['solutions']?>">
                                    <a href="{{url('main_solutions')}}"><?php echo $nav_bar[1];?><i class="fa fa-angle-down"></i></a>
                                    <ul>
                                        <?php foreach ($solution_category as $category){?>
                                        <li><a href="{{url('main_solutions?title=')}}<?php echo $category->category_name;?>"><?php echo commonController::EnglishTranslate($category->category_name);?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <li class="<?php echo $activetab['services']?>"><a href="{{url('main_services')}}"><?php echo $nav_bar[2];?><i class="fa fa-angle-down"></i></a>
                                    <ul>
                                      <?php foreach ($service_category as $category){?>
                                        <li><a href="{{url('main_services?title=')}}<?php echo $category->category_name;?>"><?php echo commonController::EnglishTranslate($category->category_name);?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <li class="<?php echo $activetab['gallary']?>"><a href="{{url('gallary')}}"><?php echo $nav_bar[3];?></a></li>
                                <li class="<?php echo $activetab['softwarehouse']?>"><a href="{{url('main_softwarehouse')}}"><?php echo $nav_bar[4];?></a></li>
                                <li class="<?php echo $activetab['about_us']?>"><a href="{{url('about_us')}}"><?php echo $nav_bar[5];?></a></li>
                                <li class="<?php echo $activetab['contact']?>"><a href="{{url('contact')}}"><?php echo $nav_bar[6];?></a></li>
                            </ul>
                        </nav>
                    </div>
                    <!-- Nav End -->

                    <div class="col-lg-3 col-6">
                        <div class="header-right-bar">                            
                            <ul>
                                <li class="search-button">
                                    <a class="" href="#search"><i class="icofont icofont-search"></i></a>                                
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Start Mobile Menu -->
            <div class="mobile-menu-area d-lg-none">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav class="mobile-menu-active">
                            <ul>
                                <li class="<?php echo $activetab['home']?>">
                                    <a href="{{url('/')}}"><?php echo $nav_bar[0];?></a>
                                </li>
                                <li class="<?php echo $activetab['solutions']?>"><a href="{{url('main_solutions')}}"><?php echo $nav_bar[1];?><i class="fa fa-angle-down"></i></a>
                                    <ul>
                                  <?php foreach ($solution_category as $category){?>
                                        <li><a href="{{url('main_solutions?id=')}}<?php echo $category->id;?>"><?php echo commonController::EnglishTranslate($category->category_name);?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <li class="<?php echo $activetab['services']?>"><a href="{{url('main_services')}}"><?php echo $nav_bar[2];?><i class="fa fa-angle-down"></i></a>
                                    <ul>
                                      <?php foreach ($service_category as $category){?>
                                        <li><a href="{{url('main_services?id=')}}<?php echo $category->id;?>"><?php echo commonController::EnglishTranslate($category->category_name);?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <li class="<?php echo $activetab['gallary']?>"><a href="{{url('gallary')}}"><?php echo $nav_bar[3];?></a></li>
                                <li class="<?php echo $activetab['softwarehouse']?>" ><a href="{{url('main_softwarehouse')}}"><?php echo $nav_bar[4];?></a></li>
                                <li class="<?php echo $activetab['about_us']?>"><a href="{{url('about_us')}}"><?php echo $nav_bar[5];?></a></li>
                                <li class="<?php echo $activetab['contact']?>"><a href="{{url('contact')}}"><?php echo $nav_bar[6];?></a></li>
                            </ul>
                                </nav>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Mobile Menu -->
        </div>
        
        @yield('content')
        
        
          <!-- End: Contact Us
    ============================= -->

    <!-- Start: Footer Sidebar
    ============================= -->

    <footer id="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 mb-lg-0 mb-4">
                    <aside class="widget widget_about">
                        <div class="footer-logo"><img src="<?php echo url($footer_info[8]);?>" alt=""></div>
                        <p class="widget-text"><?php echo $footer_info[18];?></p>
                        <a href="{{url("/about_us")}}">Go for details <i class="icofont icofont-long-arrow-right"></i></a>
                    </aside>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12 mb-lg-0 mb-4">
                    <aside class="widget widget_pages">
                        <h4 class="widget-title">Web Pages</h4>
                        <div class="title-border"></div>
                        <ul>
                            <?php $nav_footer = array();
                            
                            $nav_footer = explode('[sep]',$footer_info[12]);?>
                            <li><a href="{{url('/')}}"><?php echo $nav_footer[0];?></a></li>
                            <li><a href="{{url('main_solutions')}}"><?php echo $nav_footer[1];?></a></li>
                            <li><a href="{{url('main_service')}}"><?php echo $nav_footer[2];?></a></li>
                            <li><a href="{{url('gallary')}}"><?php echo $nav_footer[3];?></a></li>
                            <li><a href="{{url('main_softwarehouse')}}"><?php echo $nav_footer[4];?></a></li>
                            <li><a href="{{url('about_us')}}"><?php echo $nav_footer[5];?></a></li>
                            <li><a href="{{url('contact')}}"><?php echo $nav_footer[6];?></a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 mb-lg-0 mb-md-0 mb-4">
                    <aside class="widget widget_recent">
                        <h4 class="widget-title">What we do</h4>
                        <ul>
                            <?php $i=0;
                            foreach ($whatwedo as $text){$i++; if($i>3)break;?>
                            <li class="recent-item">
                                <a href="#">
                                    <img src="<?php echo url($text->image);?>" alt="">
                                    <p><?php echo commonController::EnglishTranslate($text->text);?></p>
                            </a>
                            </li>
                            <?php }?>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <aside class="widget widget_contact">
                        <h4 class="widget-title"><?php echo $footer_info[14];?></h4>
                        <p class="widget-text"><?php echo $footer_info[15];?></p>
                        <ul class="widget-social">
                            <li><a href="<?php echo $footer_info[1];?>"><i class="icofont icofont-social-facebook"></i></a></li>
                            <li><a href="<?php echo $footer_info[2];?>"><i class="icofont icofont-social-twitter"></i></a></li>
                            <li><a href="<?php echo $footer_info[3];?>"><i class="icofont icofont-social-linkedin"></i></a></li>
                            <li><a href="<?php echo $footer_info[4];?>"><i class="icofont icofont-social-tumblr"></i></a></li>
                            <li><a href="#"><i class="icofont icofont-social-dribbble"></i></a></li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </footer>

    <!-- End: Footer Sidebar
    ============================= -->

    <!-- Start: Footer Copyright
    ============================= -->

    <section id="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <p>©2017 All Rights Reserved - Designed and Developed by <a href="http://aboelwafa.bitballoon.com/" target="_blank">Aboelwafa</a></p>
                        <a href="#" class="scrollup"><i class="fa fa-arrow-up"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Footer Copyright
    ============================= -->


    <!-- Scripts -->
    <script src="{{url('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{url('js/popper.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script src="{{url('js/jquery.sticky.js')}}"></script>
    <script src="{{url('js/owl.carousel.min.js')}}"></script>
    <script src="{{url('js/jquery.shuffle.min.js')}}"></script>
    <script src="{{url('js/jquery.meanmenu.min.js')}}"></script>
    <script src="{{url('js/wow.min.js')}}"></script>
    <script src="{{url('js/jquery.magnific-popup.min.js')}}"></script>

    <!-- Smooth Scroll -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/ScrollToPlugin.min.js"></script>
    
    <!-- Custom Script -->
    <script src="{{url('js/customfrontend.js')}}"></script>

    
</body>

</html>

