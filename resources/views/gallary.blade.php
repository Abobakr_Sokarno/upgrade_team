<?php 
use \App\Http\Controllers\commonController;
$other_texts = commonController::get_content('other_text_settings');
$header_texts = commonController::get_content('header_text');$i=1;
foreach ($header_texts as $text){$header_info[$i++]=$text->text_value;}
$galleries = commonController::get_content_sortted('gallery');
$info = array();
$i=0;
foreach ($other_texts as $text){
 $info[$i++] = commonController::EnglishTranslate($text->text_value);
}
$nav_bar = explode('[sep]', $header_info[12]);
?>
@extends('master')
@section('pageTitle','Upgrade Team')
@section('content')
        <section id="breadcrumb-area"style="background:url('<?php echo $header_info[16];?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1><?php echo $nav_bar[3];?></h1>
                        <ul class="breadcrumb-nav list-inline">
                            <li><a href="{{url('/')}}"><?php echo $nav_bar[0];?></a></li>
                            <li class="active"><?php echo $nav_bar[3];?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </header>
    <!-- End: Header
    ============================= -->

    <!-- Start: Gallery Items
    ============================= -->
    <section id="gallery" class="section-padding-80">
        <div class="container">

            <div class="row">
                <?php $i=0; foreach ($galleries as $item){ ?>
                
                <div class="gallery-load col-lg-4 col-md-6 col-sm-12">
                    <div class="gallery-item">
                        <figure>
                            <img src="<?php echo url($item->image);?>" style="width: 90%; height: 250px; "alt="">
                            <figcaption style="width: 90%; height: 250px; ">
                                <div class="inner-content">
                                    <img src="{{url('img/gallery/gallery-icon.png')}}" alt="">
                                    <h4><?php echo commonController::EnglishTranslate($item->header);?></h4>
                                    <p><?php echo commonController::EnglishTranslate($item->description);?></p>
                                </div>
                                <a href="<?php echo $item->image;?>" class="gallery-popup mfp-image"><i class="icofont icofont-search-alt-1"></i></a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <?php }?>
                
            </div>   
                <!-- Load More Content Button  -->
                <button style="margin-left: 40%;"class="load-btn" href="#" id="load-pro"><span id="ti-port-load" class="ti-port-load-hide"></span><i class="fa fa-spinner"></i>View More </button>

            
        </div>
    </section>
    <!-- End: Gallery Items
    ============================= -->

    @stop