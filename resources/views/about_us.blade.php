<?php 
use App\Http\Controllers\commonController;
$other_texts = commonController::get_content('other_text_settings');
$header_texts = commonController::get_content('header_text');$i=1;
foreach ($header_texts as $text){$header_info[$i++]=$text->text_value;}
$about = commonController::get_content('about_us');
$skills = commonController::get_content_sortted('our_skills');
$teams = commonController::get_content_sortted('team');
$clients = commonController::get_content_sortted('our_clients');
foreach ($about as $about);
$info = array();
$i=1;
foreach ($other_texts as $text){
 $info[$i++] = commonController::EnglishTranslate($text->text_value);
}
$nav_bar = explode('[sep]', $header_info[12]);
?>
@extends('master')
@section('pageTitle','Upgrade Team')
@section('content')
        <!-- Breadcrumb Area -->
        <section id="breadcrumb-area"style="background:url('<?php echo $header_info[18];?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1><?php echo $nav_bar[5];?></h1>
                        <ul class="breadcrumb-nav list-inline">
                            <li><a href="{{url('/')}}"><?php echo $nav_bar[0];?></a></li>
                            <li class="active"><?php echo $nav_bar[5];?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </header>
    <!-- End: Header
    ============================= -->

    <!-- Start: About US
    ============================= -->
    <section id="about-us" class="section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-lg-0 mb-4">
                    <div class="section-header text-left">
                        <span class="section-before"></span>
                        <h2><?php echo commonController::EnglishTranslate($about->header);?></h2>
                        <p class="wow fadeInUp" data-wow-delay="0.1s"><?php echo commonController::EnglishTranslate($about->short_description);?></p>
                    </div>
                    <div class="section-info">
                        <p><?php echo commonController::EnglishTranslate($about->description);?></p>
                    </div>
                    <a href="contact" class="boxed-btn">Contact Us <i class="icofont icofont-long-arrow-right"></i></a>
                </div>
                <div class="col-lg-6">
                    <div class="video-section" style="background: url(<?php echo $about->image;?>); ">
                        <div class="play-icon">
                            <a href="<?php echo $about->link;?>" class="playbtn mfp-iframe"><img src="{{url('img/aboutus/play-icon.png')}}" alt=""></a>
                        </div>
                        <div class="watch-more">
                            <a href="<?php echo $about->link;?>">Watch more <i class="fa fa-youtube"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End: About US
    ============================= -->

    <!-- Start: Our skill
    ============================= -->
    <section id="skill" class="section-padding-top">
        <div class="container">

            <div class="row">
                <div class="col-md-6 offset-3">
                    <div class="section-header">
                        <h2><?php echo commonController::EnglishTranslate($info[12]);?></h2>
                        <p><?php echo commonController::EnglishTranslate($info[13]);?></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <?php $i=0;foreach ($skills as $skill){ $i++;if($i>2) break;?>
                <div class="col-lg-6 mb-lg-0 mb-4">
                    <div class="skill-item wow fadeInUp" data-wow-delay="0.1s" style="background:url(<?php echo $skill->image;?>)">
                        <h3><?php echo commonController::EnglishTranslate($skill->name);?></h3>
                        <p class="title"><?php echo commonController::EnglishTranslate($skill->header);?></p>
                        <p class="mb-4"><?php echo commonController::EnglishTranslate($skill->description);?></p>
                        <div class="skill-progess">
                            <div class="label">Quality</div>
                            <div class="progress">
                                <div class="progress-bar" style="width:<?php echo $skill->quality;?>%"></div>
                            </div>
                            <br>
                            <div class="label">Experience</div>
                            <div class="progress">
                                <div class="progress-bar" style="width:<?php echo $skill->experince;?>%"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="text-right">
                            <a href="#" class="view-details">View Details <i class="fa fa-share"></i></a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>

        </div>
    </section>
    <!-- End: Our skill
    ============================= -->

    <!-- Start: Our Team
    ============================= -->
    <section id="ourteam" class="section-padding-top">
        <div class="container">

            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <div class="section-header">
                        <h2><?php echo commonController::EnglishTranslate($info[14]);?></h2>
                        <p class="wow fadeInUp" data-wow-delay="0.1s"><?php echo commonController::EnglishTranslate($info[15]);?></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <?php foreach ($teams as $team){?>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-lg-0 mb-4">
                    <div class="team-member wow fadeInUp" data-wow-delay="0.4s">
                        <img src="<?php echo url($team->image);?>" alt="">
                        <ul class="member-social text-center">
                            <li><a href="<?php echo $team->facebook_link;?>" target="_blank"><i class="icofont icofont-social-facebook"></i></a></li>
                            <li><a href="<?php echo $team->twitter_link;?>" target="_blank"><i class="icofont icofont-social-twitter"></i></a></li>
                            <li><a href="<?php echo $team->linkedin_link;?>" target="_blank"><i class="icofont icofont-social-linkedin"></i></a></li>
                            <li><a href="#"><i class="icofont icofont-social-tumblr"></i></a></li>
                            <li><a href="#"><i class="icofont icofont-social-dribbble"></i></a></li>
                        </ul>
                        <div class="member-title">
                            <h4><?php echo commonController::EnglishTranslate($team->header);?></h4>
                            <p class="small"><?php echo commonController::EnglishTranslate($team->position);?></p>
                        </div>
                        <div class="inner-bio"><?php echo commonController::EnglishTranslate($team->description);?></div>
                    </div>
                </div>
                <?php } ?>
            </div>

        </div>
    </section>
    <!-- End: Our Team
    ============================= -->

    <!-- Start: Our Client
    ============================= -->
    <section id="our-client" class="section-padding">
        <div class="container">

            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <div class="section-header">
                        <h2><?php echo commonController::EnglishTranslate($info[16]);?></h2>
                        <p><?php echo commonController::EnglishTranslate($info[17]);?></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="client-carousel">
                        <?php foreach ($clients as $client){?>
                        <div class="single-client">
                            <div class="inner-client">
                                <img src="<?php echo url($client->image);?>" alt="">
                            </div>
                        </div>
                        <?php  } ?>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End: Our Client
    ============================= -->
 @stop