<?php 
use App\Http\Controllers\commonController;
$other_texts = commonController::get_content('other_text_settings');
$header_texts = commonController::get_content('header_text');$i=1;
foreach ($header_texts as $text){$header_info[$i++]=$text->text_value;}
if(isset($_GET['title'])){
$news = commonController::get_content_sortted_by_title('news', $_GET['title'],'header');
if(json_encode($news)=="null"){
    return redirect()->guest('/');
}
}else {
  ?>  
<script>
window.location.href = "/";
</script>
   <?php 
}
$info = array();
$i=0;
foreach ($other_texts as $text){
 $info[$i++] = commonController::EnglishTranslate($text->text_value);
}
$clients = commonController::get_content_sortted('our_clients');

$nav_bar = explode('[sep]', $header_info[12]);
?>
@extends('master')
@section('pageTitle','Upgrade Team')
@section('content')
        <!-- Breadcrumb Area -->
        <section id="breadcrumb-area"style="background:url('<?php echo $header_info[14];?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1>news</h1>
                        <ul class="breadcrumb-nav list-inline">
                            <li><a href="{{url('/')}}"><?php echo $nav_bar[0];?></a></li>
                            <li class="active">news</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </header>
    <!-- End: Header
    ============================= -->

    <!-- Start: Content Section
    ============================= -->
    <section id="blog-content" class="single-blog-area">
        <div class="container">

            <div class="row">
                <!-- Blog Content -->
                <div class="col-lg-9 col-md-12">
                    <article class="blog-post">
                        <div class="post-thumb">
                            <img src="<?php echo url($news->image);?>" alt="">
                        </div>
                        <ul class="meta-info">
                            <li class="post-tag"><a href="/"><i class="icofont icofont-ui-tag"></i>news</a></li>
                        </ul>
                        <div class="post-content">
                            <h4 class="post-title"><?php echo commonController::EnglishTranslate($news->header);?></h4>
                        <?php echo commonController::EnglishTranslate($news->description);?>
                            <br><br><br><h4>categories: <p><?php echo commonController::EnglishTranslate($news->categories);?></p></h4>
                        </div>
                       
                        <!-- Post Comment Area -->
                        

   <section id="our-client" class="section-padding">
        <div class="container">


            <div class="row">
                <div class="col-md-12">
                    <div class="client-carousel">
                        <?php foreach ($clients as $client){?>
                        <div class="single-client">
                            <div class="inner-client">
                                <img src="<?php  echo url($client->image)?>" alt="">
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>

        </div>
    </section>



                    </article>
                </div>

                <!-- Sidebar -->
               
            </div>

        </div>
    </section>
    <!-- End: Content Section
    ============================= -->
 @stop