<?php 
use App\Http\Controllers\commonController;
$other_texts = commonController::get_content('other_text_settings');
$header_texts = commonController::get_content('header_text');$i=1;
foreach ($header_texts as $text){$header_info[$i++]=$text->text_value;}
$solutions = commonController::get_content_sortted('solutions');
$solution_category = commonController::get_content_sortted('solution_category');
if(isset($_GET['title'])){
  $solutions = commonController::get_content_sortted_by_categoryName('solution_category','solutions',$_GET['title']);  
}
$info = array();
$i=0;
foreach ($other_texts as $text){
 $info[$i++] = commonController::EnglishTranslate($text->text_value);
}
$nav_bar = explode('[sep]', $header_info[12]);
?>
@extends('master')
@section('pageTitle','Upgrade Team')
@section('content')
        <!-- Breadcrumb Area -->
        <section id="breadcrumb-area" style="background:url('<?php echo $header_info[14];?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1><?php echo $nav_bar[1];?></h1>
                        <ul class="breadcrumb-nav list-inline">
                            <li><a href="{{url('/')}}"><?php echo $nav_bar[0];?></a></li>
                            <li class="active"><?php echo $nav_bar[1];?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </header>
    <!-- End: Header
    ============================= -->

    <!-- Start: Content Section
    ============================= -->
    <section id="blog-content">
        <div class="container">

            <div class="row">
                <!-- Blog Content -->
                <div class="col-lg-9 col-md-12 solutions">
                    <?php $i=-1; foreach ($solutions as $solution){
                    $display='block;'; 
                    if($i>1){$display='none;';}
                    $i++; 
                    ?>
                    <article class="blog-post page<?php echo (int)($i/3)+1;?>" style="display: <?php echo $display;?>">
                        <div class="post-thumb">
                            <img src="<?php echo url($solution->image);?>" alt="">
                            <div class="post-overlay">
                                <a href="<?php echo url('solution').'?title='.$solution->header;?>"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="post-content">
                            <h4 class="post-title"><a href="<?php echo url('solution').'?title='.$solution->header;?>"><?php echo commonController::EnglishTranslate($solution->header);?></a></h4>
                            <p class="content">
                                <?php echo commonController::EnglishTranslate($solution->description);?>
                            </p>
                            <a href="<?php echo url('solution').'?title='.$solution->header;?>" class="continue-reading">Continue reading <i class="fa fa-share"></i></a>
                        </div>

                    </article>
                    <?php } ?>
                    <nav class="pagination" aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <?php for($i=0; $i<count($solutions)/3; $i++){?>
                            <button onclick="paginate('page<?php echo $i+1;?>')"class="page-link page<?php echo $i+1;?> "><?php echo $i+1;?></button>
                            <?php } ?>
                            <li class="page-item more-page">
                                <a class="page-link" href="#">View More <i class="icofont icofont-long-arrow-right"></i></a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <!-- Sidebar -->
                <div class="col-lg-3 col-md-12">
                    <section class="sidebar">
                        <aside class="widget widget-search">
                            <h5 class="widget-title">Search</h5>
                            <form class='search-form' action='search' method='post'>
                                {!!csrf_field()!!}
                                <input type='hidden' name='form-name' value='form 1' />
                                <input type="search" name="phrase"placeholder="Search Solutions.html">
                                <input type="button" class="search-btn" value="Go">
                            </form>
`
                            
                        </aside>
                        <aside class="widget widget-categories">
                            <h5 class="widget-title">Categories</h5>
                            <ul>
                                <?php foreach ($solution_category as $category){?>
                                <li><a href="main_solutions?id=<?php echo $category->id;?>"><?php echo commonController::EnglishTranslate($category->category_name);?><i class="icofont icofont-long-arrow-right"></i></a></li>
                                <?php } ?>
                            </ul>
                        </aside>

                    </section>
                </div>
            </div>

        </div>
    </section>
    <!-- End: Content Section
    ============================= -->
   
 @stop