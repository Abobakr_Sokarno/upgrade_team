<?php 
use App\Http\Controllers\commonController;
$sliders = commonController::get_content_sortted('slider');
$homepagesliderblocks = commonController::get_content_sortted('home_page_slider_blocks');
$testmonials = commonController::get_content_sortted('testmonial');
$other_texts = commonController::get_content('other_text_settings');
$gallery_categories = commonController::get_content_sortted('gallery_category');
$gallery = commonController::get_content_sortted('gallery');
$features = commonController::get_content('features');
$news = commonController::get_content('news');
$contactus = commonController::get_content('contact_us');
foreach ($contactus as $contactus);
$info = array();
$i=0;
foreach ($other_texts as $text){
 $info[$i++] = commonController::EnglishTranslate($text->text_value);
}
?>
@extends('master')
@section('pageTitle','Upgrade Team')
@section('content')
        <!-- Header Slider -->
        <div class="row">
            <div class="col-md-12">
                <div class="header-slider">
                    <?php foreach ($sliders as $slider){?>
                    <div class="header-single-slider">
                        <figure>
                            <img src="<?php echo url($slider->image);?>" alt="">
                            <figcaption>
                                <div class="content">
                                    <div class="container inner-content text-left">
                                        <h1><?php echo commonController::EnglishTranslate($slider->header);?></h1>
                                        <p><?php echo commonController::EnglishTranslate($slider->description);?></p>
                                            <a href="#" class="boxed-btn">Explore More <i class="icofont icofont-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                   
                </div>
            </div>
        </div>
    </header>
    <!-- End: Header
    ============================= -->

    <!-- Start: Features List
    ============================= -->
    <section id="features-list">
        <div class="container">
            <div class="row text-lg-left text-md-center">
                
                <?php $i=0; foreach ($homepagesliderblocks as $homepagesliderblock){$i++; if($i>3)break;?>
                <div class="col-md-4 mb-lg-0 mb-3">
                    <div class="features-list-item wow fadeInUp" data-wow-delay="0.1s">
                        <h4><img src="<?php echo url($homepagesliderblock->image);?>">
                            &nbsp;&nbsp;<?php echo commonController::EnglishTranslate($homepagesliderblock->header);?></h4><br><br>
                            <p class="small"><?php echo commonController::EnglishTranslate($homepagesliderblock->description);?></p>
                        <a href="#" class="view-more">View more</a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <!-- End: Features List
    ============================= -->

    <!-- Start: Testimonial
    ============================= -->
    <section id="testimonial" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <div class="section-header">
                        <h2><?php echo $info[0];?></h2>
                        <p class="wow fadeInUp" data-wow-delay="0.1s"><?php echo $info[1];?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="testimonial-carousel text-center">
                        <?php foreach ($testmonials as $testmonial){?>
                        <div class="single-testimonial">
                            <div class="img-rounded"><img src="<?php echo $testmonial->image;?>" alt=""></div>
                            <h4><?php echo commonController::EnglishTranslate($testmonial->name);?></h4>
                            <p class="title"><?php echo commonController::EnglishTranslate($testmonial->company_name);?></p>
                            <p><?php echo commonController::EnglishTranslate( $testmonial->description);?></p>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End: Testimonial
    ============================= -->


        <!-- Start: Call Out
    ============================= -->
    <section id="call-out">
        <div class="container">
            <div class="row call-out">
                <div class="col-lg-9 col-md-7 col-12 text-md-left text-center mb-md-0 mb-4">
                    <h3>Banner</h3>
                    <p>Lorem Ipsum is simply dummy </p>
                </div>
                <div class="col-lg-3 col-md-5 col-12 text-md-right text-center">
                    <a href="#" class="boxed-btn">More</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End: Call Out
    ============================= -->


    <!-- Start: Portfolio
    ============================= -->
    <section id="portfolio" class="portfolio-masonary section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <div class="section-header">
                        <h2><?php echo $info[2];?></h2>
                        <p class="wow fadeInUp" data-wow-delay="0.1s"><?php echo $info[3];?></p>
                    </div>
                </div>
            </div>
            <div class="row portfolio-tab">
                <div class="col-md-12 text-center">
                    <ul class="portfolio-tab-sorting sorting-btn" id="filter">
                        <li><a href="#" data-group="Show All" class="active"><i class="icofont icofont-volleyball-alt"></i> Show All</a></li>
                         <?php foreach ($gallery_categories as $category){?>
                        <li><a href="#" data-group="<?php echo commonController::EnglishTranslate($category->category_name);?>"><i class="icofont icofont-news"></i> <?php echo commonController::EnglishTranslate($category->category_name);?></a></li>
                        <?php }?>
                    </ul>
                </div>
            </div>
            <div class="row portfolio" id="grid">
                <?php foreach ($gallery as $image){
                    $category_name = commonController::EnglishTranslate(commonController::category_name($image->category_id, 'gallery_category'));
                    ?>
                
                <div class="col-lg-3 col-md-6 col-sm-6 col-12" data-groups='["<?php echo $category_name;?>", "More", "Show All"]'>
                    <figure>
                        <img src="<?php echo url($image->image);?>" alt="">
                        <figcaption>
                            <div class="inner-text">
                                <h4><?php echo commonController::EnglishTranslate($image->header);?></h4>
                                <p><?php echo commonController::EnglishTranslate($image->description);?></p>
                                <a href="#" class="view-more">View more</a>
                                <a href="#"><img src="{{url('img/portfolio-hover.png')}}" alt=""></a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <?php } ?>
                
            </div>
        </div>
    </section>
    <!-- End: Portfolio
    ============================= -->

    <!-- Start: Features
    ============================= -->
    <section id="features" class="section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <div class="section-header">
                        <h2><?php echo $info[4];?></h2>
                        <p><?php echo $info[5];?></p>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <?php foreach ($features as $feature){?>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-lg-0 mb-4">
                    <div class="features-item item-1 wow fadeInUp" data-wow-delay="0.1s">
                        <img src="<?php echo $feature->image;?>">
                        <h5><?php echo commonController::EnglishTranslate($feature->header);?></h5>
                        <p class="small"><?php echo commonController::EnglishTranslate($feature->description);?></p>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </section>
    <!-- End: Feautres
    ============================= -->

    <!-- Start: Recent Blog
    ============================= -->

    <section id="recent-blog" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center">
                    <div class="section-header">
                        <h2><?php echo $info[6];?></h2>
                        <p class="wow fadeInUp" data-wow-delay="0.1s"><?php echo $info[7];?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                 <?php foreach ($news as $new){?>
                <div class="col-lg-4 col-md-6 col-sm-12 mb-lg-0 mb-4">
                       
                    <article class="blog-post">
                        <div class="post-thumbnail">
                            <img src="<?php echo $new->image;?>" alt="" class="img-responsive center-block">
                            <ul class="meta-info list-inline">
                                <li class="posted-by">By <a href="#"><?php echo commonController::EnglishTranslate($new->published_by);?></a></li>
                                <li class="post-date"><a href="#"><?php echo  $new->date;?></a></li>
                                <li class="tags"> Categories: <a href="#"><?php echo commonController::EnglishTranslate($new->categories);?></a></li>
                            </ul>
                        </div>
                        <div class="post-content">
                            <div class="post-content-inner">
                                <h5 class="post-title"><a href="news_item?title=<?php echo $new->header;?>"><?php echo  commonController::EnglishTranslate($new->header);?> </a></h5>
                                <p> <?php echo  commonController::EnglishTranslate($new->description);?></p>
                            </div>
                            <div class="read-more-wrapper">
                                <a href="news_item?title=<?php echo $new->header;?>" class="read-more-link">Read More</a>
                            </div>
                        </div>
                    </article>
                     
                </div>
                <?php }?>
            </div>
        </div>
    </section>

    <!-- End: Recent Blog
    ============================= -->

    <!-- Start: Contact Us
    ============================= -->

    <section id="contact-area">
       
        <div class="contact-box">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="contact-us-info">
                        <h3><?php echo $info[8]?></h3>
                        <ul class="contact-info">
                            <li>
                                <i class="icofont icofont-location-pin"></i>
                                <p><?php echo commonController::EnglishTranslate($contactus->address);?></p>
                            </li>
                            <li>
                                <i class="icofont icofont-email"></i>
                                <p><?php echo $contactus->email;?></p>
                            </li>
                            <li>
                                <i class="icofont icofont-ui-call"></i>
                                <p><?php echo explode(',', $contactus->phone)[0];?>
                                    <br><?php echo explode(',', $contactus->phone)[1];?></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12">
                    <div class="get-in-touch">
                        <h3><?php echo $info[9];?></h3>
                        <p><?php echo $info[10];?></p>
                        <form action="{{url('contactaction')}}" method="POST" id="contactForm">
                            {!!csrf_field()!!}
                            <div id="success" style="color:#33ce67; font-size: x-large;"></div>
                            <div id="fail" style="color:red; font-size: x-large;"></div>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your name" >
                            <input type="text" name="email" class="form-control" id="email" placeholder="Email here" >
                            <textarea name="message" class="form-control" rows="1" placeholder="Type your message" ></textarea>
                            <button type="submit" class="boxed-btn"> Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @stop

  