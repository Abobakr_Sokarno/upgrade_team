<?php 
use App\Http\Controllers\commonController;
$other_texts = commonController::get_content('other_text_settings');
$header_texts = commonController::get_content('header_text');$i=1;
foreach ($header_texts as $text){$header_info[$i++]=$text->text_value;}
if(isset($_GET['title'])){
$software_house = commonController::get_content_sortted_by_title('software_house', $_GET['title'],'header');
if(json_encode($software_house)=="null"){
    return redirect()->guest('/');
}
$related = commonController::get_content('software_house');
}else {
  ?>  
<script>
window.location.href = "/";
</script>
   <?php
}

$clients = commonController::get_content_sortted('our_clients');
$info = array();
$i=0;
foreach ($other_texts as $text){
 $info[$i++] = commonController::EnglishTranslate($text->text_value);
}
$nav_bar = explode('[sep]', $header_info[12]);
?>
@extends('master')
@section('pageTitle','Upgrade Team')
@section('content')
        <!-- Breadcrumb Area -->
        <section id="breadcrumb-area" style="background:url('<?php echo $header_info[15];?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1><?php echo $nav_bar[4];?></h1>
                        <ul class="breadcrumb-nav list-inline">
                            <li><a href="{{url('/')}}"><?php echo $nav_bar[0];?></a></li>
                            <li class="active"><?php echo $nav_bar[4];?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </header>
    <!-- End: Header
    ============================= -->

    <!-- Start: Content Section
    ============================= -->
    <section id="blog-content" class="single-blog-area">
        <div class="container">

            <div class="row">
                <!-- Blog Content -->
                <div class="col-lg-9 col-md-12">
                    <article class="blog-post">
                        <div class="post-thumb">
                            <img src="<?php echo url($software_house->image);?>" alt="">
                        </div>
                        <ul class="meta-info">
                            <li class="post-tag"><a href="#"><i class="icofont icofont-ui-tag"></i>software_house</a></li>
                        </ul>
                        <div class="post-content">
                            <h4 class="post-title"><?php echo commonController::EnglishTranslate($software_house->header);?></h4>
                        <?php echo commonController::EnglishTranslate($software_house->description);?>
                        </div>
                        <div class="tag-share">
                            <div class="row">
                                <div class="col-md-12">
                                   <h3>Related software_house</h3>
                                </div>
                            </div>
                        </div>
                        <!-- Post Comment Area -->
                        <div class="post-comment-area">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="media-left">
                                        <a href="#"><img src="{{url('img/blog/avatar01.png')}}" alt=""></a>
                                    </div>
                               <div class="media-body">
                              <?php foreach ($related as $item){?>
                                   <a href="software_house?title=<?php echo $item->header;?>"><p><?php echo commonController::EnglishTranslate($item->header);?></p> </a>
                              <?php }?>    
                               </div>
                                </li>
                            </ul>
                        </div>

   <section id="our-client" class="section-padding">
        <div class="container">


            <div class="row">
                <div class="col-md-12">
                    <div class="client-carousel">
                        <?php foreach ($clients as $client){?>
                        <div class="single-client">
                            <div class="inner-client">
                                <img src="<?php  echo $client->image?>" alt="">
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>

        </div>
    </section>



                    </article>
                </div>

                <!-- Sidebar -->
               
            </div>

        </div>
    </section>
    <!-- End: Content Section
    ============================= -->
 @stop