@extends('backend.master')
@section('pageTitle','Edit ourskills')
@section('content')
<?php 
use App\Http\Controllers\commonController;
if(isset($ourskills)){
    
        $id = $ourskills->id;
        $english_name = commonController::EnglishTranslate($ourskills->name);
        $english_header = commonController::EnglishTranslate($ourskills->header);
        $arabic_name = commonController::ArabicTranslate($ourskills->name);
        $arabic_header = commonController::ArabicTranslate($ourskills->header);
        $english_description = commonController::EnglishTranslate($ourskills->description);
        $arabic_description = commonController::ArabicTranslate($ourskills->description);
       
        $experince = $ourskills->experince; 
        $quality = $ourskills->quality; 
        $image = $ourskills->image; 
    
}
?>
                    <!-- BEGIN PAGE name-->
                    <h3 class="page-title">
                        <?php echo "ourskills List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('ourskills')}}">ourskills</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE name-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">

                        
                       <div class="row">

                        <div class="col-md-12 ">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i> <?php echo "Edit ourskills"; ?> Form 
                                    </div>

                                </div>
                                <div class="portlet-body form">
                                    
                                       
                                         <form class="form-horizontal" role="form" action="<?php echo url('editourskillsaction/'.$id);?>" method="POST" enctype="multipart/form-data" id="userPrivFrom">
                      
                                             {!! csrf_field() !!}
                                        <?php if(isset($_GET['validate'])) echo $_GET['validate'];?>
                                             @if ($errors->any())
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                      <li>{{ $error }}</li>
                                                      @endforeach
                                                 </ul>
                                             </div>
                                             @endif
                    
                                            <div class="tabbable-line">
                                                <ul class="nav nav-tabs nav-tabs-lg">
<!--                                                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"> English </a></li>
                                                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> Arabic </a></li>-->

                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1">
                                                        <div class="form-body">

                                                            <div class="form-group ">
                                                                <label class="col-md-3 control-label">ourskills name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="name" class="form-control" placeholder="Enter text" value="<?php echo $english_name; ?>" >
                                                                </div>
                                                            </div> 
                                                            <div class="form-group ">
                                                                <label class="col-md-3 control-label">header</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="header" class="form-control" placeholder="Enter text" value="<?php echo $english_header; ?>" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">ourskills Description</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="description" class="form-control" placeholder="Enter text" value="<?php echo $english_description; ?>" >
                                                               </div>
                                                            </div> 
                                                             <div class="form-group">
                                                                <label class="col-md-3 control-label">quality%</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="quality" class="form-control" placeholder="Enter text" value="<?php echo $quality;?>" >
                                                               </div>
                                                            </div> 
                                                           
                                                             
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">experince%</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="experince" class="form-control" placeholder="Enter text" value="<?php echo $experince;?>" >
                                                               </div>
                                                            </div> 

                          
                                                            <div class="form-group ">
                                                                <label class="control-label col-md-3"> Image</label>
                                                                <div class="col-md-9">
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="" style="width: 100%; height: 150px;">
                                                                            <img src="<?php echo url($image); ?>" id="imageExt" style="width: 18%; height: 100%;"alt=""/>
                                                                        </div>
                                                                       
                                                                        <div>
                                                                            <span class=" btn btn-file  ">
                                                                                <span class="fileinput-new">
                                                                                    Select image </span>
                                                                                <input type="file" name="image" id="image">
                                                                           </span>
                                                                            <button href="#" class="btn red fileinput-exists" data-dismiss="fileinput" onclick="cancelBtn()">
                                                                                Remove </button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                        </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_2">
                                                        <div class="form-body">
                                                              <div class="form-group ">
                                                                <label class="col-md-3 control-label">our skills name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="arabicname" class="form-control" placeholder="Enter text" value="<?php echo $arabic_name; ?>" >
                                                                </div>
                                                            </div> 
                                                            <div class="form-group ">
                                                                <label class="col-md-3 control-label">header</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="arabicheader" class="form-control" placeholder="Enter text" value="<?php echo $arabic_header; ?>" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">ourskills Description</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="arabicdescription" class="form-control" placeholder="Enter text" value="<?php echo $arabic_description;?>" >
                                                                </div>
                                                            </div> 
                                                            
                                                            
                                   
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                           <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">

                                                        <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>


                                                        <a href="{{url('ourskills')}}" class="btn default"> Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->

                        </div>
                    </div>


                    </div>
                   

                </div>
            </div>
            <!-- END CONTENT -->

        </div>
        @stop