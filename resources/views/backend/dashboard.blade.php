<?php 
use App\Http\Controllers\commonController;
?>
@extends('backend.master')
@section('pageTitle','Dashboard')
@section('content')

                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        <?php echo "Dashboard"; ?> <small><?php echo "Reports & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="dashboard">Dashboard</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">



                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red-intense">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo commonController::count('news'); ?>
                                    </div>
                                    <div class="desc">
                                        New(s)
                                    </div>
                                </div>
                                <a class="more" href="news">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>


                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat purple-plum">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo commonController::count('solutions'); ?>
                                    </div>
                                    <div class="desc">
                                        Solution(s)
                                    </div>
                                </div>
                                <a class="more" href="solutions">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green-haze">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo commonController::count('services'); ?>
                                    </div>
                                    <div class="desc">
                                        Service(s)
                                    </div>
                                </div>
                                <a class="more" href="services">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo commonController::count('contact_messages'); ?>
                                    </div>
                                    <div class="desc">
                                        Contact(s)
                                    </div>
                                </div>
                                <a class="more" href="contactmessages">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>


                    </div>
                    <!-- END DASHBOARD STATS -->
                    

                    <div class="clearfix">
                    </div>

                    <div class="row ">

                       
                       
                        <div class="col-md-6">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Latest from News
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="" title="">
                                        </a>

                                        <a href="javascript:;" class="remove" data-original-title="" title="">
                                        </a>
                                    </div>
                                </div>
                                <?php  ?>
                                <div class="portlet-body" style="display: block;">
                                    <div class="table-scrollable">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Title
                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $array = json_decode(commonController::get_content('news'));
                                                array_reverse($array);
                                                $i=1;
                                                foreach ($array as $array_item) { if($i==6)break;?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $i++;?>
                                                        </td>
                                                        <td>
                                                            <?php echo commonController::EnglishTranslate($array_item->header); //title                           ?>

                                                        </td>

                                                    </tr>
                                                <?php } ?>  


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>

                        <div class="col-md-6">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-coffee"></i>Latest From Solutions
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="" title="">
                                        </a>

                                        <a href="javascript:;" class="remove" data-original-title="" title="">
                                        </a>
                                    </div>
                                </div>
                                <?php $array = json_decode(commonController::get_content('solutions'));
                                       array_reverse($array);
                                       
                                ?>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Title
                                                    </th>



                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1; foreach ($array as $array_item) { if($i==6)break;?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $i++; //title                          ?>
                                                        </td>
                                                        <td>
                                                            <?php echo commonController::EnglishTranslate($array_item->header); //title                           ?>

                                                        </td>


                                                    </tr>
                                                <?php } ?> 

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>

                    </div>
                    <div class="row ">
                        <div class="col-md-6">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-coffee"></i>Latest From Services
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="" title="">
                                        </a>

                                        <a href="javascript:;" class="remove" data-original-title="" title="">
                                        </a>
                                    </div>
                                </div>
                                <?php $array = json_decode(commonController::get_category('services'));?>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Title
                                                    </th>



                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1; foreach ($array as $array_item) {if($i==6)break; ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $i++; //title                          ?>
                                                        </td>
                                                        <td>
                                                            <?php echo commonController::EnglishTranslate($array_item->header); //title                           ?>

                                                        </td>


                                                    </tr>
                                                <?php } ?> 

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>



                        <div class="col-md-6">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box grey-cascade">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-comments"></i>Latest from software house
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="" title="">
                                        </a>

                                        <a href="javascript:;" class="remove" data-original-title="" title="">
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Title
                                                    </th>
                                                    

                                                </tr>
                                            </thead>
                                            <tbody>
                                           <?php $softare_house = commonController::get_content('software_house');
                                           $i=1;
                                           foreach ($softare_house as $house){ if($i>6)break;?>
                                                <tr>
                                                    <td>
                                                        <?php echo $i++;?>
                                                    </td>
                                                    <td>
                                                         <?php echo commonController::EnglishTranslate($house->header);?>
                                                    </td>
                                                    
                                                </tr>
                                           <?php } ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                </div>
            </div>
            <!-- END CONTENT -->

        </div>
        @stop
