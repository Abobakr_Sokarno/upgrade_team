
@extends('backend.master')
@section('pageTitle',' Backend user')
@section('content')

                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        <?php echo "Backend Users List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="/users">Backend user</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">

                        <div >
                            <p style="font-size: 18px;"> 
                            Back end users Table : 
                            </p>
                        </div>

                        
                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <!--id="sample_editable_1_new"-->
                                                    <a href="addusers" class="btn green"> Add New <i class="fa fa-plus"></i></a>

                                                </div>
                                            </div>

                                        </div>
                                    </div>

         <?php if(isset($result)){echo '<div id="result" style="background-color:green; "><p style="text-align: center;color:white;font-size: 30px;">User edited successfully</p></div><script>$("#result").fadeOut(4000);</script>';}?>
                                   <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Username
                                                </th>

                                                <th>
                                                    Email
                                                </th>

                                                
                                                <th>
                                                    Actions
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                        <?php foreach ($users as $user){
                            ?>
                                            
                                            <tr table="users" id="<?php echo $user->id;?>">
                                                    <td>
                                                        <?php echo $user->name ; ?>
                                                    </td>


                                                    <td class="center">
                                                        <?php echo $user->email;?>
                                                    </td>

                                                    <td>

                                                        <a title="Edit" href="edituser/<?php echo $user->id;?>">
                                                            <i class="glyphicon glyphicon-pencil btn-default font-blue"></i></a>
                                                        &nbsp;

                                                        &nbsp;
                                                        <a title="Delete" class="delete" href="javascript:;">
                                                            <i class="glyphicon glyphicon-trash btn-default font-blue"></i></a>



                                                    </td>

                                                </tr>
                                                <?php
                                            }
                                            ?> 

                                        </tbody>
                                    </table>


                    </div>
                        

                </div>
            </div>
            <!-- END CONTENT -->

        </div>
        @stop

