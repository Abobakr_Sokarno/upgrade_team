@extends('backend.master')

@section('pageTitle','Add Backend user')
@section('content')

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    <?php echo "Backend Users "; ?> <small><?php echo "ADD NEW"; ?></small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="javascript:void(0)">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="users">Backend user</a>
        </li>
    </ul>

</div>
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS -->
<div class="row">

    <div >
        <p style="font-size: 18px;"> 
            Add back end user : 
        </p>
    </div>

    <div class="row">

        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box green ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i> <?php echo "Add New Backend User"; ?> Form 
                    </div>

                </div>
                <div class="portlet-body form">


                    <form class="form-horizontal" role="form" action="{{ url('adduseraction') }}" method="post" enctype="multipart/form-data" id="userPrivFrom">
                                       {!! csrf_field() !!}
                                         <?php if(isset($_GET['validate'])) echo $_GET['validate'];?>
                                        
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                        <div class="tabbable-line">

                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="form-body">

                                        <div class="form-group ">
                                            <label class="col-md-3 control-label">User name</label>
                                            <div class="col-md-9">
                                                <input type="text" name="name" class="form-control" placeholder="Enter text" value="" >
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">E-mail</label>
                                            <div class="col-md-9">
                                                <input type="text" name="email" class="form-control" placeholder="Enter text" value="" >
                                            </div>
                                        </div> 


                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Password</label>
                                            <div class="col-md-9">
                                                <input type="password" name="password" class="form-control" placeholder="Enter text" value="" >
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Confirm Password</label>
                                            <div class="col-md-9">
                                                <input type="password" name="cpassword" class="form-control" placeholder="Enter text" value="" >
                                            </div>
                                        </div>



                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">

                                    <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>


                                    <a href="{{ url('users') }}" class="btn default"> Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->

        </div>
    </div>


</div>


</div>
</div>
<!-- END CONTENT -->

</div>
@stop
