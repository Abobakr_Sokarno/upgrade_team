<?php 
use \App\Http\Controllers\commonController;
?>
@extends('backend.master')
@section('pageTitle','contact us')
@section('content')

 <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        <?php echo "contact us List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('contactus')}}">contact us</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                       
                  
                    <br><br>
                    
                     <div><p style="font-size: x-large;">See the controlled content -> <a target="_blanck" href="{{url('uploads/screenshots/contact_us.png')}}">click</a></p></div>
                   
                     <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                   Address 
                                                </th>

                                               
                                                
                                                
                                                <th>
                                                    email
                                                </th>
                                                <th>
                                                    phone
                                                </th>


                                                <th>
                                                    Actions
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($contactus as $about) {
                                                ?>
                                                <tr table="contactus" id="<?php echo $about->id ?>">
                                                    <td>
                                                        <?php echo commonController::EnglishTranslate($about->address) ?>
                                                    </td>
                                                    
                                                    <td>
                                                        <?php echo $about->email ?>
                                                    </td> 
                                                    
                                                    <td>
                                                        <?php echo $about->phone ?>
                                                    </td>

                                                    <td>
                                                        <a title="Edit" href="<?php echo url('editcontactus') .'/'. $about->id ?>">
                                                            <i class="glyphicon glyphicon-pencil btn-default font-blue"></i></a>
                                                        &nbsp;

                                             

                                                    </td>

                                                </tr>
                                                <?php
                                            }
                                            ?> 

                                        </tbody>
                                    </table>
                     </div>
                      
                       </div>
                    

            <!-- END CONTENT -->
           
@stop
