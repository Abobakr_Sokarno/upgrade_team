<?php 
use \App\Http\Controllers\commonController;
?>
@extends('backend.master')
@section('pageTitle','branches')
@section('content')

 <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        <?php echo "branches List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('branches')}}">branches</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                        <div><p style="font-size: x-large;">See the controlled content -> <a target="_blanck" href="{{url('uploads/screenshots/branches.png')}}">click</a></p></div>
                   
                    <a href="<?php echo url('addbranches') ?>" class="btn green"> Add New <i class="fa fa-plus"></i></a>

                    <br><br>
                     <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    City
                                                </th>

                                                <th>
                                                    Address
                                                </th>

                                                <th>
                                                    Arabic address
                                                </th>


                                                <th>
                                                    Actions
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($branches as $branch) {
                                                ?>
                                                <tr table="branches" id="<?php echo $branch->id ?>">
                                                    <td>
                                                        <?php echo commonController::EnglishTranslate($branch->city) ?>
                                                    </td>
                                                    <td>
                                                        <?php echo commonController::EnglishTranslate($branch->map_address) ?>
                                                    </td>

                                                    <td>
                                                        <?php echo commonController::EnglishTranslate($branch->map_address) ?>
                                                    </td>

                                                    <td>
                                                        <a title="Edit" href="<?php echo url('editbranches') .'/'. $branch->id ?>">
                                                            <i class="glyphicon glyphicon-pencil btn-default font-blue"></i></a>
                                                        &nbsp;

                                                         <a class="delete" title="Delete" href="javascript:;">
                                            <i class="glyphicon glyphicon-trash btn-default font-blue"></i></a>


                                                    </td>

                                                </tr>
                                                <?php
                                            }
                                            ?> 

                                        </tbody>
                                    </table>
                     </div>
                      
                       </div>
                    

            <!-- END CONTENT -->
           
@stop
