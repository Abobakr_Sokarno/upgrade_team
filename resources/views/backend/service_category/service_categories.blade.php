<?php

use \App\Http\Controllers\commonController;
?>
@extends('backend.master')
@section('pageTitle','Services')
@section('content')
<h3 class="page-title">
<?php echo "services List"; ?> <small><?php echo "Info & statstics"; ?></small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="javascript:void(0)">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{url('servicecategory')}}">Service categories</a>
        </li>
    </ul>

</div>
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS -->
 <div><p style="font-size: x-large;">See the controlled content -> <a target="_blanck" href="{{url('uploads/screenshots/servcice_category.png')}}">click</a></p></div>
                   
<a href="<?php echo url('addservicecategory') ?>" class="btn green"> Add New <i class="fa fa-plus"></i></a>



<div class="nz-accordion  row" data-collapsible=false id="sortme" table="service_category">
    <?php
    foreach ($categories as $category) {
        ?>
        <div class="col-md-12" table="service_category" id="menu_<?php echo $category->id; ?>" ids="<?php echo $category->id ?>" >
            <div  class="with-icon toggle-title nz-clearfix ">
                <span class=" s_image toggle-icon  "></span>
                <div class="toggletabs team-info" >
                    <?php if ($category->featured == 1) { ?>
                        <a class="featuredfaq" title="Show" href="javascript:;"><i class="glyphicon glyphicon-star btn-default font-graytab"></i></a>
                    <?php } else { ?>
                        <a class="featuredfaq" title="Hide" href="javascript:;"><i class="glyphicon glyphicon-star-empty btn-default font-graytab"></i></a>
    <?php } ?>


                    &nbsp;
                    <a class="edit" title="Edit" href="<?php echo url('editservicecategory').'/'.$category->id; ?>">
                        <i class="glyphicon glyphicon-pencil btn-default font-graytab"></i></a>

                    &nbsp;
                    <a class="deletefaq" title="Delete" href="javascript:;">
                        <i class="glyphicon glyphicon-trash btn-default font-graytab"></i></a>
                </div>


    <?php echo commonController::EnglishTranslate($category->category_name); ?>
            </div>

<!--            <div id="heelo we-create-awesome-websites-for-your-business" class="c_image">
        
            </div>  -->
        </div>
<?php } ?>


</div>
</div>

</div>

@stop

