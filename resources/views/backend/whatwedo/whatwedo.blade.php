-----<?php 
use \App\Http\Controllers\commonController;
?>
@extends('backend.master')
@section('pageTitle','whatwedo')
@section('content')

 <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        <?php echo "whatwedo List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('whatwedo')}}">what we do</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS --> 
                    <div><p style="font-size: x-large;">See the controlled content -> <a target="_blanck" href="{{url('uploads/screenshots/what_we_do.png')}}">click</a></p></div>
                   
                    <a href="<?php echo url('addwhatwedo') ?>" class="btn green"> Add New <i class="fa fa-plus"></i></a>
                    
                  
                   
                     <div class="row thumbnails" id="sortme" table="what_we_do">
                       <?php foreach ($whatwedo as $object){?>
                        <div class="col-md-3 cat category" table="what_we_do" id="menu_<?php echo $object->id;?>" ids="<?php echo $object->id;?>">
                                <div class="meet-our-team" >
                                    <h3><a href="<?php echo url('editwhatwedo').'/'.$object->id;?>" class="name" title=""><?php echo commonController::EnglishTranslate($object->text);?></a> </h3>
                                    <a class="mix-preview fancybox-button" href="<?php echo $object->image;?>" title="" data-rel="fancybox-button">

                                        <img src="<?php echo url($object->image);?>" title="" style="max-width:80%;height:150px;margin:auto;" alt="" class="img-responsive"/>
                                    </a>
                                    <div style="margin-left: 12px; margin-top: 12px;"class="team-info">

                                        <?php if($object->featured){?>
                                        <a class="featured" title="Show" href="javascript:;"><i class="glyphicon glyphicon-star btn-default font-blue"></i></a>
                                        <?php } else {?>
                                       
                                        <a class="featured" title="Hide" href="javascript:;"><i class="glyphicon glyphicon-star-empty btn-default font-blue"></i></a>
                                        <?php } ?>

                                        &nbsp;
                                        <a class="edit" title="Edit" href="<?php echo url('editwhatwedo').'/'.$object->id;?>">
                                            <i class="glyphicon glyphicon-pencil btn-default font-blue"></i></a>

                                        &nbsp;
                                        <a class="delete" title="Delete" href="javascript:;">
                                            <i class="glyphicon glyphicon-trash btn-default font-blue"></i></a>
                                        <span class="logtable_span">
                                        </span>

                                    </div>
                                </div>

                            </div>
                       <?php } ?>
                          
                           </div>
                    
                     </div>
                      
                       </div>
                    

            <!-- END CONTENT -->
           
@stop
