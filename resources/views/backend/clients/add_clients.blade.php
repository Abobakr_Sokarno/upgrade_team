<?php 
use App\Http\Controllers\commonController;
?>

@extends('backend.master')
@section('pageTitle','Add New clients')
@section('content')

                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        <?php echo "clients List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('clients')}}">clients</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">

                        
                       <div class="row">

                        <div class="col-md-12 ">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i> <?php echo "Add New clients"; ?> Form 
                                    </div>

                                </div>
                                <div class="portlet-body form">
                                    
                                       
                                         <form class="form-horizontal" role="form" action="{{ url('addclientsaction')}}" method="POST" enctype="multipart/form-data" id="userPrivFrom">
                      
                                             {!! csrf_field() !!}
                                        <?php if(isset($_GET['validate'])) echo $_GET['validate'];?>
                                             @if ($errors->any())
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                      <li>{{ $error }}</li>
                                                      @endforeach
                                                 </ul>
                                             </div>
                                             @endif
                    
                                            <div class="tabbable-line">
                 
                                                <div class="tab-content">
                                                        <div class="form-body">

                                                            <div class="form-group ">
                                                                <label class="col-md-3 control-label">link</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="link" class="form-control" placeholder="Enter text" value="" >
                                                                </div>
                                                            </div> 
                                                          
                                                          
                                                            <div class="form-group ">
                                                                <label class="control-label col-md-3"> Image</label>
                                                                <div class="col-md-9">
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="" style="width: 100%; height: 150px;">
                                                                            <img src="{{url('images/-text.png')}}" id="imageExt" style="width: 18%; height: 100%;"alt=""/>
                                                                        </div>
                                                                       
                                                                        <div>
                                                                            <span class=" btn btn-file  ">
                                                                                <span class="fileinput-new">
                                                                                    Select image </span>
                                                                                <input type="file" name="image" id="image">
                                                                           </span>
                                                                            <button href="#" class="btn red fileinput-exists" data-dismiss="fileinput" onclick="cancelBtn()">
                                                                                Remove </button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                        
                                                        </div>

                                                </div>
                                            </div>

                                           <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">

                                                        <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>


                                                        <a href="{{url('clients')}}" class="btn default"> Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->

                        </div>
                    </div>


                    </div>
                   

                </div>
            </div>
            <!-- END CONTENT -->

        </div>
        @stop