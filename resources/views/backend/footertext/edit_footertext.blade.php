@extends('backend.master')
@section('pageTitle','Edit footertext')
@section('content')
<?php 
use App\Http\Controllers\commonController;
if(isset($footertext)){
    
        $id = $footertext->id;
        $english_value = $footertext->text_value;
        $english_footer = $footertext->text_name;
        
       
}
?>
                    <!-- BEGIN PAGE footer-->
                    <h3 class="page-title">
                        <?php echo "footertext List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('footertext')}}">footertext</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE footer-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">

                        
                       <div class="row">

                        <div class="col-md-12 ">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                
                                <div class="portlet-body form">
                                    
                                       
                                         <form class="form-horizontal" role="form" action="<?php echo url('editfootertextaction/'.$id);?>" method="POST" enctype="multipart/form-data" id="userPrivFrom">
                      
                                             {!! csrf_field() !!}
                                        <?php if(isset($_GET['validate'])) echo $_GET['validate'];?>
                                             @if ($errors->any())
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                      <li>{{ $error }}</li>
                                                      @endforeach
                                                 </ul>
                                             </div>
                                             @endif
                    
                                            <div class="tabbable-line">
         
                                                <div class="tab-content">
                                                       <div class="form-body">

                                                            <div class="form-group ">
                                                                <label class="col-md-3 control-label">text footer</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" disabled="true" name="header" class="form-control" placeholder="Enter text" value="<?php echo $english_footer; ?>" >
                                                                </div>
                                                            </div> 
                                                            <?php $display = "none;"; if($id==8 || $id==11){?>
                                                            <div class="form-group ">
                                                                <label class="control-label col-md-3"> Image</label>
                                                                <div class="col-md-9">
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="" style="width: 100%; height: 150px;">
                                                                            <img src="<?php echo url($english_value); ?>" id="imageExt" style="width: 18%; height: 100%;"alt=""/>
                                                                        </div>
                                                                       
                                                                        <div>
                                                                            <span class=" btn btn-file  ">
                                                                                <span class="fileinput-new">
                                                                                    Select image </span>
                                                                                <input type="file" name="image" id="image">
                                                                           </span>
                                                                            <button href="#" class="btn red fileinput-exists" data-dismiss="fileinput" onclick="cancelBtn()">
                                                                                Remove </button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <?php }else if($id==12 || $id=13){
                                                             $nav_footer = explode('[sep]', $english_value);  
                                                             $nav = array('Home','Solution','Services','Gallery','Software House','About us','Contact us');
                                                             $nav_names = array('home','solutions','services','gallery','softwarehouse','aboutus','contactus');
                                                             for($i=0; $i<count($nav_footer); $i++){
                                                                 echo '<div class="form-group" >
                                                                <label class="col-md-3 control-label">'.$nav[$i].' </label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="'.$nav_names[$i].'" class="form-control" placeholder="Enter text" value="'.$nav_footer[$i].'" >
                                                               </div>
                                                            </div> ';
                                                             }
                                                            }else { $display = "block;";}?>
                                                            <div class="form-group" style="display: <?php echo $display;?>">
                                                                <label class="col-md-3 control-label">value </label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="value" class="form-control" placeholder="Enter text" value="<?php echo $english_value; ?>" >
                                                               </div>
                                                            </div> 
                                                        </div>
                                                        
                                                </div>
                                            </div>

                                           <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">

                                                        <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>


                                                        <a href="{{url('footertext')}}" class="btn default"> Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->

                        </div>
                    </div>


                    </div>
                   

                </div>
            </div>
            <!-- END CONTENT -->

        </div>
        @stop