<?php 
use \App\Http\Controllers\commonController;
?>
@extends('backend.master')
@section('pageTitle','othertextsettings')
@section('content')

 <!-- BEGIN PAGE footer-->
                    <h3 class="page-title">
                        <?php echo "othertextsettings List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('othertextsettings')}}">othertextsettings</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE footer-->
                    <!-- BEGIN DASHBOARD STATS -->
                       
                  
                   
                    
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Title
                                                </th>

                                                <th>
                                                    Value
                                                </th>

                                               


                                                <th>
                                                    Actions
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($othertextsettings as $text) {
                                                ?>
                                                <tr table="footer_text" id="<?php echo $text->id ?>">
                                                    <td>
                                                        <?php echo $text->text_name ?>
                                                    </td>
                                                    <td>
                                                        <?php echo commonController::EnglishTranslate($text->text_value) ?>
                                                    </td>

                                                    

                                                    <td>
                                                        <a title="Edit" href="<?php echo url('editothertextsettings') .'/'. $text->id ?>">
                                                            <i class="glyphicon glyphicon-pencil btn-default font-blue"></i></a>
                                                        &nbsp;



                                                    </td>

                                                </tr>
                                                <?php
                                            }
                                            ?> 

                                        </tbody>
                                    </table>
                     </div>
                      
                       </div>
                    

            <!-- END CONTENT -->
           
@stop
