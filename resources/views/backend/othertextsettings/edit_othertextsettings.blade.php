@extends('backend.master')
@section('pageTitle','Edit othertextsettings')
@section('content')
<?php 
use App\Http\Controllers\commonController;
if(isset($othertextsettings)){
    
        $id = $othertextsettings->id;
        $english_value = commonController::EnglishTranslate($othertextsettings->text_value);
        $english_footer = $othertextsettings->text_name;
        $arabic_value = commonController::ArabicTranslate($othertextsettings->text_value);
        $arabic_footer = $othertextsettings->text_name;
       
}
?>
                    <!-- BEGIN PAGE footer-->
                    <h3 class="page-title">
                        <?php echo "othertextsettings List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('othertextsettings')}}">othertextsettings</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE footer-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">

                        
                       <div class="row">

                        <div class="col-md-12 ">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i> <?php echo "Edit othertextsettings"; ?> Form 
                                    </div>

                                </div>
                                <div class="portlet-body form">
                                    
                                       
                                         <form class="form-horizontal" role="form" action="<?php echo url('editothertextsettingsaction/'.$id);?>" method="POST" enctype="multipart/form-data" id="userPrivFrom">
                      
                                             {!! csrf_field() !!}
                                        <?php if(isset($_GET['validate'])) echo $_GET['validate'];?>
                                             @if ($errors->any())
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                      <li>{{ $error }}</li>
                                                      @endforeach
                                                 </ul>
                                             </div>
                                             @endif
                    
                                            <div class="tabbable-line">
                                                <ul class="nav nav-tabs nav-tabs-lg">
<!--                                                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"> English </a></li>
                                                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> Arabic </a></li>-->

                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1">
                                                        <div class="form-body">

                                                            <div class="form-group ">
                                                                <label class="col-md-3 control-label">text footer</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" disabled="true" name="footer" class="form-control" placeholder="Enter text" value="<?php echo $english_footer; ?>" >
                                                                </div>
                                                            </div> 
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">value </label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="value" class="form-control" placeholder="Enter text" value="<?php echo $english_value; ?>" >
                                                               </div>
                                                            </div> 
                                                            

                          
                                                            


                                                        </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_2">
                                                        <div class="form-body">
                                                              <div class="form-group ">
                                                                <label class="col-md-3 control-label">text footer</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="arabicfooter" disabled="true" class="form-control" placeholder="Enter text" value="<?php echo $arabic_footer; ?>" >
                                                                </div>
                                                            </div> 
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">value</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="arabicvalue" class="form-control" placeholder="Enter text" value="<?php echo $arabic_value;?>" >
                                                                </div>
                                                            </div> 
                                                            
                                                            
                                   
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                           <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">

                                                        <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>


                                                        <a href="{{url('othertextsettings')}}" class="btn default"> Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->

                        </div>
                    </div>


                    </div>
                   

                </div>
            </div>
            <!-- END CONTENT -->

        </div>
        @stop