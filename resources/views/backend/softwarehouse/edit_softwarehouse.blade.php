@extends('backend.master')
@section('pageTitle','Edit softwarehouse')
@section('content')
<?php 
use App\Http\Controllers\commonController;
if(isset($softwarehouse)){
    
        $id = $softwarehouse->id;
        $english_header = commonController::EnglishTranslate($softwarehouse->header);
        $arabic_header = commonController::ArabicTranslate($softwarehouse->header);
        $english_description = commonController::EnglishTranslate($softwarehouse->description);
        $arabic_description = commonController::ArabicTranslate($softwarehouse->description);
        $twitter_link = $softwarehouse->twitter_link;
        $facebook_link = $softwarehouse->facebook_link;
        $linkedin_link = $softwarehouse->linkedin_link;
        $image = $softwarehouse->image; 
    
}
?>
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        <?php echo "softwarehouse List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('softwarehouse')}}">softwarehouse</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">

                        
                       <div class="row">

                        <div class="col-md-12 ">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i> <?php echo "Edit softwarehouse"; ?> Form 
                                    </div>

                                </div>
                                <div class="portlet-body form">
                                    
                                       
                                         <form class="form-horizontal" role="form" action="<?php echo url('editsoftwarehouseaction/'.$id);?>" method="POST" enctype="multipart/form-data" id="userPrivFrom">
                      
                                             {!! csrf_field() !!}
                                        <?php if(isset($_GET['validate'])) echo $_GET['validate'];?>
                                             @if ($errors->any())
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                      <li>{{ $error }}</li>
                                                      @endforeach
                                                 </ul>
                                             </div>
                                             @endif
                    
                                            <div class="tabbable-line">
                                                <ul class="nav nav-tabs nav-tabs-lg">
<!--                                                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"> English </a></li>
                                                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"> Arabic </a></li>-->

                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1">
                                                        <div class="form-body">

                                                            <div class="form-group ">
                                                                <label class="col-md-3 control-label">softwarehouse header</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="header" class="form-control" placeholder="Enter text" value="<?php echo $english_header; ?>" >
                                                                </div>
                                                            </div> 
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">softwarehouse Description</label>
                                                                <div class="col-md-9">
                                                                    <textarea type="text" name="description" class="form-control" placeholder="Enter text" rows="6"  ><?php echo $english_description; ?></textarea>
                                                               </div>
                                                            </div> 
                                                        
                                                            
                          
                                                            <div class="form-group ">
                                                                <label class="control-label col-md-3"> Image</label>
                                                                <div class="col-md-9">
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="" style="width: 100%; height: 150px;">
                                                                            <img src="<?php echo url($image); ?>" id="imageExt" style="width: 18%; height: 100%;"alt=""/>
                                                                        </div>
                                                                       
                                                                        <div>
                                                                            <span class=" btn btn-file  ">
                                                                                <span class="fileinput-new">
                                                                                    Select image </span>
                                                                                <input type="file" name="image" id="image">
                                                                           </span>
                                                                            <button href="#" class="btn red fileinput-exists" data-dismiss="fileinput" onclick="cancelBtn()">
                                                                                Remove </button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                        </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_2">
                                                        <div class="form-body">
                                                              <div class="form-group ">
                                                                <label class="col-md-3 control-label">software header</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="arabicheader" class="form-control" placeholder="Enter text" value="<?php echo $arabic_header; ?>" >
                                                                </div>
                                                            </div> 
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">softwarehouse Description</label>
                                                                <div class="col-md-9">
                                                                    <textarea type="text" name="arabicdescription" class="form-control" placeholder="Enter text"  ><?php echo $arabic_description;?></textarea>
                                                                </div>
                                                            </div> 
                                                            
                                                            
                                   
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                           <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">

                                                        <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>


                                                        <a href="{{url('softwarehouse')}}" class="btn default"> Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->

                        </div>
                    </div>


                    </div>
                   

                </div>
            </div>
            <!-- END CONTENT -->

        </div>
        @stop