<?php 
use \App\Http\Controllers\commonController;
?>
@extends('backend.master')
@section('pageTitle','softwarehouse')
@section('content')

 <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        <?php echo "softwarehouse List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('softwarehouse')}}">softwarehouse</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                     <div><p style="font-size: x-large;">See the controlled content -> <a target="_blanck" href="{{url('uploads/screenshots/header.png')}}">click</a></p></div>
                   
                       
                    <a href="<?php echo url('addsoftwarehouse') ?>" class="btn green"> Add New <i class="fa fa-plus"></i></a>
                    
<!--                    <div class="form-group">
                   
                        <select id="selectcategory"class="form-control" style="margin-top: 20px; align-items: center; width: 30%;">
                         <option value="all">All</option>
                         <?php 
//                         $categories = commonController::get_category('softwarehouse_category');
//                         if(isset($categories)){
//                           foreach ($categories as $category){
                              ?>
                         <option value="<?php // echo $category->id;?>"><?php // echo commonController::EnglishTranslate($category->category_name);?></option>
                         <?php //  }
//                         }?>
                     </select>         
                 </div>-->
                   
                    
                    <div class="row thumbnails" id="sortme" table="software_house">
                       <?php foreach ($softwarehouses as $softwarehouse){?>
                        <div class="col-md-3 cat category<?php echo $softwarehouse->category_id;?>" table="software_house" id="menu_<?php echo $softwarehouse->id;?>" ids="<?php echo $softwarehouse->id;?>">
                                <div class="meet-our-team" >
                                    <h3><a href="<?php echo url('editsoftwarehouse').'/'.$softwarehouse->id;?>" class="name" title=""><?php echo commonController::EnglishTranslate($softwarehouse->header);?></a> </h3>
                                    <a class="mix-preview fancybox-button" href="<?php echo $softwarehouse->image;?>" title="" data-rel="fancybox-button">

                                        <img src="<?php echo $softwarehouse->image;?>" title="" style="max-width:80%;height:150px;margin:auto;" alt="" class="img-responsive"/>
                                    </a>
                                    <div style="margin-left: 12px; margin-top: 12px;"class="team-info">

                                        <?php if($softwarehouse->featured){?>
                                        <a class="featured" title="Show" href="javascript:;"><i class="glyphicon glyphicon-star btn-default font-blue"></i></a>
                                        <?php } else {?>
                                       
                                        <a class="featured" title="Hide" href="javascript:;"><i class="glyphicon glyphicon-star-empty btn-default font-blue"></i></a>
                                        <?php } ?>

                                        &nbsp;
                                        <a class="edit" title="Edit" href="<?php echo url('editsoftwarehouse').'/'.$softwarehouse->id;?>">
                                            <i class="glyphicon glyphicon-pencil btn-default font-blue"></i></a>

                                        &nbsp;
                                        <a class="delete" title="Delete" href="javascript:;">
                                            <i class="glyphicon glyphicon-trash btn-default font-blue"></i></a>
                                        <span class="logtable_span">
                                           <?php echo commonController::EnglishTranslate($softwarehouse->description);?> 
                                        </span>

                                    </div>
                                </div>

                            </div>
                       <?php } ?>
                          
                           </div>
                     </div>
                      
                       </div>
                    

            <!-- END CONTENT -->
           
@stop
