<?php 
use App\Http\Controllers\commonController;
?>

@extends('backend.master')
@section('pageTitle','message details')
@section('content')
 

<div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet">

                                <div class="portlet-body">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs nav-tabs-lg">
                                            <li class="active">
                                                <a href="#tab_1" data-toggle="tab">
                                                    Details </a>
                                            </li>

                                        </ul>
                                        <?php
                                        $array = commonController::get_content_by_id('contact_messages', $_GET['id']);
                                        foreach ($array as $message);
                                        ?>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <div class="row">

                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="portlet blue-hoki box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-cogs"></i>Contact Details
                                                                </div>

                                                               
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name">
                                                                        Name:
                                                                    </div>
                                                                    <div class="col-md-7 value">
                                                                        <?php echo $message->sender_name; ?>
                                                                    </div>
                                                                </div>
                                                               

                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name">
                                                                        Email:
                                                                    </div>
                                                                    <div class="col-md-7 value">
                                                                        <a href="mailto:<?php echo $message->sender_email; ?>"><?php echo $message->sender_email; ?></a>
                                                                    </div>
                                                                </div>
                                                                
                                                                

                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name">
                                                                        Date:
                                                                    </div>
                                                                    <div class="col-md-7 value">
                                                                        <?php echo $message->created_at; ?>
                                                                    </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name">
                                                                        Time:
                                                                    </div>
                                                                    <div class="col-md-7 value">
                                                                        <?php echo $message->created_at; ?>
                                                                    </div>
                                                                </div>
                                                                <div class="row static-info">
                                                                    <div class="col-md-5 name">
                                                                        Message:
                                                                    </div>
                                                                    <div class="col-md-7 value">
                                                                        <?php echo $message->sender_message ?>
                                                                    </div>
                                                                </div>
                                                                <a href="<?php echo url('contactmessages') ?>" class="btn blue"> Back to Contacts <i class="fa fa-mail-reply"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div></div></div>
@stop