<?php 
use App\Http\Controllers\commonController;
?>

@extends('backend.master')
@section('pageTitle','contact messages')
@section('content')
 <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box grey-cascade">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>Contact Messages
                                    </div>

                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="btn-group pull-right">
                                                    <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">

                                                        <li>
                                                            <a   href="{{ url('deleteall')}}">
                                                                <i class="fa fa-trash-o"></i> Delete all</a>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $messages = commonController::get_content('contact_messages');
                                    ?>
                                    <table class="table table-striped table-bordered table-hover" id="sample_editable_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    #
                                                </th>
                                                <th>
                                                    Name
                                                </th>

                                                <th>
                                                    Email
                                                </th>

                                                <th>
                                                    Date
                                                </th>
                                                <th>
                                                    Time
                                                </th>

                                                <th>
                                                    Actions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                             for ($i = 0; $i<count($messages); $i++ ) {
                                                 $message =$messages[$i];
                                                ?>
                                                <tr table="contact_messages" id="<?php echo $message->id ?>">
                                                    <td>
                                                        <?php echo $i+1; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $message->sender_name; ?>
                                                    </td>

                                                    <td>
                                                        <?php echo $message->sender_email ?>
                                                    </td>

                                                    <td class="center">
                                                        <?php echo explode(' ',$message->created_at)[0]; ?>
                                                    </td>
                                                    <td class="center">
                                                        <?php echo explode(' ',$message->created_at)[1]; ?>
                                                    </td>

                                                    <td>
                                                        <!--class="edit"-->
                                                        <?php
                                                        $font_color = "font-blue";
                                                        if ($message->seen == 0) {
                                                            $font_color = "font-blue-dark";
                                                        }
                                                        ?>

                                                        <a title="Show" href="<?php echo url('show_contact/?id=').$message->id; ?>">
                                                            <i class="glyphicon glyphicon-eye-open btn-default <?php echo $font_color; ?>"></i></a>


                                                        &nbsp;
                                                        <a title="Delete" id="delete" data-data="contact_messages" data-id="{{$message->id}}" class="delete" href="javascript:;">

                                                            <i class="glyphicon glyphicon-trash btn-default font-blue"></i></a>

                                                    </td>

                                                </tr>
                                                <?php
                                            }
                                            ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
</div>
<div>
@stop
