<?php 
use \App\Http\Controllers\commonController;
?>
@extends('backend.master')
@section('pageTitle','homepagesliderblocks')
@section('content')

 <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        <?php echo "homepageslidrblocks List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('homepagesliderblocks')}}">homepagesliderblocks</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                     <div><p style="font-size: x-large;">See the controlled content -> <a target="_blanck" href="{{url('uploads/screenshots/homeslidercontent.png')}}">click</a></p></div>
                   
                       
                    <a href="<?php echo url('addhomepageslidrblocks') ?>" class="btn green"> Add New <i class="fa fa-plus"></i></a>
                    
                
                   
                    
                    <div class="row thumbnails" id="sortme" table="home_page_slider_blocks">
                       <?php foreach ($homepagesliderblockss as $homepagesliderblocks){?>
                        <div class="col-md-3 cat " table="home_page_slider_blocks" id="menu_<?php echo $homepagesliderblocks->id;?>" ids="<?php echo $homepagesliderblocks->id;?>">
                                <div class="meet-our-team" >
                                    <h3><a href="<?php echo url('edithomepageslidrblocks').'/'.$homepagesliderblocks->id;?>" class="name" title=""><?php echo commonController::EnglishTranslate($homepagesliderblocks->header);?></a> </h3>
                                    <a class="mix-preview fancybox-button" href="<?php echo $homepagesliderblocks->image;?>" title="" data-rel="fancybox-button">

                                        <img src="<?php echo url($homepagesliderblocks->image);?>" title="" style="max-width:80%;height:150px;margin:auto;" alt="" class="img-responsive"/>
                                    </a>
                                    <div style="margin-left: 12px; margin-top: 12px;"class="team-info">

                                        <?php if($homepagesliderblocks->featured){?>
                                        <a class="featured" title="Show" href="javascript:;"><i class="glyphicon glyphicon-star btn-default font-blue"></i></a>
                                        <?php } else {?>
                                       
                                        <a class="featured" title="Hide" href="javascript:;"><i class="glyphicon glyphicon-star-empty btn-default font-blue"></i></a>
                                        <?php } ?>

                                        &nbsp;
                                        <a class="edit" title="Edit" href="<?php echo url('edithomepageslidrblocks').'/'.$homepagesliderblocks->id;?>">
                                            <i class="glyphicon glyphicon-pencil btn-default font-blue"></i></a>

                                        &nbsp;
                                        <a class="delete" title="Delete" href="javascript:;">
                                            <i class="glyphicon glyphicon-trash btn-default font-blue"></i></a>
                                        <span class="logtable_span">
                                           <?php echo commonController::EnglishTranslate($homepagesliderblocks->description);?> 
                                        </span>

                                    </div>
                                </div>

                            </div>
                       <?php } ?>
                          
                           </div>
                     </div>
                      
                       </div>
                    

            <!-- END CONTENT -->
           
@stop
