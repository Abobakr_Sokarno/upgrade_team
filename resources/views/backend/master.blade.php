<?php 
use App\Http\Controllers\commonController;
if(session()->get('email')== null){
    ?>
<script>
window.location.href = "<?php echo url('/');?>";
</script>
<?php 
}

?>
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>@yield('pageTitle')</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <link rel="shortcut icon" href="images/icon.png"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES 
        
        <link rel="stylesheet" type="text/css" href="{{asset("assets/global/plugins/select2/select2.css")}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css")}}"/>
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME STYLES -->

        <link href="{{url('assets/global/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="{{url('assets/admin/layout/css/themes/darkblue.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="{{url("assets/global/plugins/bootstrap-select/bootstrap-select.min.css")}}"/>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="{{url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="{{url('assets/admin/pages/css/tasks.css')}}" rel="stylesheet" type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link href="{{url('assets/global/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{url('assets/admin/layout/css/themes/darkblue.css')}}" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="{{url('assets/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
</head>
    <body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 

        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{url('/dashboard')}}">
                        <img src="{{url('images/logo.png')}}" alt="logo" class="logo-default"/>
                    </a>
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->

                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- BEGIN NOTIFICATION DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                        <!-- END NOTIFICATION DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                            <?php
//                            require("classes/notification.php");
//                            $item = new notification();
//
//                            $contacts_array = $item->getUnSeenData("contact");
//                            $num1 = count($contacts_array);
                            ?>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-envelope-open"></i>
                                <span class="badge badge-default">
                                    <?php echo commonController::count('contact_messages'); ?> </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>You have <span class="bold"> <?php echo commonController::count('contact_messages'); ?> New</span> Messages</h3>
                                    <a href="{{url('contactmessages')}}">view all</a>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                       <?php $messages = json_decode(commonController::get_content('contact_messages')); 
                                          array_reverse($messages);
                                       foreach ($messages as $message){
                                       ?>
                                        <li>
                                            <a href="<?php echo url('show_contact').'?id='.$message->id; ?>">
                                                <span class="photo">
                                                    <i class="fa fa-bullhorn"></i>
                                                </span>
                                                <span class="subject">
                                                    <span class="from">
                                                        <?php echo $message->sender_name ?></span>
                                                    <span class="time"><?php echo $message->created_at;?></span>
                                                </span>
                                                <span class="message">
                                                    <?php echo  $message->sender_message; ?>
                                                </span>
                                            </a>
                                        </li>
                                      <?php } ?>

                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- END NOTIFICATION DROPDOWN -->

                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                                <span class="username username-hide-on-mobile">
                                    <?php echo session()->get('name'); ?> </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="<?php echo url('edituser').'/'. session()->get('id');?>">
                                        <i class="icon-user"></i> Edit My Info </a>
                                </li>


                                <li>
                                    <a href="{{url('/')}}" target="_blank">
                                        <i class="icon-rocket"></i> Visit Website 
                                    </a>
                                </li>
                                <li class="divider">
                                </li>

                                <li>
                                    <a href="{{url('/logout')}}">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->

                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->   <div class="page-container">
            <!-- BEGIN SIDEBAR -->

            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="sidebar-toggler-wrapper">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler">
                            </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                        <div class=""></div>
                        <?php
                        $menu = array(
                            'dashboard' => array('Dashboard', 'icon-home', '', ''),
                            'users' => array('Backend Users', 'icon-user', 'users', ''),
                            'servicecategory' => array('Services categories', 'icon-docs', 'service_category', ''),
                            'services' => array('Services', 'icon-puzzle', 'services', ''),
                            'solutioncategory' => array('Solution categories', 'icon-docs', 'solution_category', ''),
                            'solutions' => array('Solutions', 'icon-puzzle', 'solutions', ''),
                            'gallerycategory' => array('Gallery categories', 'icon-layers', 'gallery_category', ''),
                            'gallerys' => array('Gallery', 'icon-picture', 'gallery', ''),
                            'softwarehouse' => array('Software house', 'icon-calculator', 'software_house', ''),
                            'clients' => array('Clients', 'icon-share', 'our_clients', ''),
                            'feature' => array('Features', 'icon-feed', 'features', ''),
                            'homepageslidrblocks' => array('Home page slider blocks', 'icon-compass', 'home_page_slider_blocks', ''),
                            'team' => array('Team', 'icon-emoticon-smile', 'team', ''),
                            'testmonial' => array('Testmonial', 'icon-bag', 'testmonial', ''),
                            'ourskills' => array('Our skills', 'icon-cup', 'our_skills', ''),
                            'news' => array('News', 'icon-paper-plane', 'news', ''),
                            'slider' => array('Slider', 'icon-film', 'slider', ''),
                            'aboutus' => array('About us', 'icon-book-open', 'about_us', ''),
                            'contactus' => array('Contact us', 'icon-call-in', 'contact_us', ''),
                            'branches' => array('Branches', 'icon-direction', 'branches', ''),
                            'whatwedo' => array('What we do', 'icon-feed', 'what_we_do', ''),
                            'headertext' => array('Headers text', 'icon-pencil', 'header_text', ''),
                            'othertextsettings' => array('Other text settings', 'icon-settings', 'other_text_settings', ''),
                            'footertext' => array('Footer text', 'icon-pencil', 'footer_text', ''),
                            'contactmessages' => array('Messages', 'icon-envelope-open', 'contact_messages', ''),
                           // 'viewproducts' => array('Products List', 'icon-user', 'products', ''),
                          //  'viewitems' => array('Items List', 'icon-user', 'items', ''),
                        );

                        if (!empty($menu)) {
                            foreach ($menu as $path => $info) {
                                $reg = "/" . $path . "/";
                                $title = $info[0];
                                $icon_class = $info[1];
                                $table_name = $info[2];
                                $language_flag = $info[3];
                                $server_request  = str_replace("/upgrade_team/", "",$_SERVER['REQUEST_URI']);
                                
                                $match = preg_match($reg, $server_request);

                                if ($match <= 0) {
                                    $add_path = str_replace(".php", "_add.php", $path);
                                    $add_reg = "/" . $add_path . "/";
                                    $match = preg_match($add_reg,$server_request);

                                    if ($match <= 0) {
                                        $edit_path = str_replace(".php", "_edit.php", $path);
                                        $edit_reg = "/" . $edit_path . "/";
                                        $match = preg_match($edit_reg, $server_request);
                                    }
                                }


                                if ($match > 0) {
                                    ?>
                                    <li class="start active open">
                                        <a href="<?php echo url($path); ?>">
                                            <span class="badge badge-roundless badge-danger"><?php
                                                if ($table_name != '') {
                                             echo $count = commonController::count($table_name);
                                             
                                                }
                                                ?></span>
                                            <i class="<?php echo $icon_class ?>"></i>
                                            <span class="title"><?php echo $title ?></span>
                                            <span class="selected"></span>

                                        </a>

                                    </li>
                                    <?php
                                } else {
                                    ?>
                                    <li >
                                        <a href="<?php echo url($path); ?>">
                                            <span class="  badge badge-roundless badge-danger"><?php
                                                if ($table_name != '') {
                                                    $count = commonController::count($table_name);

                                                    echo $count;
                                                }
                                                ?></span>
                                            <i class="<?php echo $icon_class ?>"></i>
                                            <span class="title"><?php echo $title ?></span>
                                        </a>

                                    </li>
                                    <?php
                                }
                            }
                        }
                        ?>


                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    @yield('content')

                    <script src="{{url('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
                    <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
                    <!-- END CORE PLUGINS -->
                    <script type="text/javascript" src="{{url('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js')}}"></script>
                    <script type="text/javascript" src="{{url('assets/global/plugins/bootstrap-markdown/lib/markdown.js')}}"></script>

                    <!-- BEGIN PAGE LEVEL PLUGINS -->
                    
                    <!-- BEGIN PAGE LEVEL PLUGINS -->
                    <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
                    <!-- END PAGE LEVEL PLUGINS -->
                    <!-- BEGIN PAGE LEVEL SCRIPTS -->
                    <script src="{{url('assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/admin/pages/scripts/index.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/admin/pages/scripts/tasks.js')}}" type="text/javascript"></script>
                    <script src="{{url('js/user-table-editable.js')}}"></script>
                    <script src="{{url('assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
                    <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
                    <script src="{{url('assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
                    <script src="{{url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
                    <!-- END CORE PLUGINS -->
                    <!-- BEGIN PAGE LEVEL PLUGINS -->
                    <script type="text/javascript" src="{{url('assets/global/plugins/select2/select2.min.js')}}"></script>
                    <script type="text/javascript" src="{{url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
                    <script type="text/javascript" src="{{url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
                    <!-- END PAGE LEVEL PLUGINS -->
                    <!-- BEGIN PAGE LEVEL SCRIPTS -->
                    <script language="JavaScript" type="text/javascript" src="{{url('assets/admin/pages/scripts/components-form-tools.js')}}"></script>
                    <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
	
        <script src="{{url('js/sortme.js')}}" type="text/javascript"></script>
                    <script>
                        var base_url = '<?php echo url("/") ?>';
                    </script>
        <script src="{{url('js/custom.js')}}" type="text/javascript"></script>
        <script src="{{url('js/datafilter.js')}}" type="text/javascript"></script>
      
        <!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function () {
    Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    QuickSidebar.init(); // init quick sidebar
    Demo.init(); // init demo features
    TableEditable.init();
   });

$('#image').on('change', function () {
    $("#imageExt").val(document.getElementById('image').value);
});
$("#image").change(function () {
    readURL(this);
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageExt').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function cancelBtn() {
    $("#imageExt").attr('src', 'http://localhost:8000/images/-text.png');
}

                    </script>
                    <!-- END JAVASCRIPTS -->
                    </body>

                    <!-- END BODY -->
                    <div class="page-footer">
                        <div class="page-footer-inner">
                            2018 &copy; UPGRADE TEAM. Admin Panel.
                        </div>
                        <div class="scroll-to-top">
                            <i class="icon-arrow-up"></i>
                        </div>
                    </div>	
                    </html>
