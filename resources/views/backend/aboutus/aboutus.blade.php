<?php 
use \App\Http\Controllers\commonController;
?>
@extends('backend.master')
@section('pageTitle','about us')
@section('content')

 <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        <?php echo "aboutus List"; ?> <small><?php echo "Info & statstics"; ?></small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="javascript:void(0)">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{url('aboutus')}}">aboutus</a>
                            </li>
                        </ul>

                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                       
                   
                    <br><br>
                     <div><p style="font-size: x-large;">See the controlled content -> <a target="_blanck" href="{{url('uploads/screenshots/about_us.png')}}">click</a></p></div>
                   
                     <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                   English Header
                                                </th>

                                               
                                                
                                                <th>
                                                    English Description
                                                </th>

                                               
                                                <th>
                                                    Image
                                                </th>
                                                <th>
                                                    link Description
                                                </th>


                                                <th>
                                                    Actions
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($aboutus as $about) {
                                                ?>
                                                <tr table="aboutus" id="<?php echo $about->id ?>">
                                                    <td>
                                                        <?php echo commonController::EnglishTranslate($about->header) ?>
                                                    </td>
                                                    
                                                    <td>
                                                        <?php echo commonController::EnglishTranslate($about->description) ?>
                                                    </td>

                                                    <td style="width: 40%;">
                                                        <img src="<?php echo url($about->image) ?>" style="width: 60%;">
                                                    </td>
                                                    <td>
                                                        <?php echo $about->link ?>
                                                    </td>

                                                    <td>
                                                        <a title="Edit" href="<?php echo url('editaboutus') .'/'. $about->id ?>">
                                                            <i class="glyphicon glyphicon-pencil btn-default font-blue"></i></a>
                                                        &nbsp;

                                          

                                                    </td>

                                                </tr>
                                                <?php
                                            }
                                            ?> 

                                        </tbody>
                                    </table>
                     </div>
                      
                       </div>
                    

            <!-- END CONTENT -->
           
@stop
