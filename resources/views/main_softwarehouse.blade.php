<?php 
use App\Http\Controllers\commonController;
$other_texts = commonController::get_content('other_text_settings');
$header_texts = commonController::get_content('header_text');$i=1;
foreach ($header_texts as $text){$header_info[$i++]=$text->text_value;}
$softwarehouse = commonController::get_content_sortted('software_house');
//$software_category = commonController::get_content_sortted('software_category');
if(isset($_GET['title'])){
  $softwarehouse = commonController::get_content_sortted_by_category('software_house',$_GET['title']);  
}
$info = array();
$i=0;
foreach ($other_texts as $text){
 $info[$i++] = commonController::EnglishTranslate($text->text_value);
}
$nav_bar = explode('[sep]', $header_info[12]);
?>
@extends('master')
@section('pageTitle','Upgrade Team')
@section('content')
        <!-- Breadcrumb Area -->
        <section id="breadcrumb-area" style="background:url('<?php echo $header_info[15];?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1><?php echo $nav_bar[4];?></h1>
                        <ul class="breadcrumb-nav list-inline">
                            <li><a href="{{url('/')}}"><?php echo $nav_bar[0];?></a></li>
                            <li class="active"><?php echo $nav_bar[4];?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </header>
    <!-- End: Header
    ============================= -->

    <!-- Start: Content Section
    ============================= -->
    <section id="blog-content">
        <div class="container">

            <div class="row">
                <!-- Blog Content -->
                <div class="col-lg-9 col-md-12 solutions">
                    <?php $i=-1; foreach ($softwarehouse as $software){
                    $display='block;'; 
                    if($i>1){$display='none;';}
                    $i++; 
                    ?>
                    <article class="blog-post page<?php echo (int)($i/3)+1;?>" style="display: <?php echo $display;?>">
                        <div class="post-thumb">
                            <img src="<?php echo url($software->image);?>" alt="">
                            <div class="post-overlay">
                                <a href="<?php echo url('software_house').'?title='.$software->header;?>"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <div class="post-content">
                            <h4 class="post-title"><a href="<?php echo url('software_house').'?title='.$software->header;?>"><?php echo commonController::EnglishTranslate($software->header);?></a></h4>
                            <p class="content">
                                <?php echo commonController::EnglishTranslate($software->description);?>
                            </p>
                            <a href="<?php echo url('software_house').'?title='.$software->header;?>" class="continue-reading">Continue reading <i class="fa fa-share"></i></a>
                        </div>

                    </article>
                    <?php } ?>
                    <nav class="pagination" aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <?php for($i=0; $i<count($softwarehouse)/3; $i++){?>
                            <button onclick="paginate('page<?php echo $i+1;?>')"class="page-link page<?php echo $i+1;?> "><?php echo $i+1;?></button>
                            <?php } ?>
                            <li class="page-item more-page">
                                <a class="page-link" href="#">View More <i class="icofont icofont-long-arrow-right"></i></a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <!-- Sidebar -->
                <div class="col-lg-3 col-md-12">
                    <section class="sidebar">
                        <aside class="widget widget-search">
                            <h5 class="widget-title">Search</h5>
                            <form class='search-form' action='search' method='post'>
                                {!!csrf_field()!!}
                                <input type='hidden' name='form-name' value='form 1' />
                                <input type="search" name="phrase"placeholder="Search softwarehouse.html">
                                <input type="button" class="search-btn" value="Go">
                            </form>
`
                            
                        </aside>
                        

                    </section>
                </div>
            </div>

        </div>
    </section>
    <!-- End: Content Section
    ============================= -->
   
 @stop