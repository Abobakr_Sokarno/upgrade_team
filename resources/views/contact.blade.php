<?php 
use App\Http\Controllers\commonController;
$contactus = commonController::get_content('contact_us');
$other_texts = commonController::get_content('other_text_settings');
$header_texts = commonController::get_content('header_text');$i=1;
foreach ($header_texts as $text){$header_info[$i++]=$text->text_value;}
$branches = commonController::get_content('branches');
$other_texts = commonController::get_content('other_text_settings');
foreach ($contactus as $contactus);
$info = array();
$i=0;
foreach ($other_texts as $text){
 $info[$i++] = commonController::EnglishTranslate($text->text_value);
}
$nav_bar = explode('[sep]', $header_info[12]);
?>
@extends('master')
@section('pageTitle','Upgrade Team')
@section('content')
        <!-- Breadcrumb Area -->
        <section id="breadcrumb-area" style="background:url('<?php echo $header_info[19];?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1><?php echo $nav_bar[6];?></h1>
                        <ul class="breadcrumb-nav list-inline">
                            <li><a href="{{url('/')}}"><?php echo $nav_bar[0];?></a></li>
                            <li class="active"><?php echo $nav_bar[6];?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </header>
    <!-- End: Header
    ============================= -->

    <!-- End: Contact Us Office
    ============================= -->
    <section id="contact-us-page">
        <div class="container">

            <div class="row">
                <?php foreach ($branches as $branch){?>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-lg-0 mb-5">
                    <div class="office">
                        <i class="icon icofont icofont-location-pin"></i>
                        <h4><?php echo commonController::EnglishTranslate($branch->map_address);?></h4>
                        <ul>
                            <li>Street :  <?php echo commonController::EnglishTranslate($branch->street);?></li>
                            <li>City : <?php echo commonController::EnglishTranslate($branch->city);?></li>
                        </ul>
                        <ul class="contact-social"> 
                            <li><a href="<?php echo $branch->twitter_link;?>" target="_blank"><i class="icofont icofont-social-twitter"></i></a></li>
                            <li><a href="<?php echo $branch->linkedin_link;?>" target="_blank"><i class="icofont icofont-social-linkedin"></i></a></li>
                            <li><a href="<?php echo $branch->twitter_link;?>" target="_blank"><i class="icofont icofont-social-tumblr"></i></a></li>
                            <li><a href="<?php echo $branch->twitter_link;?>" target="_blank"><i class="icofont icofont-social-dribbble"></i></a></li>
                            <li><a href="<?php echo $branch->facebook_link;?>" target="_blank"><i class="icofont icofont-social-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <?php } ?>
            </div>

        </div>
    </section>
    <!-- End: Contact Us Office
    ============================= -->

    <!-- Start: Contact Us
    ============================= -->

    <section id="contact-area">
       
        <div class="contact-box">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="contact-us-info">
                        <h3><?php echo $info[8]?></h3>
                        <ul class="contact-info">
                            <li>
                                <i class="icofont icofont-location-pin"></i>
                                <p><?php echo commonController::EnglishTranslate($contactus->address);?></p>
                            </li>
                            <li>
                                <i class="icofont icofont-email"></i>
                                <p><?php echo $contactus->email;?></p>
                            </li>
                            <li>
                                <i class="icofont icofont-ui-call"></i>
                                <p><?php echo explode(',', $contactus->phone)[0];?>
                                    <br><?php echo explode(',', $contactus->phone)[1];?></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12">
                    <div class="get-in-touch">
                        <h3><?php echo $info[9];?></h3>
                        <p><?php echo $info[10];?></p>
                        <form action="{{url('contactaction')}}" method="POST" id="contactForm">
                            {!!csrf_field()!!}
                            <div id="success" style="color:#33ce67; font-size: x-large;"></div>
                            <div id="fail" style="color:red; font-size: x-large;"></div>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your name" >
                            <input type="text" name="email" class="form-control" id="email" placeholder="Email here" >
                            <textarea name="message" class="form-control" rows="1" placeholder="Type your message" ></textarea>
                            <button type="submit" class="boxed-btn"> Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Contact Us
    ============================= -->

    <!-- Start: Footer Sidebar
    ============================= -->
 @stop