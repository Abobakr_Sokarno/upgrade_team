<?php 
use App\Http\Controllers\commonController;
$other_texts = commonController::get_content('other_text_settings');
$header_texts = commonController::get_content('header_text');$i=1;
foreach ($header_texts as $text){$header_info[$i++]=$text->text_value;}
if(isset($_GET['title'])){
$service = commonController::get_content_sortted_by_title('services', $_GET['title'],'header');
if(json_encode($service)=="null"){
    return redirect()->guest('/');
}
$related = commonController::get_content_sortted_by_category('services', $service->category_id);
}else {
 return redirect()->route('home'); 
}

$service_category = commonController::get_content_sortted('service_category');
$clients = commonController::get_content_sortted('our_clients');
$info = array();
$i=0;
foreach ($other_texts as $text){
 $info[$i++] = commonController::EnglishTranslate($text->text_value);
}
$nav_bar = explode('[sep]', $header_info[12]);
?>
@extends('master')
@section('pageTitle','Upgrade Team')
@section('content')
        <!-- Breadcrumb Area -->
        <section id="breadcrumb-area" style="background:url('<?php echo $header_info[15];?>');">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1><?php echo $nav_bar[2];?></h1>
                        <ul class="breadcrumb-nav list-inline">
                            <li><a href="{{url('/')}}"><?php echo $nav_bar[0];?></a></li>
                            <li class="active"><?php echo $nav_bar[2];?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </header>
    <!-- End: Header
    ============================= -->

    <!-- Start: Content Section
    ============================= -->
    <section id="blog-content" class="single-blog-area">
        <div class="container">

            <div class="row">
                <!-- Blog Content -->
                <div class="col-lg-9 col-md-12">
                    <article class="blog-post">
                        <div class="post-thumb">
                            <img src="<?php echo url($service->image);?>" alt="">
                        </div>
                        <ul class="meta-info">
                            <li class="post-tag"><a href="<?php echo url('main_services')?>"><i class="icofont icofont-ui-tag"></i>service</a></li>
                        </ul>
                        <div class="post-content">
                            <h4 class="post-title"><?php echo commonController::EnglishTranslate($service->header);?></h4>
                        <?php echo commonController::EnglishTranslate($service->description);?>
                        </div>
                        <div class="tag-share">
                            <div class="row">
                                <div class="col-md-12">
                                   <h3>Related service</h3>
                                </div>
                            </div>
                        </div>
                        <!-- Post Comment Area -->
                        <div class="post-comment-area">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="media-left">
                                        <a href="#"><img src="{{url('img/blog/avatar01.png')}}" alt=""></a>
                                    </div>
                               <div class="media-body">
                              <?php foreach ($related as $item){?>
                                   <a href="service?title=<?php echo $item->header;?>"><p><?php echo commonController::EnglishTranslate($item->header);?></p> </a>
                              <?php }?>    
                               </div>
                                </li>
                            </ul>
                        </div>

   <section id="our-client" class="section-padding">
        <div class="container">


            <div class="row">
                <div class="col-md-12">
                    <div class="client-carousel">
                        <?php foreach ($clients as $client){?>
                        <div class="single-client">
                            <div class="inner-client">
                                <img src="<?php  echo $client->image?>" alt="">
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>

        </div>
    </section>



                    </article>
                </div>

                <!-- Sidebar -->
                <div class="col-lg-3 col-md-12">
                    <section class="sidebar">
                        <aside class="widget widget-categories">
                            <h5 class="widget-title">Categories</h5>
                            <ul>
                                <?php foreach ($service_category as $category){?>
                                <li><a href="main_services?title=<?php echo $category->category_name;?>"><?php echo commonController::EnglishTranslate($category->category_name);?><i class="icofont icofont-long-arrow-right"></i></a></li>
                                <?php } ?>
                            </ul>
                        </aside>

                    </section>
                </div>
            </div>

        </div>
    </section>
    <!-- End: Content Section
    ============================= -->
 @stop