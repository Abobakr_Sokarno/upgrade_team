<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class userRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $variable = $this->input('password');
         $rules = [
            'email'=>'required|email|max:255555',
            'password'=>'required|max:255555|min:6',
            'cpassword'=>'required|max:255555|in:'.$variable,
        ];
        
        return $rules;
    }
}
