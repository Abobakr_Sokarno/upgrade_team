<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class changepasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $variable = $this->input('cpassword');
        $variable2 = session()->get('code');
        return [
            'code'=>'required|in:'.$variable2,
            'password'=>'required|max:255555|min:6',
            'cpassword'=>'required|max:255555|in:'.$variable,
        ];
    }
}
