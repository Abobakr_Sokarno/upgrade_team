<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class branchesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'mapaddress'=>'required|max:255555',
             //'arabicmapaddress'=>'required|max:255555',
             //'arabiccity'=>'required|max:255555',
             'city'=>'required|max:255555',
             //'arabicstreet'=>'required|max:255555',
             'street'=>'required|max:255555',
        ];
    }
}
