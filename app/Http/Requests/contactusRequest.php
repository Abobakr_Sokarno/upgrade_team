<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class contactusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'=>'required|max:255555',
            //'arabicaddress'=>'required|max:255555',
            'phone'=>'required|max:255555',
            'email'=>'required|max:255555|email',
        ];
    }
}
