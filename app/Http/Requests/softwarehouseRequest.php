<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class softwarehouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'header'=>'required|max:255555',
            //'arabicheader'=>'required|max:255555',
            'description'=>'required|max:255555',
            //'arabicdescription'=>'required|max:255555',
        ];
    }
}
