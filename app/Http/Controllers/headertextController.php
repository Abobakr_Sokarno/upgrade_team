<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Header_text;
use App\Http\Requests\headertextRequest;
use App\Http\Controllers\Redirect;
use Intervention\Image\ImageManagerStatic as Image;

class headertextController extends Controller
{
      public function index()
    {
        $headertext = new Header_text();
        return view('backend.headertext.headertext')->with('header_text', $headertext->orderBy('id','ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(headertextRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'headertext', 'header')>=1){
            return redirect()->route('addheadertext',['validate'=>'<div class="alert alert-danger"><p>This headertext was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addheadertext',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $headertext = new Header_text();
        $headertext->header = $header.'[EN:]'.$arabicheader;
        $headertext->description = $description.'[EN:]'.$arabicdescription;
        $headertext->image = 'uploads/'.$image;
        $headertext->save();
        return redirect('headertext');
        
    }
        public   function upload_file(headertextRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(1400, 730);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
       
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $headertext = Header_text::find($id);
       // echo json_encode($headertext);
        return view('backend.headertext.edit_headertext')->with('headertext', $headertext);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(headertextRequest $request, $id)
    {
         $value = $request->input('value');
         if($id==12 || $id == 13){
             $value = $request->input('home').'[sep]'.$request->input('solutions').'[sep]'.$request->input('services').'[sep]'.
                      $request->input('gallery').'[sep]'. $request->input('softwarehouse').'[sep]'. 
                      $request->input('aboutus').'[sep]'.$request->input('contactus');
         }
         if($request->hasFile('image')){
            $image = $this->upload_file($request); 
             if( $image != "-text.png")
            $value = 'uploads/'.$image;
         }
        $headertext =  Header_text::find($id);
        //echo $value;
        $headertext->text_value = $value;
        $headertext->save();
       // echo $value;
        return redirect('headertext');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
