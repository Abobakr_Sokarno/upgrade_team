<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\commonController;
use App\Service_category;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\servicecategoryRequest;
class servicecategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = new Service_category(); 
        return view('backend.service_category.service_categories')->with('categories', $categories->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(servicecategoryRequest $request)
    {
        $name = $request->input('name'); 
        $arabicname = $request->input('arabicname');
        if(commonController::validateTable($name.'[EN:]'.$arabicname, 'service_category', 'category_name')>=1){
            return redirect()->route('addservicecategory',['validate'=>'<div class="alert alert-danger"><p>This category was added before</p></div>']);
        }
        $category = new Service_category();
        $category ->category_name = $name . '[EN:]' . $arabicname;
        $category->save();
        return redirect('servicecategory');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Service_category::find($id);
        return view('backend.service_category.edit_service_category')->with('category',$category);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(servicecategoryRequest $request, $id)
    {
         $name = $request->input('name'); 
         $arabicname = $request->input('arabicname');
         if(commonController::validateTable($name.'[EN:]'.$arabicname, 'service_category', 'category_name')>=2){
            return redirect()->route('addservicecategory',['validate'=>'<div class="alert alert-danger"><p>This category was added before</p></div>']);
        }
         $category = Service_category::find($id);
         $category ->category_name = $name . '[EN:]' . $arabicname;
         $category->save();
        return redirect('servicecategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
