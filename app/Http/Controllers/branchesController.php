<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branches;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\branchesRequest;
use App\Http\Controllers\Redirect;
class BranchesController extends Controller
{
    public function index()
    {
        $branches = new Branches();
        return view('backend.branches.branches')->with('branches', $branches->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(branchesRequest $request)
    {
        $city = $request->input('city');
        $arabiccity = $request->input('arabiccity');
        $street = $request->input('street');
        $arabicstreet = $request->input('arabicstreet');
        $mapaddress = $request->input('mapaddress');
        $arabicmapaddress = $request->input('arabicmapaddress');
        $twitterlink = $request->input('twitter');
        $linkedinlink = $request->input('linkedin');
        $facebooklink = $request->input('facebook');
       
        $branches = new Branches();
        $branches->street = $street.'[EN:]'.$arabicstreet;
        $branches->city = $city.'[EN:]'.$arabiccity;
        $branches->map_address = $mapaddress.'[EN:]'.$arabicmapaddress;
        $branches->facebook_link = $facebooklink;
        $branches->twitter_link = $twitterlink;
        $branches->linkedin_link = $linkedinlink;
        $branches->save();
        return redirect('branches');
        
    }
        public   function upload_file(branchesRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(175, 37);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $branches = Branches::find($id);
       // echo json_encode($branches);
        return view('backend.branches.edit_branches')->with('branches', $branches);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(branchesRequest $request, $id)
    {
     
        $city = $request->input('city');
        $arabiccity = $request->input('arabiccity');
        $street = $request->input('street');
        $arabicstreet = $request->input('arabicstreet');
        $mapaddress = $request->input('mapaddress');
        $arabicmapaddress = $request->input('arabicmapaddress');
        $twitterlink = $request->input('twitter');
        $linkedinlink = $request->input('linkedin');
        $facebooklink = $request->input('facebook');
        
        $branches = Branches::find($id);
      
        $branches->street = $street.'[EN:]'.$arabicstreet;
        $branches->city = $city.'[EN:]'.$arabiccity;
        $branches->map_address = $mapaddress.'[EN:]'.$arabicmapaddress;
        $branches->facebook_link = $facebooklink;
        $branches->twitter_link = $twitterlink;
        $branches->linkedin_link = $linkedinlink;
        $branches->save();
       
        return redirect('branches');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
