<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About_us;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\aboutusRequest;
class AboutusController extends Controller
{
   public function index()
    {
        $aboutus = new About_us();
        return view('backend.aboutus.aboutus')->with('aboutus', $aboutus->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(aboutusRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $mapaddress = $request->input('mapaddress');
        $arabicmapaddress = $request->input('arabicmapaddress');
        $twitterlink = $request->input('twitter');
        $linkedinlink = $request->input('linkedin');
        $facebooklink = $request->input('facebook');
       
        $aboutus = new About_us();
        $aboutus->description = $description.'[EN:]'.$arabicdescription;
        $aboutus->header = $header.'[EN:]'.$arabicheader;
        $aboutus->map_address = $mapaddress.'[EN:]'.$arabicmapaddress;
        $aboutus->facebook_link = $facebooklink;
        $aboutus->twitter_link = $twitterlink;
        $aboutus->linkedin_link = $linkedinlink;
        $aboutus->save();
        return redirect('aboutus');
        
    }
        public   function upload_file(aboutusRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(499,548);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aboutus = About_us::find($id);
       // echo json_encode($aboutus);
        return view('backend.aboutus.edit_aboutus')->with('aboutus', $aboutus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(aboutusRequest $request, $id)
    {
     
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $shortdescription = $request->input('shortdescription');
        $arabicshortdescription = $request->input('arabicshortdescription');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $link = $request->input('link');
        $image = $this->upload_file($request);
        
        $aboutus = About_us::find($id);
      
        $aboutus->description = $description.'[EN:]'.$arabicdescription;
        $aboutus->short_description = $shortdescription.'[EN:]'.$arabicshortdescription;
        $aboutus->header = $header.'[EN:]'.$arabicheader;
        $aboutus->link = $link;
        if($image != "-text.png"){
        $aboutus->image = 'uploads/'.$image;
        }
        $aboutus->save();
       
        return redirect('aboutus');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
