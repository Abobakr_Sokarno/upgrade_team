<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Features;
use App\feature_category;
use App\Http\Requests\featureRequest;
use App\Http\Controllers\Redirect;

use Intervention\Image\ImageManagerStatic as Image;
class FeatureController extends Controller
{
      public function index()
    {
        $feature = new Features();
        return view('backend.feature.feature')->with('features', $feature->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(featureRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'features', 'header')>=1){
            return redirect()->route('addfeature',['validate'=>'<div class="alert alert-danger"><p>This feature was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addfeature',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $feature = new Features();
        $feature->header = $header.'[EN:]'.$arabicheader;
        $feature->description = $description.'[EN:]'.$arabicdescription;
        $feature->image = 'uploads/'.$image;
        $feature->save();
        return redirect('feature');
        
    }
        public   function upload_file(featureRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(38, 31);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feature = Features::find($id);
       // echo json_encode($feature);
        return view('backend.feature.edit_feature')->with('feature', $feature);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(featureRequest $request, $id)
    {
         $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
         
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'features', 'header')>=2){
            return redirect()->route('addfeature',['validate'=>'<div class="alert alert-danger"><p>This feature was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $feature =  Features::find($id);
        $feature->header = $header.'[EN:]'.$arabicheader;
        $feature->description = $description.'[EN:]'.$arabicdescription;

        if($image != "-text.png"){
        $feature->image = 'uploads/'.$image;
        }
        $feature->save();
        return redirect('feature');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
