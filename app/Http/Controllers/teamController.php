<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\team_category;
use App\Http\Requests\teamRequest;
use App\Http\Controllers\Redirect;
use Intervention\Image\ImageManagerStatic as Image;
class teamController extends Controller
{
    public function index()
    {
        $team = new team();
        return view('backend.team.team')->with('team', $team->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(teamRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $position = $request->input('position');
        $arabicposition = $request->input('arabicposition');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $facbebooklink = $request->input('facebooklink');
        $twitterlink= $request->input('twitterlink');
        $linkedinlink = $request->input('linkedinlink');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'team', 'header')>=1){
            return redirect()->route('addteam',['validate'=>'<div class="alert alert-danger"><p>This team was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addteam',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $team = new team();
        $team->header = $header.'[EN:]'.$arabicheader;
        $team->description = $description.'[EN:]'.$arabicdescription;
        $team->position = $position.'[EN:]'.$arabicposition;
        $team->facebook_link = $facbebooklink;
        $team->twitter_link = $twitterlink;
        $team->linkedin_link = $linkedinlink;
        $team->image = url('/').'/uploads/'.$image;
        $team->save();
        return redirect('team');
        
    }
    public   function upload_file(teamRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_resize = Image::make($image->getRealPath()); 
            $image_resize->resize(678, 678);
            $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
            $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = team::find($id);
       // echo json_encode($team);
        return view('backend.team.edit_team')->with('team', $team);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(teamRequest $request, $id)
    {
         $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $position = $request->input('position');
        $arabicposition = $request->input('arabicposition');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $facbebooklink = $request->input('facebooklink');
        $twitterlink= $request->input('twitterlink');
        $linkedinlink = $request->input('linkedinlink');
        $image = $this->upload_file($request);
         
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'team', 'header')>=2){
            return redirect()->route('addteam',['validate'=>'<div class="alert alert-danger"><p>This team was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $team =  team::find($id);
        $team->header = $header.'[EN:]'.$arabicheader;
        $team->description = $description.'[EN:]'.$arabicdescription;
        $team->position = $position.'[EN:]'.$arabicposition;
        $team->facebook_link = $facbebooklink;
        $team->twitter_link = $twitterlink;
        $team->linkedin_link = $linkedinlink;
        if($image != "-text.png"){
        $team->image = url('/').'/uploads/'.$image;
        }
        $team->save();
        return redirect('team');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
