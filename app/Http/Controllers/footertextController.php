<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Footer_text;
use App\Http\Requests\footertextRequest;
use App\Http\Controllers\Redirect;

use Intervention\Image\ImageManagerStatic as Image;
class footertextController extends Controller
{
    public function index()
    {
        $footertext = new Footer_text();
        return view('backend.footertext.footertext')->with('footer_text', $footertext->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(footertextRequest $request)
    {
        $footer = $request->input('footer');
        $arabicfooter = $request->input('arabicfooter');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($footer.'[EN:]'.$arabicfooter, 'footertext', 'footer')>=1){
            return redirect()->route('addfootertext',['validate'=>'<div class="alert alert-danger"><p>This footertext was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addfootertext',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $footertext = new Footer_text();
        $footertext->footer = $footer.'[EN:]'.$arabicfooter;
        $footertext->description = $description.'[EN:]'.$arabicdescription;
        $footertext->image = 'uploads/'.$image;
        $footertext->save();
        return redirect('footertext');
        
    }
        public   function upload_file(footertextRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(1920, 1080);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $footertext = Footer_text::find($id);
       // echo json_encode($footertext);
        return view('backend.footertext.edit_footertext')->with('footertext', $footertext);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(footertextRequest $request, $id)
    {
         $value = $request->input('value');
         if($id==12 || $id == 13){
             $value = $request->input('home').'[sep]'.$request->input('solutions').'[sep]'.$request->input('services').'[sep]'.
                      $request->input('gallery').'[sep]'. $request->input('softwarehouse').'[sep]'. 
                      $request->input('aboutus').'[sep]'.$request->input('contactus');
         }
         if($request->hasFile('image')){
            $image = $this->upload_file($request); 
             if( $image != "-text.png")
            $value = 'uploads/'.$image;
        $footertext =  Footer_text::find($id);
        $footertext->text_value = $value;
        $footertext->save();
         }else if ($id !=8 || $id!=11 || $id!=12){
        $footertext =  Footer_text::find($id);
        //echo $value;
        $footertext->text_value = $value;
        $footertext->save();
         }
         return redirect('footertext');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
