<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\commonController;
use App\Users;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\userRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\changepasswordRequest;
use DB;
class userController extends Controller
    {
    public function changepassword(changepasswordRequest $request){
       $password =  $request->input('password');
       $email = session()->get('cemail');
       $res = array();
       $res['res']=DB::table('users')->where('email',$email)->update(['password' => md5($password)]);
       return redirect('/admin');
    
        
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function forgetpassword(Request $request){
        $email = $request->input('email');
        $res = array(); 
        $user =  Users::where('email',$email);
        if(!$user->count()){
            $res['res'] = 0;
            echo json_encode($res);
        }else{
            $user2 = DB::table('users')->where('email',$email)->get();
            foreach ($user2 as $user2);
            $code =rand(0, 9999);
            session(['code'=>$code,'cemail'=>$email]);
            Mail::send('forgetpasswordmail',array('user'=>$user2,'code'=>$code),function($message){
            $message->to(\Illuminate\Support\Facades\Input::get('email'))->subject('welcom to upgrade team');
            });
             $res['res'] = 1;
            echo json_encode($res);
           
        }
        
        
        
    }
    public function index()
    {
        $users = new Users;
        return view('backend.users.users')->with('users',$users->get());
    }

     public function check(Request $request){
         $email = $request->input('email');
         $password =  $request->input('password');
               

         $user = Users::where('email',$email)->where('password',md5($password));
         if($user->count()){
             foreach ($user->get() as $us);
             session(['email'=>$email,'name'=>$us->name,'id'=>$us->id]);
         }
         $res['count'] = $user->get(); 
         echo json_encode($res);
         return;
        //
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(userRequest $request){
        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $cpassword = $request->input('cpassword');
         if(commonController::validateTable($email, 'users', 'email')>=1){
            return redirect()->route('addusers',['validate'=>'<div class="alert alert-danger"><p>This email is already registered</p></div>']);
        }
        
        $user = new Users();
        $user->name = $name;
        $user->email = $email;
        $user->password = md5($password);
        $user->save();
        $user2 =  array();
        $user2['name'] = $name;
        $user2['email'] = $email;
        $user2['password'] = $password;
        
        Mail::send('mailers',array('user'=> $user2),function($message){
            $message->to(\Illuminate\Support\Facades\Input::get('email'))->subject('welcom to upgrade team');
        });
        return redirect('users');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Users::find($id);
        return view('backend.users.edituser')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(userRequest $request, $id)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $cpassword = $request->input('cpassword');
         if(commonController::validateTable($email, 'users', 'email')>=2){
            return redirect()->route('addusers',['validate'=>'<div class="alert alert-danger"><p>This email is already registered</p></div>']);
        }
        $user = Users::find($id);
        $user->email = $email;
        $user->name = $name;
        $user->password = md5($password);
        $user->save();
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        session()->flush();
        return redirect('admin');
    }
}
