<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Http\Requests\newsRequest;
use Intervention\Image\ImageManagerStatic as Image;
class newsController extends Controller
{
    public function index()
    {
        $news = new News();
        return view('backend.news.news')->with('newss', $news->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(newsRequest $request)
    {
        $header = $request->input('header');
        $publishby = $request->input('publishby');
        $arabicheader = $request->input('arabicheader');
        $arabicpublishby = $request->input('arabicpublishby');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $category = $request->input('category');
        $arabiccategory = $request->input('arabiccategory');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'news', 'header')>=1){
            return redirect()->route('addnews',['validate'=>'<div class="alert alert-danger"><p>This news was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addnews',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $news = new News();
        $news->header = $header.'[EN:]'.$arabicheader;
        $news->categories = $category.'[EN:]'.$arabiccategory;
        $news->date = date("Y-m-d");
        $news->published_by = $publishby.'[EN:]'.$arabicpublishby;
        $news->description = $description.'[EN:]'.$arabicdescription;
        $news->image = url('/').'/uploads/'.$image;
        $news->save();
        return redirect('news');
        
    }
    public function upload_file(newsRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(772, 513);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);
       // echo json_encode($news);
        return view('backend.news.edit_news')->with('news', $news);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(newsRequest $request, $id)
    {
         $header = $request->input('header');
        $publishby = $request->input('publishby');
        $arabicheader = $request->input('arabicheader');
        $arabicpublishby = $request->input('arabicpublishby');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $category = $request->input('category');
        $arabiccategory = $request->input('arabiccategory');
        $image = $this->upload_file($request);
         
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'news', 'header')>=2){
            return redirect()->route('addnews',['validate'=>'<div class="alert alert-danger"><p>This news was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $news =  News::find($id);
        $news->header = $header.'[EN:]'.$arabicheader;
        $news->categories = $category.'[EN:]'.$arabiccategory;
        $news->date = date("Y-m-d");
        $news->published_by = $publishby.'[EN:]'.$arabicpublishby;
        $news->description = $description.'[EN:]'.$arabicdescription;
        
        if($image != "-text.png"){
        $news->image = url('/').'/uploads/'.$image;
        }
        $news->save();
        return redirect('news');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
