<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use App\Gallery_category;
use App\Http\Requests\galleryRequest;
use App\Http\Controllers\Redirect;
use Intervention\Image\ImageManagerStatic as Image;

class galleryController extends Controller
{
 public function index()
    {
        $gallery = new Gallery();
        $categories = new Gallery_category(); 
        return view('backend.gallery.gallery')->with('galleries', $gallery->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(galleryRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $category_id = $request->input('category');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'gallery', 'header')>=1){
            return redirect()->route('addgallery',['validate'=>'<div class="alert alert-danger"><p>This gallery was added before</p></div>']);
        }
        if($category_id == 0){
            return redirect()->route('addgallery',['validate'=>'<div class="alert alert-danger"><p>you must select a category</p></div>']);
        }
        if($image == '-text.png'){
            return redirect()->route('addgallery',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $gallery = new Gallery();
        $gallery->header = $header.'[EN:]'.$arabicheader;
        $gallery->description = $description.'[EN:]'.$arabicdescription;
        $gallery->category_id = $category_id;
        $gallery->image = 'uploads/'.$image;
        $gallery->save();
        return redirect('gallery');
        
    }
        public   function upload_file(galleryRequest $request){
       $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(270, 270);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gallery = Gallery::find($id);
       // echo json_encode($gallery);
        return view('backend.gallery.edit_gallery')->with('gallery', $gallery);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(galleryRequest $request, $id)
    {
         $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $category_id = $request->input('category');
        $image = $this->upload_file($request);
         
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'gallery', 'header')>=2){
            return redirect()->route('addgallery',['validate'=>'<div class="alert alert-danger"><p>This gallery was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $gallery = Gallery::find($id);
        $gallery->header = $header.'[EN:]'.$arabicheader;
        $gallery->description = $description.'[EN:]'.$arabicdescription;
        $gallery->category_id = $category_id;
        if($image != "-text.png"){
        $gallery->image = 'uploads/'.$image;
        }
        $gallery->save();
        return redirect('gallery');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
