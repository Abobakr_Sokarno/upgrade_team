<?php

namespace App\Http\Controllers;

use App\Software_house;
use App\Http\Requests\softwarehouseRequest;
use App\Http\Controllers\Redirect;
use Intervention\Image\ImageManagerStatic as Image;
class softwarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $softwarehouse = new Software_house();
        return view('backend.softwarehouse.softwarehouse')->with('softwarehouses', $softwarehouse->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(softwarehouseRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
//        $facbebooklink = $request->input('facebooklink');
//        $twitterlink= $request->input('twitterlink');
//        $linkedinlink = $request->input('linkedinlink');
        
        //$category_id = $request->input('category');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'software_house', 'header')>=1){
            return redirect()->route('addsoftwarehouse',['validate'=>'<div class="alert alert-danger"><p>This softwarehouse was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addsoftwarehouse',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $softwarehouse = new Software_house();
        $softwarehouse->header = $header.'[EN:]'.$arabicheader;
        $softwarehouse->description = $description.'[EN:]'.$arabicdescription;
//        $team->facebook_link = $facbebooklink;
//        $team->twitter_link = $twitterlink;
//        $team->linkedin_link = $linkedinlink;
       // $softwarehouse->category_id = $category_id;
        $softwarehouse->image = 'uploads/'.$image;
        $softwarehouse->save();
        return redirect('softwarehouse');
        
    }
        public   function upload_file(softwarehouseRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(870, 359);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $softwarehouse = Software_house::find($id);
       // echo json_encode($softwarehouse);
        return view('backend.softwarehouse.edit_softwarehouse')->with('softwarehouse', $softwarehouse);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(softwarehouseRequest $request, $id)
    {
         $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
//        $facbebooklink = $request->input('facebooklink');
//        $twitterlink= $request->input('twitterlink');
//        $linkedinlink = $request->input('linkedinlink');
        //$category_id = $request->input('category');
        $image = $this->upload_file($request);
         
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'software_house', 'header')>=2){
            return redirect()->route('addsoftwarehouse',['validate'=>'<div class="alert alert-danger"><p>This softwarehouse was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $softwarehouse = Software_house::find($id);
        $softwarehouse->header = $header.'[EN:]'.$arabicheader;
        $softwarehouse->description = $description.'[EN:]'.$arabicdescription;
//        $team->facebook_link = $facbebooklink;
//        $team->twitter_link = $twitterlink;
//        $team->linkedin_link = $linkedinlink;
        //$softwarehouse->category_id = $category_id;
        if($image != "-text.png"){
        $softwarehouse->image = 'uploads/'.$image;
        }
        $softwarehouse->save();
        return redirect('softwarehouse');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
