<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Our_Clients;
use App\Http\Requests\clientsRequest;
use App\Http\Controllers\Redirect;
use Intervention\Image\ImageManagerStatic as Image;
class clientsController extends Controller
{
      public function index()
    {
        $clients = new Our_Clients();
        return view('backend.clients.clients')->with('clients', $clients->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(clientsRequest $request)
    {
        $link = $request->input('link');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($link, 'our_clients', 'link')>=1){
            return redirect()->route('addclients',['validate'=>'<div class="alert alert-danger"><p>This clients was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addclients',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $clients = new Our_Clients();
        $clients->link = $link;
        $clients->image = 'uploads/'.$image;
        $clients->save();
        return redirect('clients');
        
    }
        public   function upload_file(clientsRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(175, 37);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clients = Our_Clients::find($id);
       // echo json_encode($clients);
        return view('backend.clients.edit_clients')->with('clients', $clients);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(clientsRequest $request, $id)
    {
         $link = $request->input('link');
       
        $image = $this->upload_file($request);
         
        if(commonController::validateTable($link, 'our_clients', 'link')>=2){
            return redirect()->route('addclients',['validate'=>'<div class="alert alert-danger"><p>This clients was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $clients = Our_Clients::find($id);
        $clients->link = $link;
        if($image != "-text.png"){
        $clients->image = 'uploads/'.$image;
        }
        $clients->save();
        return redirect('clients');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
