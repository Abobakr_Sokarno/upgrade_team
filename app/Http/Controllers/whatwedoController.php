<?php

namespace App\Http\Controllers;

use App\What_we_do;
use App\Http\Requests\whatwedoRequest;
use Illuminate\Http\Request;

use Intervention\Image\ImageManagerStatic as Image;
class whatwedoController extends Controller
{
    
        public function index()
    {
        $whatwedo = new What_we_do();
        return view('backend.whatwedo.whatwedo')->with('whatwedo', $whatwedo->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(whatwedoRequest $request)
    {
        $text = $request->input('text');
        $arabictext = $request->input('arabictext');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($text.'[EN:]'.$arabictext, 'what_we_do', 'text')>=1){
            return redirect()->route('addwhatwedo',['validate'=>'<div class="alert alert-danger"><p>This text was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addwhatwedo',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $whatwedo = new What_we_do();
        $whatwedo->text = $text.'[EN:]'.$arabictext;
        $whatwedo->image = 'uploads/'.$image;
        $whatwedo->save();
        return redirect('whatwedo');
        
    }
        public   function upload_file(whatwedoRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(48, 48);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $whatwedo = What_we_do::find($id);
       // echo json_encode($whatwedo);
        return view('backend.whatwedo.edit_whatwedo')->with('whatwedo', $whatwedo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(whatwedoRequest $request, $id)
    {
         $text = $request->input('text');
         $arabictext = $request->input('arabictext');
       
       
        $image = $this->upload_file($request);
       
        $whatwedo = What_we_do::find($id);
         $whatwedo->text = $text.'[EN:]'.$arabictext;
        if($image != "-text.png"){
        $whatwedo->image = "http://$_SERVER[HTTP_HOST]".'/uploads/'.$image;
        }
        $whatwedo->save();
        return redirect('whatwedo');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
