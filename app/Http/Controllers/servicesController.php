<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use App\Service_category;
use App\Http\Requests\servicesRequest;
use App\Http\Controllers\Redirect;
use Intervention\Image\ImageManagerStatic as Image;
class servicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = new Services();
        $categories = new Service_category(); 
        return view('backend/services/services')->with('services', $services->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(servicesRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $category_id = $request->input('category');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'services', 'header')>=1){
            return redirect()->route('addservice',['validate'=>'<div class="alert alert-danger"><p>This service was added before</p></div>']);
        }
        if($category_id == 0){
            return redirect()->route('addservice',['validate'=>'<div class="alert alert-danger"><p>you must select a category</p></div>']);
        }
        if($image == '-text.png'){
            return redirect()->route('addservice',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $services = new Services();
        $services->header = $header.'[EN:]'.$arabicheader;
        $services->description = $description.'[EN:]'.$arabicdescription;
        $services->category_id = $category_id;
        $services->image = url('/').'/uploads/'.$image;
        $services->save();
        return redirect('services');
        
    }
        public   function upload_file(servicesRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(870, 359);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $services = Services::find($id);
       // echo json_encode($services);
        return view('backend/services/edit_service')->with('service', $services);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(servicesRequest $request, $id)
    {
         $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $category_id = $request->input('category');
        $image = $this->upload_file($request);
         
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'services', 'header')>=2){
            return redirect()->route('addservice',['validate'=>'<div class="alert alert-danger"><p>This service was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $services =  Services::find($id);
        $services->header = $header.'[EN:]'.$arabicheader;
        $services->description = $description.'[EN:]'.$arabicdescription;
        $services->category_id = $category_id;
        if($image != "-text.png"){
        $services->image = "http://$_SERVER[HTTP_HOST]".'/upgrade_team/uploads/'.$image;
        }
        $services->save();
        return redirect('services');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
