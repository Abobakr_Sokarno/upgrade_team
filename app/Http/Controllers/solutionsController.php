<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solutions;
use App\Solutions_category;
use App\Http\Requests\solutionsRequest;
use Intervention\Image\ImageManagerStatic as Image;
class solutionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $solutions = new Solutions();
        $categories = new Solutions_category(); 
        return view('backend.solutions.solutions')->with('solutions', $solutions->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public   function upload_file(solutionsRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(870, 359);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    public function create(solutionsRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $category_id = $request->input('category');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'solutions', 'header') >= 1){
            return redirect()->route('addsolutions',['validate'=>'<div class="alert alert-danger"><p>This solutions was added before</p></div>']);
        }
        if($category_id == 0){
            return redirect()->route('addsolution',['validate'=>'<div class="alert alert-danger"><p>you must select a category</p></div>']);
        }
        if($image == '-text.png'){
            return redirect()->route('addsolution',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $solution = new Solutions();
        $solution->header = $header.'[EN:]'.$arabicheader;
        $solution->description = $description.'[EN:]'.$arabicdescription;
        $solution->category_id = $category_id;
        $solution->image = 'uploads/'.$image;
        $solution->save();
        return redirect('solutions');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $solution = Solutions::find($id);
       // echo json_encode($services);
        return view('backend.solutions.edit_solution')->with('solution', $solution);
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(solutionsRequest $request, $id)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $category_id = $request->input('category');
        $image = $this->upload_file($request);
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'solutions', 'header') >= 2){
            return redirect()->route('addsolutions',['validate'=>'<div class="alert alert-danger"><p>This solutions was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $solution =  Solutions::find($id);
        $solution->header = $header.'[EN:]'.$arabicheader;
        $solution->description = $description.'[EN:]'.$arabicdescription;
        $solution->category_id = $category_id;
        if($image != "-text.png"){
        $solution->image = 'uploads/'.$image;
        }
        $solution->save();
        return redirect('solutions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
