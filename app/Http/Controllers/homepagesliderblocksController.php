<?php

namespace App\Http\Controllers;
use App\Home_page_slider_block;
use App\Http\Requests\homepagesliderblocksRequest;
use Illuminate\Http\Request;

use Intervention\Image\ImageManagerStatic as Image;
class homepagesliderblocksController extends Controller
{
  public function index()
    {
        $homepagesliderblocks = new Home_page_slider_block();
        return view('backend.homepagesliderblocks.homepagesliderblocks')->with('homepagesliderblockss', $homepagesliderblocks->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(homepagesliderblocksRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'home_page_slider_blocks', 'header')>=1){
            return redirect()->route('addhomepagesliderblocks',['validate'=>'<div class="alert alert-danger"><p>This homepagesliderblocks was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addhomepagesliderblocks',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $homepagesliderblocks = new Home_page_slider_block();
        $homepagesliderblocks->header = $header.'[EN:]'.$arabicheader;
        $homepagesliderblocks->description = $description.'[EN:]'.$arabicdescription;
        $homepagesliderblocks->image = 'uploads/'.$image;
        $homepagesliderblocks->save();
        return redirect('homepageslidrblocks');
        
    }
        public   function upload_file(homepagesliderblocksRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(32, 33);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $homepagesliderblocks = Home_page_slider_block::find($id);
       // echo json_encode($homepagesliderblocks);
        return view('backend.homepagesliderblocks.edit_homepagesliderblocks')->with('homepagesliderblocks', $homepagesliderblocks);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(homepagesliderblocksRequest $request, $id)
    {
         $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
         
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'home_page_slider_blocks', 'header')>=2){
            return redirect()->route('addhomepagesliderblocks',['validate'=>'<div class="alert alert-danger"><p>This homepagesliderblocks was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $homepagesliderblocks =  Home_page_slider_block::find($id);
        $homepagesliderblocks->header = $header.'[EN:]'.$arabicheader;
        $homepagesliderblocks->description = $description.'[EN:]'.$arabicdescription;

        if($image != "-text.png"){
        $homepagesliderblocks->image = 'uploads/'.$image;
        }
        $homepagesliderblocks->save();
        return redirect('homepageslidrblocks');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
