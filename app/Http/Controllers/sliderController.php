<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\slider_category;
use App\Http\Requests\sliderRequest;
use App\Http\Controllers\Redirect;
use Intervention\Image\ImageManagerStatic as Image;

class sliderController extends Controller
{
    public function index()
    {
        $slider = new Slider();
        return view('backend.slider.slider')->with('sliders', $slider->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(sliderRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'slider', 'header')>=1){
            return redirect()->route('addslider',['validate'=>'<div class="alert alert-danger"><p>This slider was added before</p></div>']);
        }
        
        if($image == '-text.png'){
            return redirect()->route('addslider',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $slider = new Slider();
        $slider->header = $header.'[EN:]'.$arabicheader;
        $slider->description = $description.'[EN:]'.$arabicdescription;
        $slider->image = 'uploads/'.$image;
        $slider->save();
        return redirect('slider');
        
    }
        public   function upload_file(sliderRequest $request){
       $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(1400, 730);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slider = Slider::find($id);
       // echo json_encode($slider);
        return view('backend.slider.edit_slider')->with('slider', $slider);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(sliderRequest $request, $id)
    {
         $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
         
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'slider', 'header')>=2){
            return redirect()->route('addslider',['validate'=>'<div class="alert alert-danger"><p>This slider was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $slider =  Slider::find($id);
        $slider->header = $header.'[EN:]'.$arabicheader;
        $slider->description = $description.'[EN:]'.$arabicdescription;
        if($image != "-text.png"){
        $slider->image = 'uploads/'.$image;
        }
        $slider->save();
        return redirect('slider');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
