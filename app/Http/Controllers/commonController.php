<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Contact_messages;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
class commonController extends Controller {

     public function  delete_all_messages(){
     DB::table('contact_messages')->delete();
     return redirect('contactmessages');
 }
    public function delete(Request $request) {
        $table = $request->input('table');
        $id = $request->input('id');
        $res['table'] = $table;
        $res['id'] = $id;
        echo json_encode($res);
        DB::table($table)->where('id', $id)->delete();
    }

    public static function count($table) {
        return DB::table($table)->count();
    }

    public static function ArabicTranslate($content) {
        $key = '[EN:]';
        $arabic = explode($key, $content)[1];
        return $arabic;
    }

    public static function EnglishTranslate($content) {
        $key = '[EN:]';
        $english = explode($key, $content)[0];
        return $english;
    }

    public static function upload_file(Request $request) {
        $photoName = "none";
        if ($request->hasFile('image')) {
            $photoName = rand(10, 100000) . time() . '.' . $request->image->getClientOriginalExtension();
            $request->ifile->move('uploads', $photoName);
        }
        return $photoName;
    }

    public static function validateTable($content, $table, $column) {
        echo $result = DB::table($table)->where($column, $content)->count();
    }

    public static function category_name($id, $table) {
        $result = DB::table($table)->where('id', $id)->get();
        foreach ($result as $res)
            ;
        return $res->category_name;
    }

    public static function get_category($table) {
        $result = DB::table($table)->get();
        return $result;
    }

    public function sort(Request $request) {
        $table = $request->input('table');
        $menu = $request->input('menu');

        for ($i = 0; $i < count($menu); $i++) {
            DB::table($table)->where('id', $menu[$i])->update(['sort' => $i]);
        }
    }

    public function featured(Request $request) {
        $table = $request->input('table');
        $id = $request->input('id');
        $i = $request->input('f');
        DB::table($table)->where('id', $id)->update(['featured' => $i]);
        echo json_encode($table);
    }

    public static function get_content($table) {
        return DB::table($table)->get();
    }

    public static function get_content_sortted($table) {
        return DB::table($table)->where('featured', '0')->orderBy('sort', 'ASC')->get();
    }
    public static function get_content_sortted_by_category($table , $id){
        return DB::table($table)->where('featured', '0')->where('category_id',$id)->orderBy('sort', 'ASC')->get();
    }
    
    public static function get_content_sortted_by_title($table , $title,$column){
        return DB::table($table)->where('featured', '0')->where($column,$title)->orderBy('sort', 'ASC')->get()->first();
       
         
        
    }
    public static function get_content_sortted_by_categoryName($category_table,$table , $title){
        $cat_title = DB::table($category_table)->where('featured', '0')->where('category_name',$title)->orderBy('sort', 'ASC')->get()->first();
        return DB::table($table)->where('featured', '0')->where('category_id',$cat_title->id)->orderBy('sort', 'ASC')->get();
    }
    public static function get_content_by_id($table, $id) {
        return DB::table($table)->where('id', $id)->get();
    }

    public function send(Request $request) {
        $email = $this->make_safe($request->input('email'));
        $name  =  $this->make_safe($request->input('name'));
        $message  =  $this->make_safe($request->input('message'));
       $res = array();
        if(!$email || !$message || !$name){
        $res['res'] = 1;
        }else if(!filter_var($email, FILTER_VALIDATE_EMAIL) ){
            $res['res']=2;
        }
       else {
           $contact_message =  Contact_messages::where('sender_message',$message)->get();
           if($contact_message->count()){
               $res['res']=3;
           }else {
           $contact_message = new Contact_messages();
           $contact_message->sender_name = $name;
           $contact_message->sender_email = $email;
           $contact_message->sender_message = $message;
           $contact_message->save();
           $res['res']= 0;
           }
           
       }
        echo json_encode($res);
          
    }

    public static function make_safe($variable) {
        $variable = strip_tags($variable);
        $variable = stripslashes($variable);
        $variable = trim($variable, "'");
        $variable = str_replace("'", "", $variable);
        $variable = str_replace('"', "", $variable);
        $variable = str_replace('/', "", $variable);
        $variable = str_replace('/\/', "", $variable);
        return $variable;
    }
    public  function search(Request $request){
        $phrase = strtolower($this->make_safe($request->input('phrase')));
        $solutions     =DB::table('solutions')->get();
        $services      =DB::table('services')->get();
        $softwarehouse =DB::table('software_house')->get();
        $res = array(); $i=0;
        foreach ($solutions as $solution){
            if(strpos( strtolower($solution->header),$phrase)||strpos(strtolower($solution->description), $phrase)){
                $solution->link = url('/').'/solution?title='.$solution->header;
                $res[$i++]=$solution; //url('/').'/solution?id='.$solution->id;
            }
        }$i=0;
        foreach ($services as $solution){
            if(strpos(strtolower($solution->header), $phrase)||strpos(strtolower($solution->description),$phrase)){
                $solution->link = url('/').'/service?title='.$solution->header;
                $res[$i++]=$solution;//url('/').'/service?id='.$solution->id;
            }
        }$i=0;
        foreach ($softwarehouse as $solution){
            if(strpos(strtolower($solution->header), $phrase)||strpos(strtolower($solution->description), $phrase)){
                $solution->link= url('/').'/softwarehouse?title='.$solution->header;
                $res[$i++]=$solution;//url('/').'/softwarehouse?id='.$solution->id;
            }
        }
        return view('searchresult')->with('res',$res);
    }
   public static function maxID($table){
        return DB::table($table)->max('id');
    }
   

}
