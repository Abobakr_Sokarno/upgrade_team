<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Contact_us;
use App\Http\Requests\contactusRequest;

class contactusController extends Controller
{
  public function index()
    {
        $contactus = new Contact_us();
        return view('backend.contactus.contactus')->with('contactus', $contactus->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(contactusRequest $request)
    {
        $address = $request->input('address');
        $arabicaddress = $request->input('arabicaddress');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $mapaddress = $request->input('mapaddress');
        $arabicmapaddress = $request->input('arabicmapaddress');
        $twitteremail = $request->input('twitter');
        $emailedinemail = $request->input('emailedin');
        $facebookemail = $request->input('facebook');
       
        $contactus = new Contact_us();
        $contactus->description = $description.'[EN:]'.$arabicdescription;
        $contactus->address = $address.'[EN:]'.$arabicaddress;
        $contactus->map_address = $mapaddress.'[EN:]'.$arabicmapaddress;
        $contactus->facebook_email = $facebookemail;
        $contactus->twitter_email = $twitteremail;
        $contactus->emailedin_email = $emailedinemail;
        $contactus->save();
        return redirect('contactus');
        
    }
        public   function upload_file(contactusRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(175, 37);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contactus = Contact_us::find($id);
       // echo json_encode($contactus);
        return view('backend.contactus.edit_contactus')->with('contactus', $contactus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(contactusRequest $request, $id)
    {
     
        $address = $request->input('address');
        $arabicaddress = $request->input('arabicaddress');
        
        $email = $request->input('email');
        $phone = $request->input('phone');
        
        $contactus = Contact_us::find($id);
      
        $contactus->address = $address.'[EN:]'.$arabicaddress;
        $contactus->email = $email;
        $contactus->phone = $phone;
        
        $contactus->save();
       
        return redirect('contactus');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
