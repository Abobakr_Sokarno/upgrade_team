<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\commonController;
use App\Solutions_category;
use App\Http\Requests\solutioncategoryRequest;
use Intervention\Image\ImageManagerStatic as Image;
class solutioncategoryController extends Controller
{
 public function index()
    {
        $categories = new Solutions_category(); 
        return view('backend.solution_category.solution_categories')->with('categories', $categories->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(solutioncategoryRequest $request)
    {
        $name = $request->input('name'); 
        $arabicname = $request->input('arabicname');
        if(commonController::validateTable($name.'[EN:]'.$arabicname, 'service_category', 'category_name')>=1){
            return redirect()->route('addservicecategory',['validate'=>'<div class="alert alert-danger"><p>This category was added before</p></div>']);
        }
        $category = new Solutions_category();
        $category ->category_name = $name . '[EN:]' . $arabicname;
        $category->save();
        return redirect('solutioncategory');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Solutions_category::find($id);
        return view('backend.solution_category.edit_solution_category')->with('category',$category);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(solutioncategoryRequest $request, $id)
    {
         $name = $request->input('name'); 
         $arabicname = $request->input('arabicname');
         
        if(commonController::validateTable($name.'[EN:]'.$arabicname, 'service_category', 'category_name')>=2){
            return redirect()->route('addservicecategory',['validate'=>'<div class="alert alert-danger"><p>This category was added before</p></div>']);
        }
         $category = Solutions_category::find($id);
         $category ->category_name = $name . '[EN:]' . $arabicname;
         $category->save();
        return redirect('solutioncategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
