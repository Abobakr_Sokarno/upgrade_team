<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\commonController;
use App\Gallery_category;
use App\Http\Requests\gallerycategoryRequest;
use Intervention\Image\ImageManagerStatic as Image;
class gallerycategoryController extends Controller
{
  public function index()
    {
        $categories = new Gallery_category(); 
        return view('backend.gallery_category.gallery_categories')->with('categories', $categories->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(gallerycategoryRequest $request)
    {
        $name = $request->input('name'); 
        $arabicname = $request->input('arabicname');
        if(commonController::validateTable($name.'[EN:]'.$arabicname, 'gallery_category', 'category_name')>=1){
            return redirect()->route('addgallerycategory',['validate'=>'<div class="alert alert-danger"><p>This category was added before</p></div>']);
        }
        $category = new Gallery_category();
        $category ->category_name = $name . '[EN:]' . $arabicname;
        $category->save();
        return redirect('gallerycategory');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Gallery_category::find($id);
        return view('backend.gallery_category.edit_gallery_category')->with('category',$category);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(gallerycategoryRequest $request, $id)
    {
         $name = $request->input('name'); 
         $arabicname = $request->input('arabicname');
         $result =  commonController::validateTable($name.'[EN:]'.$arabicname, 'gallery_category', 'category_name');
         if($result>=2){
            return redirect()->route('addgallerycategory',['validate'=>'<div class="alert alert-danger"><p>This category was added before</p></div>']);
        }
         $category = Gallery_category::find($id);
         $category ->category_name = $name . '[EN:]' . $arabicname;
         $category->save();
        return redirect('gallerycategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
