<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Our_skills;
use App\Http\Requests\ourskillsRequest;
use App\Http\Controllers\Redirect;
use Intervention\Image\ImageManagerStatic as Image;
class ourskillsController extends Controller
{
  
    public function index()
    {
        $ourskills = new Our_skills();
        return view('backend.ourskills.ourskills')->with('ourskillss', $ourskills->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ourskillsRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $name = $request->input('name');
        $arabicname = $request->input('arabicname');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $quality = $request->input('quality');
        $image = $this->upload_file($request);
        $experince = $request->input('experince');
        
        if(commonController::validateTable($name.'[EN:]'.$arabicname, 'our_skills', 'name')>=1){
            return redirect()->route('addourskills',['validate'=>'<div class="alert alert-danger"><p>This ourskills was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addourskills',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $ourskills = new Our_skills();
        $ourskills->name = $name.'[EN:]'.$arabicname;
        $ourskills->header = $header.'[EN:]'.$arabicheader;
        $ourskills->description = $description.'[EN:]'.$arabicdescription; 
        $ourskills->quality = $quality;
        $ourskills->experince = $experince;
        $ourskills->image = 'uploads/'.$image;
   
        $ourskills->save();
        return redirect('ourskills');
        
    }
        public   function upload_file(ourskillsRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(571, 376);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ourskills = Our_skills::find($id);
       // echo json_encode($ourskills);
        return view('backend.ourskills.edit_ourskills')->with('ourskills', $ourskills);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ourskillsRequest $request, $id)
    {
         $name = $request->input('name');
        $arabicname = $request->input('arabicname');
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $quality = $request->input('quality');
        $experince = $request->input('experince');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($name.'[EN:]'.$arabicname, 'our_skills', 'name')>=2){
            return redirect()->route('addourskills',['validate'=>'<div class="alert alert-danger"><p>This ourskills was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $ourskills =  Our_skills::find($id);
        $ourskills->name = $name.'[EN:]'.$arabicname;
        $ourskills->header = $header.'[EN:]'.$arabicheader;
        $ourskills->description = $description.'[EN:]'.$arabicdescription;
        $ourskills->quality = $quality;
        $ourskills->experince = $experince;
       if($image != "-text.png"){
        $ourskills->image = 'uploads/'.$image;
        }
        
        $ourskills->save();
        return redirect('ourskills');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
