<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testmonial;
use App\testmonial_category;
use App\Http\Requests\testmonialRequest;
use App\Http\Controllers\Redirect;

use Intervention\Image\ImageManagerStatic as Image;
class testmonialController extends Controller
{
    public function index()
    {
        $testmonial = new Testmonial();
        return view('backend.testmonial.testmonial')->with('testmonials', $testmonial->orderBy('sort', 'ASC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(testmonialRequest $request)
    {
        $name = $request->input('name');
        $companyname = $request->input('companyname');
        $arabicname = $request->input('arabicname');
        $arabiccompanyname = $request->input('arabiccompanyname');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($name.'[EN:]'.$arabicname, 'testmonial', 'name')>=1){
            return redirect()->route('addtestmonial',['validate'=>'<div class="alert alert-danger"><p>This testmonial was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addtestmonial',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $testmonial = new Testmonial();
        $testmonial->name = $name.'[EN:]'.$arabicname;
        $testmonial->company_name = $companyname.'[EN:]'.$arabiccompanyname;
        $testmonial->description = $description.'[EN:]'.$arabicdescription;
        $testmonial->image = 'uploads/'.$image;
        $testmonial->save();
        return redirect('testmonial');
        
    }
        public   function upload_file(testmonialRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
        $image = $request->file('image');
        $image_resize = Image::make($image->getRealPath()); 
        $image_resize->resize(71, 71);
        $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
        $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $testmonial = Testmonial::find($id);
       // echo json_encode($testmonial);
        return view('backend.testmonial.edit_testmonial')->with('testmonial', $testmonial);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(testmonialRequest $request, $id)
    {
         $name = $request->input('name');
         $companyname = $request->input('companyname');
        $arabicname = $request->input('arabicname');
         $arabiccompanyname = $request->input('arabiccompanyname');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
         
        if(commonController::validateTable($name.'[EN:]'.$arabicname, 'testmonial', 'name')>=2){
            return redirect()->route('addtestmonial',['validate'=>'<div class="alert alert-danger"><p>This testmonial was added before</p></div>']);
        }
        //echo $image;
       // return ;
        $testmonial =  Testmonial::find($id);
        $testmonial->name = $name.'[EN:]'.$arabicname;
        $testmonial->company_name = $companyname.'[EN:]'.$arabiccompanyname;
        $testmonial->description = $description.'[EN:]'.$arabicdescription;
   
        if($image != "-text.png"){
        $testmonial->image = 'uploads/'.$image;
        }
        $testmonial->save();
        return redirect('testmonial');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
