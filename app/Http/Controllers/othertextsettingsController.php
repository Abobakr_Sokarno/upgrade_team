<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Other_texts_settings;
use App\Http\Requests\othertextsettingsRequest;
use App\Http\Controllers\Redirect;
use Intervention\Image\ImageManagerStatic as Image;

class othertextsettingsController extends Controller
{
 public function index()
    {
        $othertextsettings = new Other_texts_settings();
        return view('backend.othertextsettings.othertextsettings')->with('othertextsettings', $othertextsettings->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(othertextsettingsRequest $request)
    {
        $header = $request->input('header');
        $arabicheader = $request->input('arabicheader');
        $description = $request->input('description');
        $arabicdescription = $request->input('arabicdescription');
        $image = $this->upload_file($request);
        
        if(commonController::validateTable($header.'[EN:]'.$arabicheader, 'othertextsettings', 'header')>=1){
            return redirect()->route('addothertextsettings',['validate'=>'<div class="alert alert-danger"><p>This othertextsettings was added before</p></div>']);
        }
       
        if($image == '-text.png'){
            return redirect()->route('addothertextsettings',['validate'=>'<div class="alert alert-danger"><p>you must select an image</p></div>']);
        }
        $othertextsettings = new Other_texts_settings();
        $othertextsettings->header = $header.'[EN:]'.$arabicheader;
        $othertextsettings->description = $description.'[EN:]'.$arabicdescription;
        $othertextsettings->image = 'uploads/'.$image;
        $othertextsettings->save();
        return redirect('othertextsettings');
        
    }
    public function upload_file(othertextsettingsRequest $request){
        $photoName = "-text.png";
        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_resize = Image::make($image->getRealPath()); 
            $image_resize->resize(175, 37);
            $photoName = rand(10, 100000).time().'.'.$request->image->getClientOriginalExtension();
            $image_resize->save('uploads/'.$photoName);
        }
        return $photoName;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $othertextsettings = Other_texts_settings::find($id);
       // echo json_encode($othertextsettings);
        return view('backend.othertextsettings.edit_othertextsettings')->with('othertextsettings', $othertextsettings);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(othertextsettingsRequest $request, $id)
    {
         $value = $request->input('value');
         $arabicvalue = $request->input('arabicvalue');
         
        $othertextsettings =  Other_texts_settings::find($id);
        $othertextsettings->text_value = $value.'[EN:]'.$arabicvalue;
        
        $othertextsettings->save();
        return redirect('othertextsettings');
        
        
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
