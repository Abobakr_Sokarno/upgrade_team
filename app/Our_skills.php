<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Our_skills extends Model
{
    protected $table = 'our_skills';
	public $timestamps = false;
}
