<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home_page_slider_block extends Model
{
    protected $table = 'home_page_slider_blocks';
	public $timestamps = false;
}
