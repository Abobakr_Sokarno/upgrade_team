<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service_category extends Model
{
    protected $table = 'service_category';
	public $timestamps = false;
}
