<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact_messages extends Model
{
    protected $table = 'contact_messages';
	public $timestamps = true;
}
