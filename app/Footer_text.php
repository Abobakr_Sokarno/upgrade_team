<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footer_text extends Model
{
    protected $table = 'footer_text';
	public $timestamps = false;
}
