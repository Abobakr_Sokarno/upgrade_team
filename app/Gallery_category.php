<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery_category extends Model
{
    protected $table = 'gallery_category';
	public $timestamps = false;
}
