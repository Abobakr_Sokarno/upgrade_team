<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Other_texts_settings extends Model
{
    protected $table = 'other_text_settings';
	public $timestamps = false;
}
