<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class What_we_do extends Model
{
    protected $table = 'what_we_do';
	public $timestamps = false;
}
