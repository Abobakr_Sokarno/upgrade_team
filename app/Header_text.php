<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Header_text extends Model
{
    protected $table = 'header_text';
	public $timestamps = false;
}
