<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solutions_category extends Model
{
    protected $table = 'solution_category';
	public $timestamps = false;
}
