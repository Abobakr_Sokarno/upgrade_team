<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Software_house extends Model
{
    protected $table = 'software_house';
	public $timestamps = false;
}
