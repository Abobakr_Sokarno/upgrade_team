<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Our_Clients extends Model
{
    protected $table = 'our_clients';
	public $timestamps = false;
}
