<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Branches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('branches' , function(Blueprint $table){
            $table->increments('id');
            $table->longText('map_address');
            $table->longText('street');
            $table->longText('city');
            $table->longText('twitter_link')->nulable();
            $table->longText('linkedin_link')->nulable();
            $table->longText('facebook_link')->nulable();
            $table->integer('sort')->default(0);
            $table->integer('featured')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
