<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Team extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team' , function(Blueprint $table){
            $table->increments('id');
            $table->longText('header');
            $table->longText('description');
            $table->longText('position');
            $table->longText('image');
            $table->longText('facebook_link')->nullable();
            $table->longText('linkedin_link')->nullable();
            $table->longText('twitter_link')->nullable();
            $table->integer('sort')->default(0);
            $table->integer('featured')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
