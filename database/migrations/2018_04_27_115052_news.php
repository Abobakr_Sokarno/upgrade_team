<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class News extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('news' , function(Blueprint $table){
            $table->increments('id');
            $table->longText('header');
            $table->longText('description');
            $table->longText('image');
            $table->longText('published_by');
            $table->longText('date');
            $table->longText('categories');
            $table->integer('sort')->default(0);
            $table->integer('featured')->default(0);
            //$table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
