<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Features extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features' , function(Blueprint $table){
            $table->increments('id');
            $table->longText('header');
            $table->longText('description');
            $table->longText('image');
            $table->integer('sort')->default(0);
            $table->integer('featured')->default(0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
