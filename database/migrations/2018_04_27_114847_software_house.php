<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoftwareHouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('software_house' , function(Blueprint $table){
            $table->increments('id');
            $table->longText('header');
            $table->longText('description');
            $table->longText('image');
            $table->longText('facebook_link')->nullable();
            $table->longText('linkedin_link')->nullable();
            $table->longText('twitter_link')->nullable();
            $table->integer('sort')->default(0);
            $table->integer('featured')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
