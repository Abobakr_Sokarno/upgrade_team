<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OurSkills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('our_skills' , function(Blueprint $table){
            $table->increments('id');
            $table->longText('name');
            $table->longText('header');
            $table->longText('description');
            $table->longText('image');
            $table->integer('quality');
            $table->integer('experince');
            $table->integer('sort')->default(0);
            $table->integer('featured')->default(0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
