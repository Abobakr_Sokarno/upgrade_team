<?php

use Illuminate\Database\Seeder;

class OurClientsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('our_clients')->delete();
        
        \DB::table('our_clients')->insert(array (
            0 => 
            array (
                'id' => 1,
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/895701526053717.png',
                'link' => '#',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/761451526053733.png',
                'link' => '#',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/755141526053744.png',
                'link' => '#',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/346851526053764.png',
                'link' => '#',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}