<?php

use Illuminate\Database\Seeder;

class OurSkillsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('our_skills')->delete();
        
        \DB::table('our_skills')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'Services[EN:]Services',
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/549281526114126.jpg',
                'quality' => 50,
                'experince' => 100,
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'solutions[EN:]solutions',
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/645531526113316.jpg',
                'quality' => 75,
                'experince' => 80,
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}