<?php

use Illuminate\Database\Seeder;

class WhatWeDoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('what_we_do')->delete();
        
        \DB::table('what_we_do')->insert(array (
            0 => 
            array (
                'id' => 1,
                'text' => 'We working on software development in fields of Desktop Application[EN:]We working on software development in fields of Desktop Application',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/876031526043330.png',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'text' => 'Working on Low Current System installation[EN:]Working on Low Current System installation',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/962481526043351.png',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'text' => 'Power system solutions[EN:]Power system solutions',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/490461526043374.png',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}