<?php

use Illuminate\Database\Seeder;

class SolutionCategoryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('solution_category')->delete();
        
        \DB::table('solution_category')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_name' => 'low current solution[EN:]low current solution',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_name' => 'power solution[EN:]power solution',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_name' => 'Data Center solution[EN:]Data Center solution',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'category_name' => 'Security solution[EN:]Security solution',
                'sort' => 0,
                'featured' => 0,
            ),
            4 => 
            array (
                'id' => 5,
                'category_name' => 'cloud solution[EN:]cloud solution',
                'sort' => 0,
                'featured' => 0,
            ),
            5 => 
            array (
                'id' => 6,
                'category_name' => 'Network solution[EN:]Network solution',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}