<?php

use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('branches')->delete();
        
        \DB::table('branches')->insert(array (
            0 => 
            array (
                'id' => 1,
                'map_address' => 'abdelrhman[EN:]adblerhman',
                'street' => 'elgzar[EN:]elgzar',
                'city' => 'cairo[EN:]cairo',
                'twitter_link' => 'www.twitter.com',
                'linkedin_link' => 'www.linkedin.com',
                'facebook_link' => 'www.facebook.com',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'map_address' => 'medhat[EN:]medhat',
                'street' => 'elgzar[EN:]elgzar',
                'city' => 'cairo[EN:]cairo',
                'twitter_link' => 'www.twitter.com',
                'linkedin_link' => 'www.linkedin.com',
                'facebook_link' => 'www.facebook.com',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'map_address' => 'abdelrhman[EN:]adblerhman',
                'street' => 'elgzar[EN:]elgzar',
                'city' => 'cairo[EN:]cairo',
                'twitter_link' => 'www.twitter.com',
                'linkedin_link' => 'www.linkedin.com',
                'facebook_link' => 'www.facebook.com',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}