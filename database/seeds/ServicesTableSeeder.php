<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('services')->delete();
        
        \DB::table('services')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/67071526058317.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/384141526058423.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/446101526058435.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/158061526058445.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            4 => 
            array (
                'id' => 5,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/67071526058317.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            5 => 
            array (
                'id' => 6,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/793581526058459.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            6 => 
            array (
                'id' => 7,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/780061526058472.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            7 => 
            array (
                'id' => 8,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/421821526058485.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            8 => 
            array (
                'id' => 9,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/67071526058317.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            9 => 
            array (
                'id' => 10,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/945751526058501.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            10 => 
            array (
                'id' => 11,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/845021526058517.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            11 => 
            array (
                'id' => 12,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/67071526058317.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}