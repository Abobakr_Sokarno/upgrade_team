<?php

use Illuminate\Database\Seeder;

class GalleryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('gallery')->delete();
        
        \DB::table('gallery')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_id' => 1,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/51511525969517.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_id' => 2,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/564131525969672.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_id' => 2,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/302061525969691.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'category_id' => 1,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/138141525969706.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            4 => 
            array (
                'id' => 5,
                'category_id' => 1,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/331291525969747.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            5 => 
            array (
                'id' => 6,
                'category_id' => 1,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/265171525969780.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            6 => 
            array (
                'id' => 7,
                'category_id' => 3,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/31981525969800.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            7 => 
            array (
                'id' => 8,
                'category_id' => 3,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/51511525969517.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}