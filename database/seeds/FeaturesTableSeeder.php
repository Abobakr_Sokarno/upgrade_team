<?php

use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('features')->delete();
        
        \DB::table('features')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'network solutions[EN:]network solutions',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/7411525972372.png',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'header' => 'Business develop[EN:]Business develop',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/407481525972410.png',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'header' => 'People research[EN:]People research',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/931831525972438.png',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'header' => 'Marketing[EN:]Marketing',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/308541525972467.png',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}