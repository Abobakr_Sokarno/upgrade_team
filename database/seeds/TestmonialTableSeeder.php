<?php

use Illuminate\Database\Seeder;

class TestmonialTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('testmonial')->delete();
        
        \DB::table('testmonial')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'farouk el kadi[EN:]farouk el kadi',
                'company_name' => 'Elnile Company[EN:]Elnile Company',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/878081525968553.png',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'farouk el kadi[EN:]farouk el kadi',
                'company_name' => 'Elnile Company[EN:]Elnile Company',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/878081525968553.png',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'farouk el kadi[EN:]farouk el kadi',
                'company_name' => 'Elnile Company[EN:]Elnile Company',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/878081525968553.png',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}