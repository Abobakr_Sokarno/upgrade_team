<?php

use Illuminate\Database\Seeder;

class ContactUsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contact_us')->delete();
        
        \DB::table('contact_us')->insert(array (
            0 => 
            array (
                'id' => 1,
                'address' => '12 S Al Shater Alasher st, New Maadi Cairo Egypt.[EN:]12 S Al Shater Alasher st, New Maadi Cairo Egypt.',
                'email' => 'info@upgrade-team.com',
                'phone' => '002 270 41 228,002 010 260 704 90',
            ),
        ));
        
        
    }
}