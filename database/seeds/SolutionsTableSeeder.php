<?php

use Illuminate\Database\Seeder;

class SolutionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('solutions')->delete();
        
        \DB::table('solutions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/428971526048987.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/115951526049234.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/219671526049250.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'category_id' => 4,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/725661526049267.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            4 => 
            array (
                'id' => 5,
                'category_id' => 5,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/30591526049315.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            5 => 
            array (
                'id' => 6,
                'category_id' => 6,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/435351526049287.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            6 => 
            array (
                'id' => 9,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/428971526048987.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            7 => 
            array (
                'id' => 10,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/115951526049234.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            8 => 
            array (
                'id' => 11,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/219671526049250.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            9 => 
            array (
                'id' => 12,
                'category_id' => 4,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/725661526049267.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            10 => 
            array (
                'id' => 13,
                'category_id' => 5,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/30591526049315.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            11 => 
            array (
                'id' => 14,
                'category_id' => 6,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/435351526049287.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            12 => 
            array (
                'id' => 15,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/428971526048987.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            13 => 
            array (
                'id' => 16,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/115951526049234.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            14 => 
            array (
                'id' => 17,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/219671526049250.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            15 => 
            array (
                'id' => 18,
                'category_id' => 4,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/725661526049267.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            16 => 
            array (
                'id' => 19,
                'category_id' => 5,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/30591526049315.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            17 => 
            array (
                'id' => 20,
                'category_id' => 6,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/435351526049287.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            18 => 
            array (
                'id' => 21,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/428971526048987.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            19 => 
            array (
                'id' => 22,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/115951526049234.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            20 => 
            array (
                'id' => 23,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/219671526049250.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            21 => 
            array (
                'id' => 24,
                'category_id' => 4,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/725661526049267.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            22 => 
            array (
                'id' => 25,
                'category_id' => 5,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/30591526049315.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            23 => 
            array (
                'id' => 26,
                'category_id' => 6,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/435351526049287.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}