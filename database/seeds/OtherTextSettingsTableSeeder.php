<?php

use Illuminate\Database\Seeder;

class OtherTextSettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('other_text_settings')->delete();
        
        \DB::table('other_text_settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'text_name' => 'Testimonial Header',
                'text_value' => 'Testimonial[EN:]',
            ),
            1 => 
            array (
                'id' => 2,
                'text_name' => 'Testimonial small brief',
                'text_value' => 'What My Client\'s Say[EN:]',
            ),
            2 => 
            array (
                'id' => 3,
                'text_name' => 'Portfolio header',
                'text_value' => 'Portfolio [EN:]',
            ),
            3 => 
            array (
                'id' => 4,
                'text_name' => 'Portfolio breif',
                'text_value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry[EN:]',
            ),
            4 => 
            array (
                'id' => 5,
                'text_name' => 'Features header',
                'text_value' => 'Features [EN:]',
            ),
            5 => 
            array (
                'id' => 6,
                'text_name' => 'Features brief',
                'text_value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]',
            ),
            6 => 
            array (
                'id' => 7,
                'text_name' => 'Recent News header',
                'text_value' => 'Recent News[EN:]',
            ),
            7 => 
            array (
                'id' => 8,
                'text_name' => 'Recent News brief',
                'text_value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]',
            ),
            8 => 
            array (
                'id' => 9,
                'text_name' => 'Contact Us Header',
                'text_value' => 'Contact Us[EN:]',
            ),
            9 => 
            array (
                'id' => 10,
                'text_name' => 'contact us form header',
                'text_value' => 'Get in Touch[EN:]',
            ),
            10 => 
            array (
                'id' => 11,
                'text_name' => 'contact us form small breif',
                'text_value' => 'Feel free to contact with us[EN:]',
            ),
            11 => 
            array (
                'id' => 12,
                'text_name' => 'our_skills header',
                'text_value' => 'our_skills[EN:]',
            ),
            12 => 
            array (
                'id' => 13,
                'text_name' => 'our skills brief',
                'text_value' => 'Publishing packages and web page editors now use Lorem Ipsum as their default model text[EN:]Publishing packages and web page editors now use Lorem Ipsum as their default model text',
            ),
            13 => 
            array (
                'id' => 14,
                'text_name' => 'Our Team header',
                'text_value' => 'Our Team[EN:]',
            ),
            14 => 
            array (
                'id' => 15,
                'text_name' => 'Our Team small breif',
                'text_value' => 'Publishing packages and web page editors now use Lorem Ipsum as their default model text[EN:]',
            ),
            15 => 
            array (
                'id' => 16,
                'text_name' => 'Our Client header',
                'text_value' => 'Our Client[EN:]',
            ),
            16 => 
            array (
                'id' => 17,
                'text_name' => 'Our Client brief',
                'text_value' => 'Publishing packages and web page editors now use Lorem Ipsum as their default model text[EN:]Publishing packages and web page editors now use Lorem Ipsum as their default model text',
            ),
        ));
        
        
    }
}