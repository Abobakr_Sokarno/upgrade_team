<?php

use Illuminate\Database\Seeder;

class GalleryCategoryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('gallery_category')->delete();
        
        \DB::table('gallery_category')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_name' => 'Branding[EN:]Branding',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_name' => 'Network[EN:]Network',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_name' => 'electronic[EN:]electronic',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}