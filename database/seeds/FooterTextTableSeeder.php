<?php

use Illuminate\Database\Seeder;

class FooterTextTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('footer_text')->delete();
        
        \DB::table('footer_text')->insert(array (
            0 => 
            array (
                'id' => 1,
                'text_name' => 'facebook link',
                'text_value' => 'https://www.facebook.com',
            ),
            1 => 
            array (
                'id' => 2,
                'text_name' => 'twitter link',
                'text_value' => 'https://www.twitter.com',
            ),
            2 => 
            array (
                'id' => 3,
                'text_name' => 'linkedin link',
                'text_value' => 'https://www.linkedin.com',
            ),
            3 => 
            array (
                'id' => 4,
                'text_name' => 'tawoolink',
                'text_value' => 'https://www.tawoo.com',
            ),
            4 => 
            array (
                'id' => 5,
                'text_name' => 'address',
                'text_value' => '12 , Al shater Alasher st , New Maadi , cairo , egypt',
            ),
            5 => 
            array (
                'id' => 6,
                'text_name' => 'email',
                'text_value' => 'info@upgrade-team.com',
            ),
            6 => 
            array (
                'id' => 7,
                'text_name' => 'phone',
                'text_value' => '01026070490',
            ),
            7 => 
            array (
                'id' => 8,
                'text_name' => 'logo',
                'text_value' => 'http://localhost:8000/uploads/197661526033361.png',
            ),
            8 => 
            array (
                'id' => 9,
                'text_name' => 'arabic address',
                'text_value' => '12 , Al shater Alasher st , New Maadi , cairo , egypt',
            ),
            9 => 
            array (
                'id' => 10,
                'text_name' => 'arabic phone',
                'text_value' => '01026070490',
            ),
            10 => 
            array (
                'id' => 11,
                'text_name' => 'arabic logo',
                'text_value' => 'http://localhost:8000/uploads/197661526033361.png',
            ),
            11 => 
            array (
                'id' => 12,
                'text_name' => 'nav bar',
                'text_value' => 'Home[sep]Solutions[sep]Services[sep]Gallery[sep]Software House[sep]About us[sep]Contact us',
            ),
            12 => 
            array (
                'id' => 13,
                'text_name' => 'arabic nav bar',
                'text_value' => 'الرئسية[sep]الحلول[sep]الخدمات[sep]المعرض[sep]شركات البرمجة[sep]من نحن[sep]تواصل معنا',
            ),
            13 => 
            array (
                'id' => 14,
                'text_name' => 'contact info',
                'text_value' => 'Contact information',
            ),
            14 => 
            array (
                'id' => 15,
                'text_name' => 'Contact information brief',
                'text_value' => 'Thank you for taking the time to read our company profile. If there are any questions or comments, please feel free to contact us.',
            ),
            15 => 
            array (
                'id' => 16,
                'text_name' => 'arabic Contact information',
                'text_value' => 'Contact information',
            ),
            16 => 
            array (
                'id' => 17,
                'text_name' => 'arabic Contact information brief',
                'text_value' => 'Thank you for taking the time to read our company profile. If there are any questions or comments, please feel free to contact us.',
            ),
            17 => 
            array (
                'id' => 18,
                'text_name' => 'arabic after logo breif',
                'text_value' => 'Upgrade Team is one of Partnership Company in Egypt since 2013 for Professional Software Development',
            ),
        ));
        
        
    }
}