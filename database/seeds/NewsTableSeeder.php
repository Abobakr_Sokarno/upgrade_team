<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('news')->delete();
        
        \DB::table('news')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'Working Group with Falcon General Contracting Company[EN:]Working Group with Falcon General Contracting Company',
                'description' => 'its Director Mr. Majed Samak for Mr. Qalioubia Governor and Project Manager of Giza[EN:]its Director Mr. Majed Samak for Mr. Qalioubia Governor and Project Manager of Giza',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/685471525972937.jpg',
                'published_by' => 'aboelwafa[EN:]aboelwafa',
                'date' => '2018-05-10',
                'categories' => 'Business[EN:]Business',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'header' => 'Our company was honored to work with Mr. Farouk El Kady[EN:]Our company was honored to work with Mr. Farouk El Kady',
                'description' => 'Nile Engineering Consulting Company[EN:]Nile Engineering Consulting Company',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/441251525973168.jpg',
                'published_by' => 'aboelwafa[EN:]aboelwafa',
                'date' => '2018-05-10',
                'categories' => 'Marketting[EN:]Business',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'header' => 'Due to the current expansions the developer company for technology solutions[EN:]Due to the current expansions the developer company for technology solutions',
                'description' => 'is requiredElectrical Engineer Technical Office Experience of two to five years Please send your CV to: Hr@upgrade-team.com[EN:]is requiredElectrical Engineer Technical Office Experience of two to five years Please send your CV to: Hr@upgrade-team.com',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/766971525973192.jpg',
                'published_by' => 'aboelwafa[EN:]aboelwafa',
                'date' => '2018-05-10',
                'categories' => 'Jobs[EN:]Business',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}