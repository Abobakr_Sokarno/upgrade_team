<?php

use Illuminate\Database\Seeder;

class TeamTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('team')->delete();
        
        \DB::table('team')->insert(array (
            0 => 
            array (
                'id' => 5,
                'header' => 'Abdelrhman Fathy[EN:]Abdelrhman Fathy',
                'description' => 'Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.[EN:]Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.',
                'position' => 'General Manager[EN:]General Manager',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/787321526114735.jpg',
                'facebook_link' => 'www.facebook.com',
                'linkedin_link' => 'www.linkedin.com',
                'twitter_link' => 'www.twitter.com',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 6,
                'header' => 'Abdelrhman Fathy[EN:]Abdelrhman Fathy',
                'description' => 'Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.[EN:]Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.',
                'position' => 'General Manager[EN:]General Manager',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/787321526114735.jpg',
                'facebook_link' => 'www.facebook.com',
                'linkedin_link' => 'www.linkedin.com',
                'twitter_link' => 'www.twitter.com',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 7,
                'header' => 'Abdelrhman Fathy[EN:]Abdelrhman Fathy',
                'description' => 'Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.[EN:]Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.',
                'position' => 'General Manager[EN:]General Manager',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/787321526114735.jpg',
                'facebook_link' => 'www.facebook.com',
                'linkedin_link' => 'www.linkedin.com',
                'twitter_link' => 'www.twitter.com',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 8,
                'header' => 'Abdelrhman Fathy[EN:]Abdelrhman Fathy',
                'description' => 'Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.[EN:]Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.',
                'position' => 'General Manager[EN:]General Manager',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/787321526114735.jpg',
                'facebook_link' => 'www.facebook.com',
                'linkedin_link' => 'www.linkedin.com',
                'twitter_link' => 'www.twitter.com',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}