<?php

use Illuminate\Database\Seeder;

class ContactMessagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contact_messages')->delete();
        
        \DB::table('contact_messages')->insert(array (
            0 => 
            array (
                'id' => 10,
                'sender_name' => 'ksndfkn',
                'sender_email' => 'nsdknf@jdsknf.com',
                'sender_message' => 'ssijdljsdnf',
                'seen' => 0,
                'created_at' => '2018-05-10 21:27:17',
                'updated_at' => '2018-05-10 21:27:17',
            ),
            1 => 
            array (
                'id' => 11,
                'sender_name' => 'ahmedgouda',
                'sender_email' => 'ahmedmoud@ymail.com',
                'sender_message' => 'ajfnasjfnsajnasn',
                'seen' => 0,
                'created_at' => '2018-05-10 21:28:02',
                'updated_at' => '2018-05-10 21:28:02',
            ),
            2 => 
            array (
                'id' => 12,
                'sender_name' => 'ahmedgouda',
                'sender_email' => 'ahmedmoudymailcom',
                'sender_message' => 'ajfnasjfnsajnasnsdsa',
                'seen' => 0,
                'created_at' => '2018-05-10 21:28:25',
                'updated_at' => '2018-05-10 21:28:25',
            ),
            3 => 
            array (
                'id' => 13,
                'sender_name' => 'ahmedgouda',
                'sender_email' => 'ahmedmoudymailcom',
                'sender_message' => 'ajfnasjfnsajnasnsdsasda',
                'seen' => 0,
                'created_at' => '2018-05-10 21:32:47',
                'updated_at' => '2018-05-10 21:32:47',
            ),
            4 => 
            array (
                'id' => 14,
                'sender_name' => 'kfaksln',
                'sender_email' => 'knsknfsdakn@yahh.com',
                'sender_message' => 'nsknfsam',
                'seen' => 0,
                'created_at' => '2018-05-10 21:34:20',
                'updated_at' => '2018-05-10 21:34:20',
            ),
            5 => 
            array (
                'id' => 15,
                'sender_name' => 'kasmdfkldsnj',
                'sender_email' => 'njsnfdj@jsdknfj.com',
                'sender_message' => 'nsdfkdsnf',
                'seen' => 0,
                'created_at' => '2018-05-10 21:35:00',
                'updated_at' => '2018-05-10 21:35:00',
            ),
            6 => 
            array (
                'id' => 16,
                'sender_name' => 'jnfkdsn',
                'sender_email' => 'jnjsdnfj@kndsfklsd.com',
                'sender_message' => 'nsdsnf',
                'seen' => 0,
                'created_at' => '2018-05-10 21:36:36',
                'updated_at' => '2018-05-10 21:36:36',
            ),
            7 => 
            array (
                'id' => 17,
                'sender_name' => 'kansdkn',
                'sender_email' => 'ssdnfasn@asjnfksja.com',
                'sender_message' => 'ksfknm',
                'seen' => 0,
                'created_at' => '2018-05-12 08:58:40',
                'updated_at' => '2018-05-12 08:58:40',
            ),
        ));
        
        
    }
}