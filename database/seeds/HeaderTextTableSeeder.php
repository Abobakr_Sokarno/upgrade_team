<?php

use Illuminate\Database\Seeder;

class HeaderTextTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('header_text')->delete();
        
        \DB::table('header_text')->insert(array (
            0 => 
            array (
                'id' => 1,
                'text_name' => 'facebook link',
                'text_value' => 'https://www.facebook.com',
            ),
            1 => 
            array (
                'id' => 2,
                'text_name' => 'twitter link',
                'text_value' => 'https://www.twitter.com',
            ),
            2 => 
            array (
                'id' => 3,
                'text_name' => 'linkedin link',
                'text_value' => 'https://www.linkedin.com',
            ),
            3 => 
            array (
                'id' => 4,
                'text_name' => 'tawoolink',
                'text_value' => 'https://www.tawoo.com',
            ),
            4 => 
            array (
                'id' => 5,
                'text_name' => 'address',
                'text_value' => '12 , Al shater Alasher st , New Maadi , cairo , egypt',
            ),
            5 => 
            array (
                'id' => 6,
                'text_name' => 'email',
                'text_value' => 'info@upgrade-team.com',
            ),
            6 => 
            array (
                'id' => 7,
                'text_name' => 'phone',
                'text_value' => '01026070490',
            ),
            7 => 
            array (
                'id' => 8,
                'text_name' => 'logo',
                'text_value' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/197661526033361.png',
            ),
            8 => 
            array (
                'id' => 9,
                'text_name' => 'arabic address',
                'text_value' => '12 , Al shater Alasher st , New Maadi , cairo , egypt',
            ),
            9 => 
            array (
                'id' => 10,
                'text_name' => 'arabic phone',
                'text_value' => '01026070490',
            ),
            10 => 
            array (
                'id' => 11,
                'text_name' => 'arabic logo',
                'text_value' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/197661526033361.png',
            ),
            11 => 
            array (
                'id' => 12,
                'text_name' => 'nav bar',
                'text_value' => 'Home[sep]Solutions[sep]Services[sep]Gallery[sep]Software House[sep]About us[sep]Contact us',
            ),
            12 => 
            array (
                'id' => 13,
                'text_name' => 'arabic nav bar',
                'text_value' => 'الرئسية[sep]الحلول[sep]الخدمات[sep]المعرض[sep]شركات البرمجة[sep]من نحن[sep]تواصل معنا',
            ),
            13 => 
            array (
                'id' => 14,
                'text_name' => 'solution slider',
                'text_value' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/609631526123401.jpg',
            ),
            14 => 
            array (
                'id' => 15,
                'text_name' => 'service slider',
                'text_value' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/362671526123428.jpg',
            ),
            15 => 
            array (
                'id' => 16,
                'text_name' => 'gallery slider',
                'text_value' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/536181526123441.jpg',
            ),
            16 => 
            array (
                'id' => 17,
                'text_name' => 'softwarehouse slider',
                'text_value' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/814151526123456.jpg',
            ),
            17 => 
            array (
                'id' => 18,
                'text_name' => 'about_us slider',
                'text_value' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/216761526123472.jpg',
            ),
            18 => 
            array (
                'id' => 19,
                'text_name' => 'contact_us slider',
                'text_value' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/750511526125370.jpg',
            ),
        ));
        
        
    }
}