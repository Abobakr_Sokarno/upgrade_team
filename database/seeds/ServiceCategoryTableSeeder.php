<?php

use Illuminate\Database\Seeder;

class ServiceCategoryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('service_category')->delete();
        
        \DB::table('service_category')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_name' => 'professional services[EN:]professional services',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_name' => 'support services[EN:]support services',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_name' => 'consultant services[EN:]consultant services',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}