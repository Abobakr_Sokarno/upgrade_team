<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'ahmedgouda',
            'email' => 'ahmedmoud@ymail.com',
            'password' => md5('ahmed1234'),
        ]);
        DB::table('about_us')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'Who we are[EN:]Who we are',
                'short_description' => 'Upgrade Team developed 7+ Desktop Application[EN:]Upgrade Team developed 7+ Desktop Application',
                'description' => 'Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.[EN:]Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.',
                'image' => 'http://localhsost/upgrade_team/uploads/531951526077546.jpg',
                'link' => 'http://upgrade-team.bitballoon.com/about_us.html',
            ),
        ));

    
        DB::table('branches')->insert(array (
            0 => 
            array (
                'id' => 1,
                'map_address' => 'abdelrhman[EN:]adblerhman',
                'street' => 'elgzar[EN:]elgzar',
                'city' => 'cairo[EN:]cairo',
                'twitter_link' => 'www.twitter.com',
                'linkedin_link' => 'www.linkedin.com',
                'facebook_link' => 'www.facebook.com',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'map_address' => 'medhat[EN:]medhat',
                'street' => 'elgzar[EN:]elgzar',
                'city' => 'cairo[EN:]cairo',
                'twitter_link' => 'www.twitter.com',
                'linkedin_link' => 'www.linkedin.com',
                'facebook_link' => 'www.facebook.com',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'map_address' => 'abdelrhman[EN:]adblerhman',
                'street' => 'elgzar[EN:]elgzar',
                'city' => 'cairo[EN:]cairo',
                'twitter_link' => 'www.twitter.com',
                'linkedin_link' => 'www.linkedin.com',
                'facebook_link' => 'www.facebook.com',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('contact_messages')->insert(array (
            0 => 
            array (
                'id' => 10,
                'sender_name' => 'ksndfkn',
                'sender_email' => 'nsdknf@jdsknf.com',
                'sender_message' => 'ssijdljsdnf',
                'seen' => 0,
                'created_at' => '2018-05-10 21:27:17',
                'updated_at' => '2018-05-10 21:27:17',
            ),
            1 => 
            array (
                'id' => 11,
                'sender_name' => 'ahmedgouda',
                'sender_email' => 'ahmedmoud@ymail.com',
                'sender_message' => 'ajfnasjfnsajnasn',
                'seen' => 0,
                'created_at' => '2018-05-10 21:28:02',
                'updated_at' => '2018-05-10 21:28:02',
            ),
            2 => 
            array (
                'id' => 12,
                'sender_name' => 'ahmedgouda',
                'sender_email' => 'ahmedmoudymailcom',
                'sender_message' => 'ajfnasjfnsajnasnsdsa',
                'seen' => 0,
                'created_at' => '2018-05-10 21:28:25',
                'updated_at' => '2018-05-10 21:28:25',
            ),
            3 => 
            array (
                'id' => 13,
                'sender_name' => 'ahmedgouda',
                'sender_email' => 'ahmedmoudymailcom',
                'sender_message' => 'ajfnasjfnsajnasnsdsasda',
                'seen' => 0,
                'created_at' => '2018-05-10 21:32:47',
                'updated_at' => '2018-05-10 21:32:47',
            ),
            4 => 
            array (
                'id' => 14,
                'sender_name' => 'kfaksln',
                'sender_email' => 'knsknfsdakn@yahh.com',
                'sender_message' => 'nsknfsam',
                'seen' => 0,
                'created_at' => '2018-05-10 21:34:20',
                'updated_at' => '2018-05-10 21:34:20',
            ),
            5 => 
            array (
                'id' => 15,
                'sender_name' => 'kasmdfkldsnj',
                'sender_email' => 'njsnfdj@jsdknfj.com',
                'sender_message' => 'nsdfkdsnf',
                'seen' => 0,
                'created_at' => '2018-05-10 21:35:00',
                'updated_at' => '2018-05-10 21:35:00',
            ),
            6 => 
            array (
                'id' => 16,
                'sender_name' => 'jnfkdsn',
                'sender_email' => 'jnjsdnfj@kndsfklsd.com',
                'sender_message' => 'nsdsnf',
                'seen' => 0,
                'created_at' => '2018-05-10 21:36:36',
                'updated_at' => '2018-05-10 21:36:36',
            ),
            7 => 
            array (
                'id' => 17,
                'sender_name' => 'kansdkn',
                'sender_email' => 'ssdnfasn@asjnfksja.com',
                'sender_message' => 'ksfknm',
                'seen' => 0,
                'created_at' => '2018-05-12 08:58:40',
                'updated_at' => '2018-05-12 08:58:40',
            ),
        ));
        
        DB::table('contact_us')->insert(array (
            0 => 
            array (
                'id' => 1,
                'address' => '12 S Al Shater Alasher st, New Maadi Cairo Egypt.[EN:]12 S Al Shater Alasher st, New Maadi Cairo Egypt.',
                'email' => 'info@upgrade-team.com',
                'phone' => '002 270 41 228,002 010 260 704 90',
            ),
        ));
        
        DB::table('features')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'network solutions[EN:]network solutions',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/7411525972372.png',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'header' => 'Business develop[EN:]Business develop',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/407481525972410.png',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'header' => 'People research[EN:]People research',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/931831525972438.png',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'header' => 'Marketing[EN:]Marketing',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/308541525972467.png',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    DB::table('gallery_category')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_name' => 'Branding[EN:]Branding',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_name' => 'Network[EN:]Network',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_name' => 'electronic[EN:]electronic',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
 DB::table('gallery')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_id' => 1,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/51511525969517.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_id' => 2,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/564131525969672.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_id' => 2,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/302061525969691.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'category_id' => 1,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/138141525969706.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            4 => 
            array (
                'id' => 5,
                'category_id' => 1,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/331291525969747.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            5 => 
            array (
                'id' => 6,
                'category_id' => 1,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/265171525969780.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            6 => 
            array (
                'id' => 7,
                'category_id' => 3,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/31981525969800.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            7 => 
            array (
                'id' => 8,
                'category_id' => 3,
                'header' => 'Lorem Ipsum[EN:]Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'image' => 'http://localhost/upgrade_team/uploads/51511525969517.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('header_text')->insert(array (
            0 => 
            array (
                'id' => 1,
                'text_name' => 'facebook link',
                'text_value' => 'https://www.facebook.com',
            ),
            1 => 
            array (
                'id' => 2,
                'text_name' => 'twitter link',
                'text_value' => 'https://www.twitter.com',
            ),
            2 => 
            array (
                'id' => 3,
                'text_name' => 'linkedin link',
                'text_value' => 'https://www.linkedin.com',
            ),
            3 => 
            array (
                'id' => 4,
                'text_name' => 'tawoolink',
                'text_value' => 'https://www.tawoo.com',
            ),
            4 => 
            array (
                'id' => 5,
                'text_name' => 'address',
                'text_value' => '12 , Al shater Alasher st , New Maadi , cairo , egypt',
            ),
            5 => 
            array (
                'id' => 6,
                'text_name' => 'email',
                'text_value' => 'info@upgrade-team.com',
            ),
            6 => 
            array (
                'id' => 7,
                'text_name' => 'phone',
                'text_value' => '01026070490',
            ),
            7 => 
            array (
                'id' => 8,
                'text_name' => 'logo',
                'text_value' => 'http://localhost/upgrade_team/uploads/197661526033361.png',
            ),
            8 => 
            array (
                'id' => 9,
                'text_name' => 'arabic address',
                'text_value' => '12 , Al shater Alasher st , New Maadi , cairo , egypt',
            ),
            9 => 
            array (
                'id' => 10,
                'text_name' => 'arabic phone',
                'text_value' => '01026070490',
            ),
            10 => 
            array (
                'id' => 11,
                'text_name' => 'arabic logo',
                'text_value' => 'http://localhost/upgrade_team/uploads/197661526033361.png',
            ),
            11 => 
            array (
                'id' => 12,
                'text_name' => 'nav bar',
                'text_value' => 'Home[sep]Solutions[sep]Services[sep]Gallery[sep]Software House[sep]About us[sep]Contact us',
            ),
            12 => 
            array (
                'id' => 13,
                'text_name' => 'arabic nav bar',
                'text_value' => 'الرئسية[sep]الحلول[sep]الخدمات[sep]المعرض[sep]شركات البرمجة[sep]من نحن[sep]تواصل معنا',
            ),
            13 => 
            array (
                'id' => 14,
                'text_name' => 'solution slider',
                'text_value' => 'http://localhost/upgrade_team/uploads/609631526123401.jpg',
            ),
            14 => 
            array (
                'id' => 15,
                'text_name' => 'service slider',
                'text_value' => 'http://localhost/upgrade_team/uploads/362671526123428.jpg',
            ),
            15 => 
            array (
                'id' => 16,
                'text_name' => 'gallery slider',
                'text_value' => 'http://localhost/upgrade_team/uploads/536181526123441.jpg',
            ),
            16 => 
            array (
                'id' => 17,
                'text_name' => 'softwarehouse slider',
                'text_value' => 'http://localhost/upgrade_team/uploads/814151526123456.jpg',
            ),
            17 => 
            array (
                'id' => 18,
                'text_name' => 'about_us slider',
                'text_value' => 'http://localhost/upgrade_team/uploads/216761526123472.jpg',
            ),
            18 => 
            array (
                'id' => 19,
                'text_name' => 'contact_us slider',
                'text_value' => 'http://localhost/upgrade_team/uploads/750511526125370.jpg',
            ),
        ));
        
        DB::table('home_page_slider_blocks')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'Vision statement[EN:]Vision statement',
                'description' => 'To become one of the leading Egyptian companies in providing Smart Management Solution[EN:]To become one of the leading Egyptian companies in providing Smart Management Solution',
                'image' => 'http://localhost/upgrade_team/uploads/296571525971211.png',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'header' => 'Mission statement[EN:]Mission statement',
                'description' => 'We are very keen on the development of our human capital, as we seek excellence& professionalism in serving our clients[EN:]We are very keen on the development of our human capital, as we seek excellence& professionalism in serving our clients',
                'image' => 'http://localhost/upgrade_team/uploads/847911525966058.png',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'header' => 'Business goals & objectives[EN:]Business goals & objectives',
                'description' => 'Specific goals that the company wants to achieve the full interfacing between softwaredevelopments[EN:]Specific goals that the company wants to achieve the full interfacing between softwaredevelopments',
                'image' => 'http://localhost/upgrade_team/uploads/649781525966105.png',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('news')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'Working Group with Falcon General Contracting Company[EN:]Working Group with Falcon General Contracting Company',
                'description' => 'its Director Mr. Majed Samak for Mr. Qalioubia Governor and Project Manager of Giza[EN:]its Director Mr. Majed Samak for Mr. Qalioubia Governor and Project Manager of Giza',
                'image' => 'http://localhost/upgrade_team/uploads/685471525972937.jpg',
                'published_by' => 'aboelwafa[EN:]aboelwafa',
                'date' => '2018-05-10',
                'categories' => 'Business[EN:]Business',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'header' => 'Our company was honored to work with Mr. Farouk El Kady[EN:]Our company was honored to work with Mr. Farouk El Kady',
                'description' => 'Nile Engineering Consulting Company[EN:]Nile Engineering Consulting Company',
                'image' => 'http://localhost/upgrade_team/uploads/441251525973168.jpg',
                'published_by' => 'aboelwafa[EN:]aboelwafa',
                'date' => '2018-05-10',
                'categories' => 'Marketting[EN:]Business',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'header' => 'Due to the current expansions the developer company for technology solutions[EN:]Due to the current expansions the developer company for technology solutions',
                'description' => 'is requiredElectrical Engineer Technical Office Experience of two to five years Please send your CV to: Hr@upgrade-team.com[EN:]is requiredElectrical Engineer Technical Office Experience of two to five years Please send your CV to: Hr@upgrade-team.com',
                'image' => 'http://localhost/upgrade_team/uploads/766971525973192.jpg',
                'published_by' => 'aboelwafa[EN:]aboelwafa',
                'date' => '2018-05-10',
                'categories' => 'Jobs[EN:]Business',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    DB::table('other_text_settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'text_name' => 'Testimonial Header',
                'text_value' => 'Testimonial[EN:]',
            ),
            1 => 
            array (
                'id' => 2,
                'text_name' => 'Testimonial small brief',
                'text_value' => 'What My Client\'s Say[EN:]',
            ),
            2 => 
            array (
                'id' => 3,
                'text_name' => 'Portfolio header',
                'text_value' => 'Portfolio [EN:]',
            ),
            3 => 
            array (
                'id' => 4,
                'text_name' => 'Portfolio breif',
                'text_value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry[EN:]',
            ),
            4 => 
            array (
                'id' => 5,
                'text_name' => 'Features header',
                'text_value' => 'Features [EN:]',
            ),
            5 => 
            array (
                'id' => 6,
                'text_name' => 'Features brief',
                'text_value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]',
            ),
            6 => 
            array (
                'id' => 7,
                'text_name' => 'Recent News header',
                'text_value' => 'Recent News[EN:]',
            ),
            7 => 
            array (
                'id' => 8,
                'text_name' => 'Recent News brief',
                'text_value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.[EN:]',
            ),
            8 => 
            array (
                'id' => 9,
                'text_name' => 'Contact Us Header',
                'text_value' => 'Contact Us[EN:]',
            ),
            9 => 
            array (
                'id' => 10,
                'text_name' => 'contact us form header',
                'text_value' => 'Get in Touch[EN:]',
            ),
            10 => 
            array (
                'id' => 11,
                'text_name' => 'contact us form small breif',
                'text_value' => 'Feel free to contact with us[EN:]',
            ),
            11 => 
            array (
                'id' => 12,
                'text_name' => 'our_skills header',
                'text_value' => 'our_skills[EN:]',
            ),
            12 => 
            array (
                'id' => 13,
                'text_name' => 'our skills brief',
                'text_value' => 'Publishing packages and web page editors now use Lorem Ipsum as their default model text[EN:]Publishing packages and web page editors now use Lorem Ipsum as their default model text',
            ),
            13 => 
            array (
                'id' => 14,
                'text_name' => 'Our Team header',
                'text_value' => 'Our Team[EN:]',
            ),
            14 => 
            array (
                'id' => 15,
                'text_name' => 'Our Team small breif',
                'text_value' => 'Publishing packages and web page editors now use Lorem Ipsum as their default model text[EN:]',
            ),
            15 => 
            array (
                'id' => 16,
                'text_name' => 'Our Client header',
                'text_value' => 'Our Client[EN:]',
            ),
            16 => 
            array (
                'id' => 17,
                'text_name' => 'Our Client brief',
                'text_value' => 'Publishing packages and web page editors now use Lorem Ipsum as their default model text[EN:]Publishing packages and web page editors now use Lorem Ipsum as their default model text',
            ),
        ));
        DB::table('our_clients')->insert(array (
            0 => 
            array (
                'id' => 1,
                'image' => 'http://localhost/upgrade_team/uploads/895701526053717.png',
                'link' => '#',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'image' => 'http://localhost/upgrade_team/uploads/761451526053733.png',
                'link' => '#',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'image' => 'http://localhost/upgrade_team/uploads/755141526053744.png',
                'link' => '#',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'image' => 'http://localhost/upgrade_team/uploads/346851526053764.png',
                'link' => '#',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('our_skills')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'Services[EN:]Services',
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',
                'image' => 'http://localhost/upgrade_team/uploads/549281526114126.jpg',
                'quality' => 50,
                'experince' => 100,
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'solutions[EN:]solutions',
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',
                'image' => 'http://localhost/upgrade_team/uploads/645531526113316.jpg',
                'quality' => 75,
                'experince' => 80,
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('service_category')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_name' => 'professional services[EN:]professional services',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_name' => 'support services[EN:]support services',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_name' => 'consultant services[EN:]consultant services',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('services')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/67071526058317.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/384141526058423.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/446101526058435.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/158061526058445.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            4 => 
            array (
                'id' => 5,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/67071526058317.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            5 => 
            array (
                'id' => 6,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/793581526058459.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            6 => 
            array (
                'id' => 7,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/780061526058472.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            7 => 
            array (
                'id' => 8,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/421821526058485.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            8 => 
            array (
                'id' => 9,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/67071526058317.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            9 => 
            array (
                'id' => 10,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/945751526058501.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            10 => 
            array (
                'id' => 11,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/845021526058517.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            11 => 
            array (
                'id' => 12,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/67071526058317.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('slider')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'Upgrade Team For Technology Solutions[EN:]Upgrade Team For Technology Solutions',
                'description' => 'Upgrade Team is one of Partnership Company in Egypt since 2013 for Professional Software Development, Low Current Systems and Power Systems. In short time we have a very good experience in our carrier and develop ourselves continuity towards the gateway of success.[EN:]Upgrade Team is one of Partnership Company in Egypt since 2013 for Professional Software Development, Low Current Systems and Power Systems. In short time we have a very good experience in our carrier and develop ourselves continuity towards the gateway of success.',
                'image' => 'http://localhost/upgrade_team/uploads/189601525963046.jpg',
                'button_link' => NULL,
                'sort' => 1,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'header' => 'Upgrade Team developed 7+ Desktop Application[EN:]Upgrade Team developed 7+ Desktop Application',
                'description' => 'Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.[EN:]Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.',
                'image' => 'http://localhost/upgrade_team/uploads/944171525963144.jpg',
                'button_link' => NULL,
                'sort' => 2,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'header' => 'What we do[EN:]What we do',
                'description' => 'We working on software development in fields of Desktop Application, Web Application. Working on LowCurrent System installation based on software development idea and Power system solutions.[EN:]We working on software development in fields of Desktop Application, Web Application. Working on LowCurrent System installation based on software development idea and Power system solutions.',
                'image' => 'http://localhost/upgrade_team/uploads/220491525963194.jpg',
                'button_link' => NULL,
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        DB::table('solution_category')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_name' => 'low current solution[EN:]low current solution',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_name' => 'power solution[EN:]power solution',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_name' => 'Data Center solution[EN:]Data Center solution',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'category_name' => 'Security solution[EN:]Security solution',
                'sort' => 0,
                'featured' => 0,
            ),
            4 => 
            array (
                'id' => 5,
                'category_name' => 'cloud solution[EN:]cloud solution',
                'sort' => 0,
                'featured' => 0,
            ),
            5 => 
            array (
                'id' => 6,
                'category_name' => 'Network solution[EN:]Network solution',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('solutions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/428971526048987.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/115951526049234.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/219671526049250.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'category_id' => 4,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/725661526049267.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            4 => 
            array (
                'id' => 5,
                'category_id' => 5,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/30591526049315.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            5 => 
            array (
                'id' => 6,
                'category_id' => 6,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/435351526049287.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            6 => 
            array (
                'id' => 9,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/428971526048987.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            7 => 
            array (
                'id' => 10,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/115951526049234.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            8 => 
            array (
                'id' => 11,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/219671526049250.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            9 => 
            array (
                'id' => 12,
                'category_id' => 4,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/725661526049267.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            10 => 
            array (
                'id' => 13,
                'category_id' => 5,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/30591526049315.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            11 => 
            array (
                'id' => 14,
                'category_id' => 6,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/435351526049287.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            12 => 
            array (
                'id' => 15,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/428971526048987.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            13 => 
            array (
                'id' => 16,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/115951526049234.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            14 => 
            array (
                'id' => 17,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/219671526049250.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            15 => 
            array (
                'id' => 18,
                'category_id' => 4,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/725661526049267.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            16 => 
            array (
                'id' => 19,
                'category_id' => 5,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/30591526049315.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            17 => 
            array (
                'id' => 20,
                'category_id' => 6,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/435351526049287.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            18 => 
            array (
                'id' => 21,
                'category_id' => 1,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/428971526048987.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            19 => 
            array (
                'id' => 22,
                'category_id' => 2,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/115951526049234.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            20 => 
            array (
                'id' => 23,
                'category_id' => 3,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/219671526049250.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            21 => 
            array (
                'id' => 24,
                'category_id' => 4,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/725661526049267.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            22 => 
            array (
                'id' => 25,
                'category_id' => 5,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/30591526049315.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
            23 => 
            array (
                'id' => 26,
                'category_id' => 6,
                'header' => 'What is Lorem Ipsum[EN:]What is Lorem Ipsum',
                'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words[EN:]Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words',
                'image' => 'http://localhost/upgrade_team/uploads/435351526049287.jpg',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('team')->insert(array (
            0 => 
            array (
                'id' => 5,
                'header' => 'Abdelrhman Fathy[EN:]Abdelrhman Fathy',
                'description' => 'Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.[EN:]Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.',
                'position' => 'General Manager[EN:]General Manager',
                'image' => 'http://localhost/upgrade_team/uploads/787321526114735.jpg',
                'facebook_link' => 'www.facebook.com',
                'linkedin_link' => 'www.linkedin.com',
                'twitter_link' => 'www.twitter.com',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 6,
                'header' => 'Abdelrhman Fathy[EN:]Abdelrhman Fathy',
                'description' => 'Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.[EN:]Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.',
                'position' => 'General Manager[EN:]General Manager',
                'image' => 'http://localhost/upgrade_team/uploads/787321526114735.jpg',
                'facebook_link' => 'www.facebook.com',
                'linkedin_link' => 'www.linkedin.com',
                'twitter_link' => 'www.twitter.com',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 7,
                'header' => 'Abdelrhman Fathy[EN:]Abdelrhman Fathy',
                'description' => 'Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.[EN:]Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.',
                'position' => 'General Manager[EN:]General Manager',
                'image' => 'http://localhost/upgrade_team/uploads/787321526114735.jpg',
                'facebook_link' => 'www.facebook.com',
                'linkedin_link' => 'www.linkedin.com',
                'twitter_link' => 'www.twitter.com',
                'sort' => 0,
                'featured' => 0,
            ),
            3 => 
            array (
                'id' => 8,
                'header' => 'Abdelrhman Fathy[EN:]Abdelrhman Fathy',
                'description' => 'Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.[EN:]Iusto Lorem ipsum dolor sit amet, consectetur adipisicing elit. , sapiente.',
                'position' => 'General Manager[EN:]General Manager',
                'image' => 'http://localhost/upgrade_team/uploads/787321526114735.jpg',
                'facebook_link' => 'www.facebook.com',
                'linkedin_link' => 'www.linkedin.com',
                'twitter_link' => 'www.twitter.com',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('testmonial')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'farouk el kadi[EN:]farouk el kadi',
                'company_name' => 'Elnile Company[EN:]Elnile Company',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry',
                'image' => 'http://localhost/upgrade_team/uploads/878081525968553.png',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'farouk el kadi[EN:]farouk el kadi',
                'company_name' => 'Elnile Company[EN:]Elnile Company',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry',
                'image' => 'http://localhost/upgrade_team/uploads/878081525968553.png',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'farouk el kadi[EN:]farouk el kadi',
                'company_name' => 'Elnile Company[EN:]Elnile Company',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry[EN:]Lorem Ipsum is simply dummy text of the printing and typesetting industry',
                'image' => 'http://localhost/upgrade_team/uploads/878081525968553.png',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        DB::table('what_we_do')->insert(array (
            0 => 
            array (
                'id' => 1,
                'text' => 'We working on software development in fields of Desktop Application[EN:]We working on software development in fields of Desktop Application',
                'image' => 'http://localhost/upgrade_team/uploads/876031526043330.png',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'text' => 'Working on Low Current System installation[EN:]Working on Low Current System installation',
                'image' => 'http://localhost/upgrade_team/uploads/962481526043351.png',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'text' => 'Power system solutions[EN:]Power system solutions',
                'image' => 'http://localhost/upgrade_team/uploads/490461526043374.png',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
    
        DB::table('footer_text')->insert(array (
            0 => 
            array (
                'id' => 1,
                'text_name' => 'facebook link',
                'text_value' => 'https://www.facebook.com',
            ),
            1 => 
            array (
                'id' => 2,
                'text_name' => 'twitter link',
                'text_value' => 'https://www.twitter.com',
            ),
            2 => 
            array (
                'id' => 3,
                'text_name' => 'linkedin link',
                'text_value' => 'https://www.linkedin.com',
            ),
            3 => 
            array (
                'id' => 4,
                'text_name' => 'tawoolink',
                'text_value' => 'https://www.tawoo.com',
            ),
            4 => 
            array (
                'id' => 5,
                'text_name' => 'address',
                'text_value' => '12 , Al shater Alasher st , New Maadi , cairo , egypt',
            ),
            5 => 
            array (
                'id' => 6,
                'text_name' => 'email',
                'text_value' => 'info@upgrade-team.com',
            ),
            6 => 
            array (
                'id' => 7,
                'text_name' => 'phone',
                'text_value' => '01026070490',
            ),
            7 => 
            array (
                'id' => 8,
                'text_name' => 'logo',
                'text_value' => 'http://localhost/upgrade_team/uploads/197661526033361.png',
            ),
            8 => 
            array (
                'id' => 9,
                'text_name' => 'arabic address',
                'text_value' => '12 , Al shater Alasher st , New Maadi , cairo , egypt',
            ),
            9 => 
            array (
                'id' => 10,
                'text_name' => 'arabic phone',
                'text_value' => '01026070490',
            ),
            10 => 
            array (
                'id' => 11,
                'text_name' => 'arabic logo',
                'text_value' => 'http://localhost/upgrade_team/uploads/197661526033361.png',
            ),
            11 => 
            array (
                'id' => 12,
                'text_name' => 'nav bar',
                'text_value' => 'Home[sep]Solutions[sep]Services[sep]Gallery[sep]Software House[sep]About us[sep]Contact us',
            ),
            12 => 
            array (
                'id' => 13,
                'text_name' => 'arabic nav bar',
                'text_value' => 'الرئسية[sep]الحلول[sep]الخدمات[sep]المعرض[sep]شركات البرمجة[sep]من نحن[sep]تواصل معنا',
            ),
            13 => 
            array (
                'id' => 14,
                'text_name' => 'contact info',
                'text_value' => 'Contact information',
            ),
            14 => 
            array (
                'id' => 15,
                'text_name' => 'Contact information brief',
                'text_value' => 'Thank you for taking the time to read our company profile. If there are any questions or comments, please feel free to contact us.',
            ),
            15 => 
            array (
                'id' => 16,
                'text_name' => 'arabic Contact information',
                'text_value' => 'Contact information',
            ),
            16 => 
            array (
                'id' => 17,
                'text_name' => 'arabic Contact information brief',
                'text_value' => 'Thank you for taking the time to read our company profile. If there are any questions or comments, please feel free to contact us.',
            ),
            17 => 
            array (
                'id' => 18,
                'text_name' => 'arabic after logo breif',
                'text_value' => 'Upgrade Team is one of Partnership Company in Egypt since 2013 for Professional Software Development',
            ),
        ));
        
        
    }
}
