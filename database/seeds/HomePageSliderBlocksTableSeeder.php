<?php

use Illuminate\Database\Seeder;

class HomePageSliderBlocksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('home_page_slider_blocks')->delete();
        
        \DB::table('home_page_slider_blocks')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'Vision statement[EN:]Vision statement',
                'description' => 'To become one of the leading Egyptian companies in providing Smart Management Solution[EN:]To become one of the leading Egyptian companies in providing Smart Management Solution',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/296571525971211.png',
                'sort' => 0,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'header' => 'Mission statement[EN:]Mission statement',
                'description' => 'We are very keen on the development of our human capital, as we seek excellence& professionalism in serving our clients[EN:]We are very keen on the development of our human capital, as we seek excellence& professionalism in serving our clients',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/847911525966058.png',
                'sort' => 0,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'header' => 'Business goals & objectives[EN:]Business goals & objectives',
                'description' => 'Specific goals that the company wants to achieve the full interfacing between softwaredevelopments[EN:]Specific goals that the company wants to achieve the full interfacing between softwaredevelopments',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/649781525966105.png',
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}