<?php

use Illuminate\Database\Seeder;

class SliderTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('slider')->delete();
        
        \DB::table('slider')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'Upgrade Team For Technology Solutions[EN:]Upgrade Team For Technology Solutions',
                'description' => 'Upgrade Team is one of Partnership Company in Egypt since 2013 for Professional Software Development, Low Current Systems and Power Systems. In short time we have a very good experience in our carrier and develop ourselves continuity towards the gateway of success.[EN:]Upgrade Team is one of Partnership Company in Egypt since 2013 for Professional Software Development, Low Current Systems and Power Systems. In short time we have a very good experience in our carrier and develop ourselves continuity towards the gateway of success.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/189601525963046.jpg',
                'button_link' => NULL,
                'sort' => 1,
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'header' => 'Upgrade Team developed 7+ Desktop Application[EN:]Upgrade Team developed 7+ Desktop Application',
                'description' => 'Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.[EN:]Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/944171525963144.jpg',
                'button_link' => NULL,
                'sort' => 2,
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'header' => 'What we do[EN:]What we do',
                'description' => 'We working on software development in fields of Desktop Application, Web Application. Working on LowCurrent System installation based on software development idea and Power system solutions.[EN:]We working on software development in fields of Desktop Application, Web Application. Working on LowCurrent System installation based on software development idea and Power system solutions.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/220491525963194.jpg',
                'button_link' => NULL,
                'sort' => 0,
                'featured' => 0,
            ),
        ));
        
        
    }
}