<?php

use Illuminate\Database\Seeder;

class AboutUs extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('about_us')->delete();
        
        \DB::table('about_us')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'Who we are[EN:]Who we are',
                'short_description' => 'Upgrade Team developed 7+ Desktop Application[EN:]Upgrade Team developed 7+ Desktop Application',
                'description' => 'Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.[EN:]Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.Upgrade Team developed 7+ Desktop Application in fields of CRM, Inventory Management, Restaurant Management,Sports Management and Print House.',
                'image' => 'http://'.$_SERVER['HTTP_HOST'].'uploads/531951526077546.jpg',
                'link' => 'http://upgrade-team.bitballoon.com/about_us.html',
            ),
        ));
        
        
    }
}